#include <omp.h>
#include <vector>
#include <fstream>
#include <iostream> 
#include <iomanip>
#include <ctime>

#include "oopFunc.hpp"
#include "omegaDist.hpp"
#include "data.hpp"
#include "computeValueFunction.hpp"

#include "simMM.hpp"
#include "fastSpline.hpp"

#include <Rcpp.h>

RcppExport SEXP cfsim(SEXP in);
RcppExport SEXP asymVar(SEXP in);
RcppExport SEXP valueGrid(SEXP in);
RcppExport SEXP validationFit(SEXP in);

using namespace fastSpline;
using namespace std;

template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
  if ( !v.empty() ) {
    const size_t newLine = 5;
    for(size_t j=0;j<v.size();j++) {
      if (j % newLine == 0)  out << endl << " | ";
      out << v[j] << " , ";
    }
  }
  return out;
}

// Returns an R list representing dat
Rcpp::List rData(const data &dat) {
  // copy pointers to vectors
  int nplan=dat.nplan;
  vector<int> jw, plan;
  vector<int> id, year;
  vector<double>
    cost, cumCost,   ded,  icl,   cat, weight, age, rs, nogap,
    xcut(nplan*4,NA_REAL),ycut(nplan*4,NA_REAL),slope(nplan*4,NA_REAL); 
  for(size_t i=0;i<dat.cost.dimSize(0);i++) {
    for(size_t y=0;y<dat.cost.dimSize(2);y++) {
      if (dat.in(i,y)) {
        id.push_back(i);
        year.push_back(y);
        jw.push_back(dat.joinweek(i,y));
        plan.push_back(dat.plan(i,y));
        ded.push_back(dat.ded(i,y));
        icl.push_back(dat.icl(i,y));
        cat.push_back(dat.cat(i,y));          
        weight.push_back(dat.weight(i,y));
        age.push_back(dat.age(i,y));
        if (dataNS::cfg.NX>0) rs.push_back(dat.X(i,0,y));
        else rs.push_back(0.0);
        if (dataNS::cfg.NX>2) nogap.push_back(dat.X(i,2,y));
        else nogap.push_back(0.0);
        for(size_t t=0;t<dat.cost.dimSize(1);t++) {
          cost.push_back(dat.cost(i,t,y));
          cumCost.push_back(dat.cumCost(i,t,y));
        }
      }
    }
  }
  cout << " id.size() = " << id.size() << endl;
  cout << " plan.size() = " << plan.size() << endl;
  cout << " cost.size() = " << cost.size() << endl;

  for(int i=0;i<nplan;i++) {
    for(size_t j=0;j<dat.oop[i].x().size();j++) {
      xcut[i*4+j] = dat.oop[i].x()[j];
      ycut[i*4+j] = dat.oop[i].y()[j];
      slope[i*4+j] = dat.oop[i].s()[j];      
    }
  }
  Rcpp::List out = Rcpp::List::create
    (Rcpp::Named("joinweek",jw),
     Rcpp::Named("plan",plan),
     Rcpp::Named("cost",cost),
     Rcpp::Named("cumCost",cumCost),
     Rcpp::Named("ded",ded),
     Rcpp::Named("icl",icl),
     Rcpp::Named("cat",cat),
     Rcpp::Named("weight",weight),
     Rcpp::Named("age",age),
     Rcpp::Named("rs",rs),
     Rcpp::Named("nogap",nogap),
     Rcpp::Named("xcut",xcut),
     Rcpp::Named("ycut",ycut),
     Rcpp::Named("slope",slope),
     Rcpp::Named("id",id),
     Rcpp::Named("year",year)
     );
  // FIXME: return cost and cumCost as matrices, see
  // http://stackoverflow.com/questions/12569992/constructing-3d-array-in-rcpp
  return(out);
}
                                                 
// Returns an R list representing simData
Rcpp::List rData(const simData &dat) 
{
  vector<double>
    cost, cumCost,  totOop, delay, totalDelay, li;
#ifdef DEBUG
  vector<double> vno, vtreat, theta;
#endif
  for(size_t i=0;i<dat.cost.dimSize(0);i++) {
    for(size_t y=0;y<dat.cost.dimSize(2);y++) {
      if (dat.dat->in(i,y)) {
        for(size_t t=0;t<dat.cost.dimSize(1);t++) {
          cost.push_back(dat.cost(i,t,y));
          cumCost.push_back(dat.cumCost(i,t,y));          
          delay.push_back(dat.delay(i,t,y));
          li.push_back(dat.lambdaIndex(i,t,y));
#ifdef DEBUG
          vno.push_back(dat.vno(i,t,y));
          vtreat.push_back(dat.vtreat(i,t,y));
          theta.push_back(dat.theta(i,t,y));
#endif
        }
        totalDelay.push_back(dat.totalDelay(i,y));
        int p = findPlan<int,1>(dat.dat->plan(i,y), dat.dat->up, dat.dat->nplan);        
        totOop.push_back( dat.dat->oop[p].eval(dat.cumCost(i,dat.dat->T-1,y),0) );
      }
    }
  }
  Rcpp::List out = Rcpp::List::create
    (Rcpp::Named("cost",cost),
     Rcpp::Named("cumCost",cumCost),
     Rcpp::Named("totOop",totOop),
     Rcpp::Named("delay",delay),
     Rcpp::Named("totalDelay",totalDelay),
#ifdef DEBUG
     Rcpp::Named("vno",vno),
     Rcpp::Named("vtreat",vtreat),
     Rcpp::Named("theta",theta),
#endif
     Rcpp::Named("lambdaIndex",li));
  return(out);
}

// Returns an R list representing simData
Rcpp::List rData(const vector<simData> &sd) 
{
  Rcpp::List out(sd.size());
  for(size_t i=0;i<sd.size();i++) out[i] = rData(sd[i]);
  return(out);
} 

// Returns an R list representing simData
Rcpp::List rData(const vector< vector<simData> > &sd) 
{
  Rcpp::List out(sd.size());
  for(size_t i=0;i<sd.size();i++) out[i] = rData(sd[i]);
  return(out);
} 

// Returns an R list representing simData
Rcpp::List rData(const vector<data> &sd) 
{
  Rcpp::List out(sd.size());
  for(size_t i=0;i<sd.size();i++) out[i] = rData(sd[i]);
  return(out);
} 


Rcpp::List rData(const randomDraws &rd, const parameters &pm) {
  //vector<double> theta( rd.N*rd.T, 0.0);
  //vector<double> omega( rd.N*rd.T, 0.0);
  vector<int> type;
  for(size_t i=0;i<rd.N;i++) {
    for(size_t y=0;y<rd.Y;y++) {
      if (rd.dat->in(i,y)) type.push_back(rd.type(i,y,pm));
    }
  }
  Rcpp::List out = Rcpp::List::create
    (Rcpp::Named("type",type));
  return(out);
}

/************************************************************************/
double averageSpend(const vector<simData> &sd)  {
  double mean = 0;
  size_t count = 0;
  for (size_t s = 0;s<sd.size();s++) {
    int T = sd[s].dat->T;
    int N = sd[s].dat->N;
    for (int i=0;i<N;i++) {
      mean += sd[s].cumCost[i+(T-1)*N];
      count++;
    }
  }
  mean /= count;
  return(mean);
}

vector<simData> resetDeductAndSimulate
(double ded, data &dat, const parameters &pm, const vector<randomDraws> &rand)
{
  for(int i=0;i<dat.N;i++) dat.ded[i] = ded;
  for(int p=0;p<dat.nplan;p++) {
    if (dat.oop[p].ded(ded,1)) {
      cout << "Warning oop[" << p << "].ded() not initialized" << endl;      
    }
  }
  valueFunction *vf = createValueFunctions(dat,pm);
  vector<simData> sd = simulate(pm,dat,rand,vf);
  for(size_t s=0;s<sd.size();s++) sd[s].computeValueJoin(vf,rand[s],pm);
  destroyValueFunctions(vf);
  return(sd);
}

class deductFinder {
public:
  data dat;
  parameters *pm;
  vector<randomDraws> *rand;
  vector<simData> sd;
  double (*avg)(const vector<simData> &);
  
  double operator()(double x) {
    sd = resetDeductAndSimulate(x, dat, (*pm), (*rand));
    return( (*avg)(sd));
  }
}; // for use with findEquivalent()


/**********************************************************************/
template<typename T, class F> 
T findEquivalent(T lo, T hi, const T val,F &f,
                 const double tol = 1e-12 )
/* 
   Returns x such that -tol<(f(x) - val)<tol
   x is searched for on the interval [lo,hi]. 
   T should have operations +,-, <, and /2
   F should be a class with operator () that returns f(x)
   
   Uses binary search to find x
   On entry must have sign(f(lo)-val)= -sign(f(hi)-val)
   For reliable results f should be monotonic on [lo,hi]
*/
{
  T fhi = f(hi)-val;  
  T flo = f(lo)-val;
  bool increasing = (fhi>flo);
  if (!((flo<=0 && fhi>=0) || (flo>=0 && fhi<=0)))  {
    cout << "findEquivalent:: root not bracketed." << endl;
    cout << " flo = " << flo << " fhi = " << fhi << endl;
    return(lo);
  } 
  while( ((fhi - flo)>tol || (fhi-flo)<-tol) 
         &&
         (hi - lo) >tol ) {
    T x = (lo+hi)/2;
    T fx = (f(x) - val);
    if (increasing) {
      if (fx>=0) {
        fhi = fx;
        hi = x;
      } else {
        flo = fx;
        lo = x;
      }
    } else {
      if (fx>=0) {
        flo = fx;
        lo = x;
      } else {
        fhi = fx;
        hi = x;
      }
    }    
  };
  return((lo+hi)/2);
}

// returns total out of pocket cost given spending in simData "dat" and plans in "oop"
vector<double> totalOop(const vector<simData> &dat, const vector<oopFunc> &oop) 
{
  vector<double> totOop;
  for(size_t s=0;s<dat.size();s++) {
    for(size_t i=0;i<dat[s].cost.dimSize(0);i++) {          
      for(size_t y=0;y<dat[s].cost.dimSize(2);y++) {
        if (dat[s].dat->in(i,y)) {
          int p = findPlan<int,1>(dat[s].dat->plan(i,y), dat[s].dat->up, dat[s].dat->nplan); 
          if (p<0 || p>= (int) oop.size()) {
            cout << "ERROR: totOop() p=" << p << " oop.size()=" << oop.size() << endl;
            exit(1);
          }
          totOop.push_back(oop[p].eval(dat[s].cumCost(i,dat[s].dat->T-1,y),0) );
        }
      }
    }
  } 
  return(totOop);
}


/**********************************************************************/
SEXP cfsim(SEXP listin)
{
  Rcpp::List input(listin);
  if (input.size()<2) {
    cout << "Data and x file names required" << endl;
    return(Rcpp::wrap(NA_REAL));
  }
  string dataName = Rcpp::as<std::string>(input["datafile"]);
  string xName = Rcpp::as<std::string>(input["xfile"]);
  Rcpp::NumericMatrix dic = input["dic"]; // deductible, icl, ccl for each counterfactual
  Rcpp::NumericMatrix rate = input["rate"]; // 4 rates for each counterfactual
  bool compStatics = Rcpp::as<bool>(input["compStatics"]);
  int nsim = Rcpp::as<int>(input["nsim"]); // simulations per obervation
  double pk = Rcpp::as<double>(input["probKeep"]); 
  double pk65 = Rcpp::as<double>(input["probKeep65"]);
  int nplanUse = Rcpp::as<int>(input["nplanUse"]);
  string cfgFile = Rcpp::as<std::string>(input["cfgFile"]);
  
  if (rate.nrow()!=dic.nrow() ||  rate.ncol()!=4 || dic.ncol()!=3) {
    cout << "Error: dic and/or rate invalid size.";
    exit(20);
  }  
  
  // read configuration
  cfg = config(cfgFile.c_str());
  const int nmix = dataNS::cfg.NMIX;
  const int nparam = dataNS::cfg.NPARAM;

  // load data
  data dat(dataName.c_str(),pk,pk65,nplanUse);

  vector<double> x0(nparam); // vector of parameters
  // read parameters from file
  ifstream infile;
  infile.open(xName.c_str(),ifstream::in);
  if (!infile.is_open()) {
    cout << "WARNING: Failed to open " << xName << endl 
         << "         Using default initial values." << endl;
  } else {
    for(int i=0;i<nparam;i++) {
      infile >> x0[i];
    }
    if (infile.fail() || infile.bad()) {
      cout << "Error while reading " << xName << endl;
      exit(10);
    }
  }
  infile.close();
  
  // Simulate 
  momentData md;
  parameters pm(x0);
  md.dat = &dat;
  md.rand = vector<randomDraws>(nsim);
  vector<double> g;
  for(int i=0;i<nsim;i++) {
    md.rand[i] = randomDraws(&dat,nmix,452 + i*3984852, 3925 + i*9789);
  }
  cout << "Objective = " << simMMobjective(x0, g, (void*) &md) << endl;
  // compute value function
  valueFunction *vf = createValueFunctions(dat,pm);
  vector<simData> sd = simulate(pm,dat,md.rand,vf);
  for(size_t s=0;s<sd.size();s++) sd[s].computeValueJoin(vf,md.rand[s],pm);
  destroyValueFunctions(vf);
  cout << "Finished simulating baseline" << endl;
    
  if (!compStatics) {
    // close the donut hole
    data j0d(dat);
    for (int i=0;i<j0d.N;i++) {
      j0d.joinweek[i] = 0;
    }
    j0d.valueT = j0d.T;
    for(size_t s=0;s<md.rand.size();s++) md.rand[s].zeroJoinOffset(); 
    vf = createValueFunctions(j0d,pm);
    vector<simData> simJoinZero = simulate(pm, j0d, md.rand, vf);
    destroyValueFunctions(vf);
    cout << "Finished simulating join zero" << endl;

    data nhd(j0d);
    for (int i=0;i<dat.nplan;i++) {
      const int keepCatOop = 1; // set to 0 to keep the catastrophic the same
      // in terms of total spending, 1 to keep same in terms of out of
      // pocket spending      
      int errCode= nhd.oop[i].closeHole(keepCatOop);    
      if (errCode) {
        cout << "Warning oop[" << i << "].closeHole() not initialized" << endl;
      }
    }
    nhd.resetXsolve();
    cout << "resetXsolve() complete" << endl;
    nhd.resetEeoyPrices(simJoinZero[0].cumCost);
    vf = createValueFunctions(nhd,pm);
    vector<simData> noHole = simulate(pm,nhd,md.rand,vf);
    for(size_t s=0;s<noHole.size();s++) noHole[s].computeValueJoin(vf,md.rand[s],pm);
    destroyValueFunctions(vf);
    cout << "Finished simulating no donut hole" << endl;

    vector<double> totOopJZnh = totalOop(simJoinZero, nhd.oop);
    vector<double> totOopNHh = totalOop(noHole, j0d.oop);
    
    data nded(j0d);
    for (int i=0;i<dat.nplan;i++) {
      int errCode= nded.oop[i].zeroDed();    
      if (errCode) {
        cout << "Warning oop[" << i << "].ded() not initialized" << endl;
      }
    }
    nded.resetXsolve();
    nded.resetEeoyPrices(simJoinZero[0].cumCost);
    cout << "resetXsolve() complete" << endl;
    vf = createValueFunctions(nded,pm);
    vector<simData> noDed = simulate(pm,nded,md.rand,vf);
    for(size_t s=0;s<noDed.size();s++) noDed[s].computeValueJoin(vf,md.rand[s],pm);
    destroyValueFunctions(vf);
    cout << "Finished simulating no deductible" << endl;    
    

    vector<double> totOopJZnd = totalOop(simJoinZero, nded.oop);
    vector<double> totOopNDd = totalOop(noDed, j0d.oop);
   

    // counterfactual plans with joinweek = 0 (standard benefit, no gap, etc)  
    vector<data> cf(rate.nrow());
    vector<randomDraws> stdRand=md.rand;
    vector<vector<simData> > scf(rate.nrow());
    for(size_t s=0;s<stdRand.size();s++) stdRand[s].zeroJoinOffset();
    for(int c=0;c<rate.nrow();c++) {
      bool singlePlan = false;
      for(int j=0;j<4;j++) {
        singlePlan = (singlePlan || rate(c,j)>0);
      }
      if (singlePlan) {
        cf[c] = dat;
        double r[4];
        double ded = dic(c,0), icl = dic(c,1), cat=dic(c,2);
        for(int j=0;j<4;j++) r[j] = rate(c,j);
        cf[c].nplan = 1;
        cf[c].valueT = cf[c].T;
        cf[c].up = array<int,1>(1);
        cf[c].oop = vector<oopFunc>(1);
        cf[c].up[0] = 0;
        for (size_t i=0;i<cf[c].ded.size();i++) {
          cf[c].ded[i] = ded;
          cf[c].icl[i] = icl;
          cf[c].cat[i] = cat;
          cf[c].plan[i] = 0;
          cf[c].joinweek[i] = 0;
        }
        for (int i=0;i<cf[c].nplan;i++) {
          cf[c].oop[i].setup(ded,icl,cat,r);
        } 
      } else { // use plans observed in data with rates changed by rate percent
        cf[c] = dat;
        for (int i=0;i<dat.nplan;i++) {
          if (cf[c].oop[i].ready()) {
            vector<double> rvec = cf[c].oop[i].s();
            double rnew[4];
            if (rvec.size()==3) {
            rnew[0]=-1;
            for (size_t j=0;j<rvec.size();j++)  rnew[j+1] = rvec[j]*(1+rate(c,j+1));
            } else if (rvec.size()==4) {
              for (size_t j=0;j<rvec.size();j++)  rnew[j] = rvec[j]*(1+rate(c,j));                        
            } else {
              cout << "ERROR: unexpected rate size" << endl;
              exit(1);
            }
            double ded = cf[c].oop[i].ded();
            double icl = cf[c].oop[i].icl();
            double cat = cf[c].oop[i].cat();
            cout << "old rates: " << rvec << endl;
            cout << " old dic: " << ded << " " << icl << " " << cat << endl;
            cout << "new rates: " << rnew << endl;            
            cf[c].oop[i].setup(ded,icl,cat,rnew);
            cout << "oop.s: " << cf[c].oop[i].s() << endl;
            cout << " new dic: " 
                 << cf[c].oop[i].ded() << " " 
                 << cf[c].oop[i].icl() << " " 
                 << cf[c].oop[i].cat() << endl << endl;
          }
        }
        for (size_t i=0;i<cf[c].ded.size();i++) {
          cf[c].joinweek[i] = 0;
        }
      }

      cf[c].resetXsolve();
      cf[c].resetEeoyPrices(simJoinZero[0].cumCost);
      vf = createValueFunctions(cf[c],pm);
      scf[c] = simulate(pm,cf[c],stdRand,vf);
      for(size_t s=0;s<scf[c].size();s++) 
        scf[c][s].computeValueJoin(vf,stdRand[s],pm);
      destroyValueFunctions(vf);
      cout << "Finished simulating " << c << " of " << rate.nrow() << endl;
    }
    vector<double> totOopSTDnh = totalOop(scf[0], cf[1].oop);
    vector<double> totOopSTDNHh = totalOop(scf[1], cf[0].oop);
    vector<double> totOopSTDnd(0);
    vector<double> totOopSTDNDd(0);
    if (cf.size()>2) {
      totOopSTDnd = totalOop(scf[0], cf[2].oop);
      totOopSTDNDd = totalOop(scf[2], cf[0].oop);    
    }
    // Return simulated data to R
    Rcpp::List out = Rcpp::List::create
      (Rcpp::Named("Observed",rData(dat)),
       Rcpp::Named("Simulated",rData(sd)),
       Rcpp::Named("SimJoinZero",rData(simJoinZero)),
       Rcpp::Named("NoHole",rData(nhd)),
       Rcpp::Named("SimNoHole",rData(noHole)),
       Rcpp::Named("NoDed",rData(nded)),
       Rcpp::Named("SimNoDed",rData(noDed)),
       Rcpp::Named("cf",rData(cf)),
       Rcpp::Named("scf",rData(scf)),
       Rcpp::Named("rdraw",rData(md.rand[0],pm)), 
       Rcpp::Named("totOop.jz.nh",totOopJZnh),
       Rcpp::Named("totOop.nh.h",totOopNHh),                   
       Rcpp::Named("totOop.jz.nd",totOopJZnd),
       Rcpp::Named("totOop.nd.d",totOopNDd),                   
       Rcpp::Named("totOop.std.nh",totOopSTDnh),
       Rcpp::Named("totOop.nhstd.h",totOopSTDNHh),
       Rcpp::Named("totOop.std.nd",totOopSTDnd),
       Rcpp::Named("totOop.ndstd.d",totOopSTDNDd)
       );
    return(out);
  } else {
    const int ncs = 1 + 5 + 5 + 5; // mu, sigma, p_om, lambda, delta
    const double dx = 0.05;
    data stdB = dat;
    vector<randomDraws> stdRand=md.rand;
    for(size_t s=0;s<stdRand.size();s++) stdRand[s].zeroJoinOffset();
    vector<vector<simData> > scf(ncs);
    parameters dpm(pm.xvec);
    int d = 0;
    double r[4];
    int c = 0;
    double ded = dic(c,0), icl= dic(c,1), cat=dic(c,2);
    for(int j=0;j<4;j++) r[j] = rate(c,j);
    stdB.nplan = 1;
    stdB.valueT = stdB.T;
    stdB.up = array<int,1>(1);
    stdB.oop = vector<oopFunc>(1);
    stdB.up[0] = 0;
    for (size_t i=0;i<stdB.ded.size();i++) {
      stdB.ded[i] = ded;
      stdB.icl[i] = icl;
      stdB.cat[i] = cat;
      stdB.plan[i] = 0;
      stdB.joinweek[i] = 0;
    }
    for (int i=0;i<stdB.nplan;i++) {
      stdB.oop[i].setup(ded,icl,cat,r);
    } 
    stdB.resetXsolve();
    stdB.resetEeoyPrices(dat.cumCost);
    dpm.resetIntegration();
    vf = createValueFunctions(stdB,pm);
    scf[d] = simulate(dpm,stdB,stdRand,vf);
    destroyValueFunctions(vf);
    d++;

    // mu
    for(int j=0;j<dpm.nThetaMix;j++) dpm.thetaD[j].mu *= (1-dx);
    dpm.resetIntegration();
    //cout << "Objective(mu) = " << simMMobjective(x0, g, (void*) &md) << endl;
    // compute value function
    vf = createValueFunctions(stdB,dpm);
    scf[d] = simulate(dpm,stdB,stdRand,vf);
    destroyValueFunctions(vf);
    cout << "Finished simulating dmu" << endl;
    for(int j=0;j<dpm.nThetaMix;j++) dpm.thetaD[j].mu = pm.thetaD[j].mu;
    d++;

    // sigma
    for(int j=0;j<dpm.nThetaMix;j++) dpm.thetaD[j].sig *= (1-dx);
    dpm.resetIntegration();
    //cout << "Objective(mu) = " << simMMobjective(x0, g, (void*) &md) << endl;
    // compute value function
    vf = createValueFunctions(stdB,dpm);
    scf[d] = simulate(dpm,stdB,stdRand,vf);
    destroyValueFunctions(vf);
    cout << "Finished simulating dsig" << endl;
    for(int j=0;j<dpm.nThetaMix;j++) dpm.thetaD[j].sig = pm.thetaD[j].sig;
    d++;
    
    // lambda
    for(int j=0;j<dpm.nThetaMix;j++) dpm.lambda[j] *= (1-dx);
    dpm.resetIntegration();
    //cout << "Objective(mu) = " << simMMobjective(x0, g, (void*) &md) << endl;
    // compute value function
    vf = createValueFunctions(stdB,dpm);
    scf[d] = simulate(dpm,stdB,stdRand,vf);
    destroyValueFunctions(vf);
    cout << "Finished simulating dlambda" << endl;
    for(int j=0;j<dpm.nThetaMix;j++) dpm.lambda[j] = pm.lambda[j];
    d++;
    
    // p_omega
    for(int j=0;j<dpm.nThetaMix;j++) dpm.omega[j].p *= (1-dx);
    dpm.resetIntegration();
    //cout << "Objective(mu) = " << simMMobjective(x0, g, (void*) &md) << endl;
    // compute value function
    vf = createValueFunctions(stdB,dpm);
    scf[d] = simulate(dpm,stdB,stdRand,vf);
    destroyValueFunctions(vf);
    cout << "Finished simulating dpom" << endl;
    for(int j=0;j<dpm.nThetaMix;j++) dpm.omega[j].p = pm.omega[j].p;
    d++;

    // delta
    dpm.delta *= (1-dx);
    dpm.resetIntegration();
    //cout << "Objective(mu) = " << simMMobjective(x0, g, (void*) &md) << endl;
    // compute value function
    vf = createValueFunctions(stdB,dpm);
    scf[d] = simulate(dpm,stdB,stdRand,vf);
    destroyValueFunctions(vf);
    cout << "Finished simulating ddelta" << endl;
    dpm.delta = pm.delta;      
    d++;

    for(double del=0;del<=1.0;del+=0.25) {
      dpm.delta = del;
      dpm.resetIntegration();
      //cout << "Objective(mu) = " << simMMobjective(x0, g, (void*) &md) << endl;
      // compute value function
      vf = createValueFunctions(stdB,dpm);
      scf[d] = simulate(dpm,stdB,stdRand,vf);
      destroyValueFunctions(vf);
      cout << "Finished simulating del " << del << endl;
      dpm.delta = pm.delta;      
      d++;
    }

    for(double del=0;del<=1.0;del+=0.25) {
      for(int j=0;j<dpm.nThetaMix;j++) dpm.omega[j].p = del;
      dpm.resetIntegration();
      //cout << "Objective(mu) = " << simMMobjective(x0, g, (void*) &md) << endl;
      // compute value function
      vf = createValueFunctions(stdB,dpm);
      scf[d] = simulate(dpm,stdB,stdRand,vf);
      destroyValueFunctions(vf);
      cout << "Finished simulating p_omega " << del << endl;
      for(int j=0;j<dpm.nThetaMix;j++) dpm.omega[j].p = pm.omega[j].p;
      d++;
    }
   
    Rcpp::List out = Rcpp::List::create
      (Rcpp::Named("Observed",rData(dat)),
       Rcpp::Named("Simulated",rData(sd)),
       Rcpp::Named("scf",rData(scf))
       );
    return(out);    
  }
}
/********************************************************************************/


SEXP valueGrid(SEXP listin)
{
  Rcpp::List input(listin);
  string xName = Rcpp::as<std::string>(input["xfile"]);
  Rcpp::NumericMatrix dic = input["dic"]; // deductible, icl, ccl for each counterfactual
  Rcpp::NumericMatrix rate = input["rate"]; // 4 rates for each counterfactual
  string cfgFile = Rcpp::as<std::string>(input["cfgFile"]);
  double riskAversion = Rcpp::as<double>(input["riskAversion"]);
  

  
  if (rate.nrow()!=dic.nrow() ||  rate.ncol()!=4 || dic.ncol()!=3) {
    cout << "Error: dic and/or rate invalid size.";
    exit(20);
  }  

  // read configuration
  cfg = config(cfgFile.c_str());
  const int nparam = dataNS::cfg.NPARAM;

  vector<double> x0(nparam); // vector of parameters
  // read parameters from file
  ifstream infile;
  infile.open(xName.c_str(),ifstream::in);
  if (!infile.is_open()) {
    cout << "WARNING: Failed to open " << xName << endl 
         << "         Using default initial values." << endl;
  } else {
    for(int i=0;i<nparam;i++) {
      infile >> x0[i];
    }
    if (infile.fail() || infile.bad()) {
      cout << "Error while reading " << xName << endl;
      exit(10);
    }
  }
  infile.close();
  
  parameters pm(x0);

  // compute value function
  int nplan = rate.nrow();
  int nLambda = dataNS::cfg.nLambda;
  int T = 365/dataNS::cfg.weekLength;
  int nv = nplan*pm.nThetaMix*T*nLambda;
  valueFunction *vf = new valueFunction[nv];
  spline1dinterpolant **v;
  vector<oopFunc> oop(nplan);
  v = new spline1dinterpolant*[nv];
  v[0] = new spline1dinterpolant[nv];
  for(int i=0;i<nv;i++) { 
    if (i>0) v[i] = v[i-1]+1;
    vf[i].v = v[i];
    vf[i].nparam = dataNS::cfg.NPARAM;
  }
  for(int i=0;i<nplan;i++) {
    double r[4];
    for(int j=0;j<4;j++) r[j] = rate(i,j);
    oop[i].setup(dic(i,0),dic(i,1),dic(i,2),r);
  }  
  vector<int> nx(nplan);
  vector< vector<double> > xsolve(nplan);
  for(int i=0;i<nplan;i++) {
    xsolve[i] = setXsolve(oop[i], 159420.0);
    nx[i] = xsolve[i].size();
  }
    
  computeValueFunction(v, &pm.omega[0], pm.delta, &pm.lambda[0], 
                       &pm.thetaV[0], &pm.thetaP[0], pm.nTheta, 
                       NULL, pm.nThetaMix, 
                       T, xsolve, nx, 
                       oop, nplan, 
                       pm.transitionProb, 
                       riskAversion,
                       dataNS::cfg.terminalV, 
                       pm.deltaTheta, 
                       pm.deltaOmega, 
                       pm.powDelta,
                       pm.powDeltaTheta,
                       pm.powDeltaOmega, 
                       pm.pOT
                       );
  delete[] v;
  
  // evaluate value functions on a grid
  int nc = 500;
  double maxc = 7000, dc = maxc/(nc-1);
  vector<double> cc,vcc;
  vector<int> week, plan, mix,lambda;
  for(double c = 0; c<=maxc; c+= dc) {
    for(int i=0;i<nplan;i++) {
      for(int t=0;t<T;t++) {
        for(int m=0;m<pm.nThetaMix;m++) {
          for (int li=0;li<nLambda;li++) {
            double ev;
            plan.push_back(i);
            week.push_back(t);
            cc.push_back(c);
            mix.push_back(m);
            lambda.push_back(li);
            if (riskAversion>0) {
              ev = vf[li + m*nLambda + t*pm.nThetaMix*nLambda + i*T*pm.nThetaMix*nLambda].eval(c,(double*) NULL);
            } else {
              ev = 0;
              for (int ni=0;ni<nLambda;ni++) {
                ev += pm.transitionProb(0,ni,li)*
                  vf[ni + m*nLambda + t*pm.nThetaMix*nLambda + i*T*pm.nThetaMix*nLambda].eval(c,(double*) NULL);
              }
            }
            vcc.push_back(ev);
          }
        }
      }
    }
  }  
  delete[] vf[0].v;
  delete[] vf;
  Rcpp::List out = Rcpp::List::create
    (Rcpp::Named("cumCost",cc),
     Rcpp::Named("value",vcc),
     Rcpp::Named("t",week),
     Rcpp::Named("plan",plan),
     Rcpp::Named("mix",mix),
     Rcpp::Named("lambda",lambda));
  return(out);
}
/********************************************************************************/

SEXP asymVar(SEXP listin)
{
  Rcpp::List input(listin);
  if (input.size()<2) {
    cout << "Data and x file names required" << endl;
    return(Rcpp::wrap(NA_REAL));
  }
  string dataName = Rcpp::as<std::string>(input["datafile"]);
  string xName = Rcpp::as<std::string>(input["xfile"]);
  double dx = Rcpp::as<double>(input["dx"]);
  string cfgFile = Rcpp::as<std::string>(input["cfgFile"]);
  int nsim = Rcpp::as<int>(input["nsim"]); // simulations per obervation
  
  // read configuration
  cfg = config(cfgFile.c_str());
  const int nmix = dataNS::cfg.NMIX;
  const int nparam = dataNS::cfg.NPARAM;
  const double pk = dataNS::cfg.probKeep_default;
  const double pk65 = dataNS::cfg.probKeep65_default;
  const int nplanUse = dataNS::cfg.nplanUse_default;

  // load data
  data dat(dataName.c_str(),pk,pk65,nplanUse);
  
  vector<double> x0(nparam); // vector of parameters
  // read parameters from file
  ifstream infile;
  infile.open(xName.c_str(),ifstream::in);
  if (!infile.is_open()) {
    cout << "WARNING: Failed to open " << xName << endl 
         << "         Using default initial values." << endl;
  } else {
    for(int i=0;i<nparam;i++) {
      infile >> x0[i];
    }
    if (infile.fail() || infile.bad()) {
      cout << "Error while reading " << xName << endl;
      exit(10);
    }
  }
  infile.close();
 
  // set up random draws
  momentData md;
  parameters pm(x0);
  md.dat = &dat;
  md.rand = vector<randomDraws>(nsim);
  vector<double> g;
  for(int i=0;i<nsim;i++) {
    md.rand[i] = randomDraws(&dat,nmix,452 + i*3984852, 3925 + i*9789);
  }
  // evaluate objective function
  vector<double> omoments = calcMoments(dat);
  array<double,3> smoments(x0.size(),omoments.size(), 5);
  array<double,2> df(x0.size(),omoments.size());
  valueFunction *vf;
  vf = createValueFunctions(dat,pm);
  vector<simData> sd = simulate(pm,dat,md.rand,vf);
  destroyValueFunctions(vf);
  vector<double> sm = calcMoments(sd);
  for(size_t j=0;j<x0.size();j++) {
    for(size_t k=0;k<sm.size();k++) {
      smoments(j,k,0) = sm[k];
    }
  }  
  
  data nhd(dat);
  array<double,3> nhmoments(x0.size(),omoments.size(), 5);
  for (int i=0;i<dat.nplan;i++) {
    const int keepCatOop = 1; // set to 0 to keep the catastrophic the same
    // in terms of total spending, 1 to keep same in terms of out of
    // pocket spending      
    int errCode= nhd.oop[i].closeHole(keepCatOop);    
    if (errCode) {
      cout << "Warning oop[" << i << "].closeHole() not initialized" << endl;
    }
  }
  nhd.resetXsolve();
  cout << "resetXsolve() complete" << endl;
  nhd.resetEeoyPrices(sd[0].cumCost);
  vf = createValueFunctions(nhd,pm);
  vector<simData> noHole = simulate(pm,nhd,md.rand,vf);
  for(size_t s=0;s<noHole.size();s++) noHole[s].computeValueJoin(vf,md.rand[s],pm);
  destroyValueFunctions(vf);
  sm = calcMoments(noHole);
  for(size_t j=0;j<x0.size();j++) {
    for(size_t k=0;k<sm.size();k++) {
      nhmoments(j,k,0) = sm[k];
    }
  }  
  
  cout << "Finished simulating no donut hole" << endl;

  for(size_t j=0;j<x0.size();j++) {
    vector<double> x = x0;
    double dxj;
    x[j] = x0[j] + dx;
    pm = parameters(x);
    vf = createValueFunctions(dat,pm);
    sd = simulate(pm,dat,md.rand,vf);
    sm = calcMoments(sd);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      smoments(j,k,1) = sm[k];
    }
    //
    vf = createValueFunctions(nhd,pm);
    noHole = simulate(pm,nhd,md.rand,vf);
    sm = calcMoments(noHole);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      nhmoments(j,k,1) = sm[k];
    }

    dxj = x[j] - x0[j];
    
    x[j] = x0[j] - dx;
    pm = parameters(x);
    vf = createValueFunctions(dat,pm);
    sd = simulate(pm,dat,md.rand,vf);
    sm = calcMoments(sd);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      smoments(j,k,2) = sm[k];
    }
    //
    vf = createValueFunctions(nhd,pm);
    noHole = simulate(pm,nhd,md.rand,vf);
    sm = calcMoments(noHole);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      nhmoments(j,k,2) = sm[k];
    }


    x[j] = x0[j] + 2*dx;
    pm = parameters(x);
    vf = createValueFunctions(dat,pm);
    sd = simulate(pm,dat,md.rand,vf);
    sm = calcMoments(sd);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      smoments(j,k,3) = sm[k];
    }
    //
    vf = createValueFunctions(nhd,pm);
    noHole = simulate(pm,nhd,md.rand,vf);
    sm = calcMoments(noHole);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      nhmoments(j,k,3) = sm[k];
    }


    x[j] = x0[j] - 2*dx;
    pm = parameters(x);
    vf = createValueFunctions(dat,pm);
    sd = simulate(pm,dat,md.rand,vf);
    sm = calcMoments(sd);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      smoments(j,k,4) = sm[k];
    }
    //
    vf = createValueFunctions(nhd,pm);
    noHole = simulate(pm,nhd,md.rand,vf);
    sm = calcMoments(noHole);
    destroyValueFunctions(vf);
    for(size_t k=0;k<sm.size();k++) {
      nhmoments(j,k,4) = sm[k];
    }


    
    int wid = 22;
    int prec = 16;
    for(size_t k=0;k<sm.size();k++) {        
      df(j,k) = ((smoments(j,k,1)-smoments(j,k,2))*2.0/3.0 - 
                 (smoments(j,k,3)-smoments(j,k,4))*1.0/12.0)/dxj;
      cout << "df[" << k << "]/dx[" << j << "]= " 
           << setw(wid) << setprecision(prec) << (smoments(j,k,1)-smoments(j,k,0))/(dxj)
           << " " 
           << setw(wid) << setprecision(prec) << (smoments(j,k,1)-smoments(j,k,2))/(2*dxj)
           << " " 
           << setw(wid) << setprecision(prec) << (smoments(j,k,0)-smoments(j,k,2))/(dxj)
           << " " 
           << setw(wid) << setprecision(prec) << df(j,k)
           << endl;
    }
  }
  
  // get weighting matrix and asymptotic variance of moments
  alglib::real_2d_array vm = varMoments(dat,omoments);
  alglib::real_2d_array W = weightMatrix(dat,omoments);
  vector<double> V, Wvec, smvec,dfvec, nhvec;
  for(size_t j=0;j<omoments.size();j++) {
    for(size_t k=0;k<omoments.size();k++) {
      V.push_back(vm(j,k));
      Wvec.push_back(W(j,k));
    }
  }
  for(size_t j=0;j<x0.size();j++) {
    for(size_t k=0;k<omoments.size();k++) {
      dfvec.push_back(df(j,k));
      for(size_t i=0;i<5;i++) {
        smvec.push_back(smoments(j,k,i));
        nhvec.push_back(nhmoments(j,k,i));
      }
    }
  }
  return(Rcpp::List::create(Rcpp::Named("fval",smvec),
                            Rcpp::Named("df",dfvec),
                            Rcpp::Named("vm",V),
                            Rcpp::Named("W",Wvec),
                            Rcpp::Named("nhvec",nhvec)));
}


/**********************************************************************/
SEXP validationFit(SEXP listin)
{
  Rcpp::List input(listin);
  if (input.size()<2) {
    cout << "Data and x file names required" << endl;
    return(Rcpp::wrap(NA_REAL));
  }
  string dataName = Rcpp::as<std::string>(input["datafile"]);
  string xName = Rcpp::as<std::string>(input["xfile"]);
  Rcpp::NumericMatrix dic = input["dic"]; // deductible, icl, ccl for each counterfactual
  Rcpp::NumericMatrix rate = input["rate"]; // 4 rates for each counterfactual
  int nsim = Rcpp::as<int>(input["nsim"]); // simulations per obervation
  double pk = Rcpp::as<double>(input["probKeep"]); 
  double pk65 = Rcpp::as<double>(input["probKeep65"]);
  int nplanUse = Rcpp::as<int>(input["nplanUse"]);
  string cfgFile = Rcpp::as<std::string>(input["cfgFile"]);
  
  if (rate.nrow()!=dic.nrow() ||  rate.ncol()!=4 || dic.ncol()!=3) {
    cout << "Error: dic and/or rate invalid size.";
    exit(20);
  }  
  
  // read configuration
  cfg = config(cfgFile.c_str());
  const int nmix = dataNS::cfg.NMIX;
  const int nparam = dataNS::cfg.NPARAM;
  
  // load data
  data dat2010(dataName.c_str(), pk, pk65, nplanUse, 0, false, 2010, 2010);
  
  vector<double> x0(nparam); // vector of parameters
  // read parameters from file
  ifstream infile;
  infile.open(xName.c_str(),ifstream::in);
  if (!infile.is_open()) {
    cout << "WARNING: Failed to open " << xName << endl 
         << "         Using default initial values." << endl;
  } else {
    for(int i=0;i<nparam;i++) {
      infile >> x0[i];
    }
    if (infile.fail() || infile.bad()) {
      cout << "Error while reading " << xName << endl;
      exit(10);
    }
  }
  infile.close();
  
  cout << "Read " << dat2010.N << " observations from 2010" << endl;
  cout << " dat2010.plan.size() = " << dat2010.plan.size() << endl;
  cout << " dat2010.age.size() = " << dat2010.age.size() << endl;
  cout << " dat2010.cost.size() = " << dat2010.cost.size() << endl;
  cout << " dat2010.cost.dimSize(0) = " << dat2010.cost.dimSize(0) << endl;
  cout << " dat2010.cost.dimSize(1) = " << dat2010.cost.dimSize(1) << endl;
  cout << " dat2010.cost.dimSize(2) = " << dat2010.cost.dimSize(2) << endl;
  cout << "----------------------------" << endl;

  // Simulate 
  momentData md;
  parameters pm(x0);

  md.dat=&dat2010;
  md.rand = vector<randomDraws>(nsim);
  cout << "1";
  for(int i=0;i<nsim;i++) {
    md.rand[i] = randomDraws(md.dat,nmix,452 + i*3984852, 3925 + i*9789);
  }
  cout << "2";
  valueFunction *vf = createValueFunctions(dat2010,pm);
  cout << "3";
  vector<simData> sd2010 = simulate(pm,dat2010,md.rand,vf);
  cout << "4";
  for(size_t s=0;s<sd2010.size();s++) sd2010[s].computeValueJoin(vf,md.rand[s],pm);
  cout << "5";
  destroyValueFunctions(vf);
  cout << "Finished simulating 2010" << endl;

  cout << "Read " << dat2010.N << " observations from 2010" << endl;
  cout << " dat2010.plan.size() = " << dat2010.plan.size() << endl;
  cout << " dat2010.age.size() = " << dat2010.age.size() << endl;
  cout << " dat2010.cost.size() = " << dat2010.cost.size() << endl;
  cout << " dat2010.cost.dimSize(0) = " << dat2010.cost.dimSize(0) << endl;
  cout << " dat2010.cost.dimSize(1) = " << dat2010.cost.dimSize(1) << endl;
  cout << " dat2010.cost.dimSize(2) = " << dat2010.cost.dimSize(2) << endl;


  data dat(dataName.c_str(),pk,pk65,nplanUse,0, false);
  data diffObs(dataName.c_str(),pk,pk65,nplanUse,0,true);
  data diffPlans(dataName.c_str(),pk*2,pk65*2,nplanUse,nplanUse,false);

  md.dat = &dat;
  vector<double> g;
  for(int i=0;i<nsim;i++) {
    md.rand[i] = randomDraws(md.dat,nmix,452 + i*3984852, 3925 + i*9789);
  }
  cout << "Objective = " << simMMobjective(x0, g, (void*) &md) << endl;
  // compute value function
  vf = createValueFunctions(dat,pm);
  vector<simData> sd = simulate(pm,dat,md.rand,vf);
  for(size_t s=0;s<sd.size();s++) sd[s].computeValueJoin(vf,md.rand[s],pm);
  destroyValueFunctions(vf);
  cout << "Finished simulating baseline" << endl;

  md.dat=&diffObs;
  for(int i=0;i<nsim;i++) {
    md.rand[i] = randomDraws(md.dat,nmix,452 + i*3984852, 3925 + i*9789);
  }
  vf = createValueFunctions(diffObs,pm);
  vector<simData> sdObs = simulate(pm,diffObs,md.rand,vf);
  for(size_t s=0;s<sdObs.size();s++) sdObs[s].computeValueJoin(vf,md.rand[s],pm);
  destroyValueFunctions(vf);
  cout << "Finished simulating different observations" << endl;

  md.dat=&diffPlans;
  for(int i=0;i<nsim;i++) {
    md.rand[i] = randomDraws(md.dat,nmix,452 + i*3984852, 3925 + i*9789);
  }
  vf = createValueFunctions(diffPlans,pm);
  vector<simData> sdPlans = simulate(pm,diffPlans,md.rand,vf);
  for(size_t s=0;s<sdPlans.size();s++) sdPlans[s].computeValueJoin(vf,md.rand[s],pm);
  destroyValueFunctions(vf);
  cout << "Finished simulating different plans" << endl;
  
  
  Rcpp::List out = Rcpp::List::create
    (
     Rcpp::Named("Observed",rData(dat)),
     Rcpp::Named("Simulated",rData(sd)),
     Rcpp::Named("DiffObs",rData(diffObs)),
     Rcpp::Named("SimDiffObs",rData(sdObs)),
     Rcpp::Named("DiffPlans",rData(diffPlans)),
     Rcpp::Named("SimDiffPlans",rData(sdPlans)),
     
     Rcpp::Named("Obs2010",rData(dat2010)),
     Rcpp::Named("Sim2010",rData(sd2010))
     );
  return(out);
}

