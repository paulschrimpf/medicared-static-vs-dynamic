# R code for reporting results of counterfactural simulations
#Rprof(filename="run.Rprof",memory.profiling=TRUE)
source("settings.R") ## some basic functions and settings
source("param.R")    ## for reading parameter files


dir.name <- "baseline-pc3"
outDir <- paste("figures/",dir.name,"-onlyGap/",sep="")
#paramfile <- "baseline-certain.est"
#cfgFile <- "baseline-certain.cfg"


#paramfile <- "../estimation/baseline-pc3/smmEstCMA_2014-09-07_23.30.29.txt"
#cfgFile <- "../estimation/baseline-pc3/baseline3.cfg"
#outDir <- "figures/baseline-pc3/"


if (!exists("outDir") || is.na(outDir)) {
  outDir <- paste("figures/",dir.name,"/",sep="")
}
if (!exists("paramfile") || is.na(paramfile)) {
  est.dir <- paste("../estimation/",dir.name,sep="")
  pfiles <- paste(est.dir,dir(est.dir,pattern="smmEstCMA(.+).txt"),sep="/")
  paramfile <- pfiles[order(pfiles,decreasing=TRUE)[1]]
  warning(sprintf("Using %s as paramfile",paramfile))
}
if (!exists("cfgFile") || is.na(cfgFile)) {
  est.dir <- paste("../estimation/",dir.name,sep="")
  cfgFile <- paste(est.dir,dir(est.dir,pattern="(.+).cfg$"),sep="/")
  if (length(cfgFile)>1) {
    stop("Cannot determine configuration file")
  }
}
## Settings
datafile <- "../estimation/data/medD-full-const.sl3"
nplanUse <- 5
probKeep <- 0.1
probKeep65 <- 0.1

## read configuration
cfg.txt <- readLines(cfgFile)
cfg.txt <- gsub("true","TRUE",cfg.txt)
cfg.txt <- gsub("false","FALSE",cfg.txt)
cfg <- new.env()
eval(parse(text=cfg.txt),envir=cfg)
nmix <- cfg$NMIX
NX <- cfg$NX + 1
nlambda <- cfg$nLambda
singleLambdaRatio <- cfg$singleLambdaRatio
terminalV <- cfg$terminalV
raEst <- cfg$riskAversion
omegaThetaDep <- cfg$omegaThetaDep
noUncertainty <- cfg$noUncertainty
## Simulate model in various ways to get closer to Saez setting
##  1. Our baseline dynamic model with uncertainty
##  2. Our model without uncertainty
##  3. "Remove delta" = set delta to 1
##  4. Remove both

## I use this file interactively, so time consuming portions of code
## that I do not want to re-run everytime are wrapped in if {} else {}
## blocks
if (!exists("result")) {
  ## run simulations using C++ library
  cflib <- "cf.so"
  if (is.loaded("cfsim")) dyn.unload(cflib)
  dyn.load(cflib)
  srate <- c(1.0, 0.25, 1.0, 0.07)  ## std benefit
  drate <- c(0.01, 0.05, 0.1, 0.15, 0.25)
  rate <- t(matrix(rep(srate,1+length(drate)),nrow=4))
  rate[,3] <- rate[,3]*c(1, 1-drate)
  dic <- t(matrix(rep(c(275, 2510, 4050),nrow(rate)) ## std benefit
                  , nrow=3))
  constTotCat <- FALSE
  if (constTotCat) {
    tcat <- dic[1,2] + (dic[1,3]-(dic[1,2]-dic[1,1]*rate[1,1])*rate[1,2] - dic[1,1]*rate[1,1])/rate[1,3]
    for(j in 2:nrow(dic)) {
      dic[j,3] <- (tcat - dic[j,2])*rate[j,3] + (dic[j,2]-dic[j,1]*rate[j,1])*rate[j,2] + dic[j,1]*rate[j,1]
    }
  }
  cfname <- c("standard benefit",
              sprintf("%.1f\\%% decrease",100*drate))
  tmp.file <- paste("x",as.numeric(Sys.time()),".tmp",sep="")
  pm <- parameters(paramfile,tmp.file)
  args <- list(datafile,tmp.file,nsim,rate,dic,compStatics=FALSE,probKeep,probKeep65,nplanUse,cfgFile)
  names(args) <- c("datafile","xfile","nsim","rate","dic","compStatics","probKeep","probKeep65",
                   "nplanUse","cfgFile")
  result <- .Call("cfsim",args)
  unlink(tmp.file)
} else {
  warning("Simulation result already exists, not re-running.")
}

if (!exists("simCost")) {
  ## put results into nice data structures
  id <- 1:length(result$Observed$plan)
  ## constant data
  conData <- data.frame(id,result$Observed[c("joinweek","plan","ded","icl","cat",
                                             "weight","age","rs")])
  conStd <- data.frame(id,result$cf[[1]][c("joinweek","plan","ded","icl","cat",
                                           "weight","age","rs")])
  nhData <- data.frame(id,result$NoHole[c("joinweek","plan","ded","icl","cat",
                                          "weight","age","rs")])
  ## observed cost data
  N <- length(id)
  T <- length(result$Observed$cost)/N
  idc <- as.vector(rep(1,T) %*% t(id))
  tc <- as.vector((1:T) %*% t(rep(1,N))) - 1
  jw <- as.vector(rep(1,T) %*% t(conData$joinweek))
  costData <- data.frame(idc,tc,result$Observed[c("cost","cumCost")],jw)
  names(costData)[1:2] <- c("id","t")
  costData <- costData[costData$t>=costData$jw,]
  costData <- costData[with(costData,order(id,t)),]
  ## simulated cost data
  nsim <- length(result$Simulated)
  simDF <- function(resultList,cdat) {
    jw <- as.vector(rep(1,T) %*% t(cdat$joinweek))
    df <- data.frame(idc,tc,resultList[[1]][c("cost","cumCost")],jw)
    df$sim <- 1
    if (nsim>1) {
      for(s in 2:nsim) {
        sim <- rep(s,length(idc))
        df <- rbind(df,
                    data.frame(idc,tc,resultList[[s]][c("cost","cumCost")],jw,sim))
      }
    }
    names(df) <- c(names(costData),"sim")
    df <- df[df$t>=df$jw,]
    df <- df[with(df,order(sim,id,t)),]
    return(df)   
  }

  scf <- list()
  simCost <- simDF(result$Simulated,conData)
  simNoHole <- simDF(result$SimNoHole,nhData)
  dcf <- list()
  for(j in 1:length(result$scf)) {
    dcf[[j]] <- simDF(result$scf[[j]],conStd)
  }
} else {
  warning("Simulation results already in data.frames, not re-creating.")
}


## create output directory if needed
if (!file.exists(outDir)) {
  dir.create(outDir,recursive=TRUE)
}

T <- max(costData$t)

totalSpend <- function(df,time=T) {
  subset(df,t==time)$cumCost
}
################################################################################
# Parameter table
bnames <- c("0","RS","65","nogap")
## reorder types by total spending
spend.full.ins <- 52* (pm$lam %*% pm$marginalProb) *exp(pm$mu + pm$sig^2/2)
ord <- order(spend.full.ins)
nd <- 3
filename <- paste(outDir,"param.tex",sep="")
#sink()
cat("$\\delta$ & ",format(pm$delta,digits=nd), file=filename,append=FALSE)
if (raEst) {
  cat("& risk aversion & ",format(pm$riskAversion,digits=nd), file=filename,append=TRUE)
}
cat(" \\\\ \n", file=filename,append=TRUE)
if (terminalV) {
  cat("& $\\delta_\\omega$ & ",format(pm$deltaOmega,digits=nd), file=filename,append=TRUE)
  cat("& $\\delta_\\theta$ & ",format(pm$deltaTheta,,digits=nd), file=filename,append=TRUE)
  cat(" \\\\ \n", file=filename,append=TRUE)  
}
cat(" ",sprintf("$j=%i$",1:nmix),sep=" & ",file=filename,append=TRUE); cat(" \\\\ \n",file=filename,append=TRUE)
for(j in 1:ncol(pm$bmix)) {
  cat(sprintf("$\\beta_{%s}$",bnames[j]),format(pm$bmix[ord,j],digits=nd),sep=" & ",file=filename,append=TRUE);
  cat(" \\\\ \n",file=filename,append=TRUE)
}
cat("$\\mu$",format(pm$mu[ord],digits=nd),sep=" & ",file=filename,append=TRUE);
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\sigma$",format(pm$sig[ord],digits=nd),sep=" & ",file=filename,append=TRUE);
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$p_\\omega$",format(pm$pom[ord],digits=nd),sep=" & ",file=filename,append=TRUE);
cat(" \\\\ \n",file=filename,append=TRUE)
for (i in 1:nlambda) {
  cat("$\\lambda$",format(pm$lam[ord,i],digits=nd),sep=" & ",file=filename,append=TRUE);
  cat(" \\\\ \n",file=filename,append=TRUE)
}
cat("\\\\ \n",file=filename,append=TRUE)
cat("\\multicolumn{6}{l}{$\\lambda$ transition probabilities} \\\\ \n",file=filename,append=TRUE);
for(i in 1:nlambda) {
  cat(" & " , format(pm$transitionProb[i,],digist=nd), sep=" & ", file=filename,append=TRUE);
  cat(" \\\\ \n",file=filename,append=TRUE)
}
cat("\\multicolumn{6}{l}{$\\lambda$ marginal probabilities} \\\\ \n",file=filename,append=TRUE);
for(i in 1:nlambda) {
  cat(" & " , format(pm$marginalProb[i],digist=nd), sep=" & ", file=filename,append=TRUE);
  cat(" \\\\ \n",file=filename,append=TRUE)
}
cat("\\\\ \n",file=filename,append=TRUE)
if (omegaThetaDep) {
  cat("$\\rho$ & ", format(pm$pOT, digits=nd), "\\\\ \n", file=filename,append=TRUE);
}

filename <- paste(outDir,"param2.tex",sep="")
if (NX==1) {
  xbj <- cbind(rep(1,nrow(conData))) %*% t(pm$bmix)
} else if (NX==3) {
  xbj <- cbind(rep(1,nrow(conData)), conData$rs, (conData$age==65)) %*% t(pm$bmix)
} else if (NX==4) {
  xbj <- cbind(rep(1,nrow(conData)), conData$rs, (conData$age==65), conData$nogap) %*% t(pm$bmix)
}
pj <- exp(xbj)/rowSums(exp(xbj))
if (NX>1) {
  dpdrs <- matrix(rep(pm$bmix[,2],nrow(pj)),nrow=nrow(pj), byrow=TRUE) * pj -
    pj*rowSums(matrix(rep(pm$bmix[,2],nrow(pj)),nrow=nrow(pj), byrow=TRUE) * pj)  
} else {
  dpdrs <- matrix(0,nrow=nrow(conData),ncol=nmix)
}
cat("$\\mathrm{P}(j)$", sprintf("%.2f",colMeans(pj[,ord])),sep=" & ",file=filename,append=FALSE)
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\mathrm{P}(j|\\text{age}=65)$", sprintf("%.2f",colMeans(pj[conData$age==65,ord])),sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\mathrm{P}(j|\\text{age}>65)$", sprintf("%.2f",colMeans(pj[conData$age>65,ord])),sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\mathrm{E}\\left[ \\frac{d \\mathrm{P}(j|\\text{rs})}{d \\text{rs}} \\right]$",
    sprintf("%.2f",colMeans(dpdrs[,ord])),sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)

cat("$\\mathrm{E}[\\theta|j]$", sprintf("%.2f",exp(pm$mu + pm$sig^2/2)[ord]),sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\mathrm{s.d.}(\\theta|j)$", sprintf("%.0f",(sqrt((exp(pm$sig^2)-1)*exp(2*pm$mu + pm$sig^2)))[ord]),
    sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\mathrm{E}[\\text{spend full ins.}|j]$",
    sprintf("%.2f",(52*pm$lam*exp(pm$mu + pm$sig^2/2))[ord]),sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\mathrm{E}[\\text{spend 0.25 coins.}|j]$",
    sprintf("%.2f",(52*pm$lam*((1-pm$pom) + pm$pom*0.75)*exp(pm$mu + pm$sig^2/2))[ord])
            ,sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)
cat("$\\mathrm{E}[\\text{spend no ins.}|j]$",
    sprintf("%.2f",(52*pm$lam*((1-pm$pom) + pm$pom*0.0)*exp(pm$mu + pm$sig^2/2))[ord])
    ,sep=" & ",file=filename,append=TRUE)
cat(" \\\\ \n",file=filename,append=TRUE)
if (raEst) {
  q <- weighted.quantile(costData$cumCost[costData$t==T],c(0.05,0.95),conData$weight)
  ceq <- log(sum(exp(pm$riskAversion*q)*c(0.5,0.5)))/pm$riskAversion
  cat(sprintf("\\multicolumn{6}{l}{{\\tiny{Certainty equivalent of 50/50 gamble between %.0f (5th \\%%tile) and %.0f (95th \\%%tile) annual spending = %.0f}}} \\\\ \n",
              q[1],q[2],ceq),              
      file=filename,append=TRUE)
}



################################################################################
# Elasticity table 
meanSpend <- lapply(dcf, function(x) { weighted.mean(totalSpend(x),conData$weight)} )

filename <- paste(outDir,"elasticities.tex",sep="")
cat("\\begin{tabular}{ccc}
 & Average Spending & Elasticity \\\\ \n",file=filename,append=FALSE)
cat(sprintf("Baseline & %.0f & --- \\\\ \n",meanSpend[[1]]),file=filename,append=TRUE)
#cat(sprintf("\\multicolumn{3}{l}{Reduced coinsurance rate by %.1f\\%%} \\\\ \n",
#            100*drate),file=filename,append=TRUE)
for(j in 2:length(meanSpend)) {
  cat(sprintf("%s & %.0f & %.3f \\\\ \n",cfname[j],meanSpend[[j]],
              (meanSpend[[j]]-meanSpend[[1]])/meanSpend[[1]]/(-drate[j-1]))
      ,file=filename,append=TRUE)
}
cat("\\end{tabular}\n",file=filename,append=TRUE)  


figData <- list()
################################################################################
# Histograms of spending
filename <- paste(outDir,"fit.pdf",sep="")
if (!file.exists(filename) || forceFig) {
  maxc <- 6000
  fd <- rbind(data.frame(x=subset(costData,t==T)$cumCost,w=conData$weight,group="observed"),
              data.frame(x=subset(simCost,t==T)$cumCost,w=conData$weight,group="estimated"))
  tab <- data.table(fd)[,list(p0=weighted.mean(x==0,w),
                              mean=weighted.mean(x,w),
                              sd=weighted.sd(x,w),
                              median=weighted.median(x,w)),by=group]
  fd$x[fd$x>maxc] <- maxc
  fitPlot <- ggplot(data=fd, aes(x=x,y=2*..count../sum(..count..),fill=group,weight=w)) + themeLE() + 
    stat_bin(breaks=seq(0.0,maxc+1,by=100),alpha=1.0,position="dodge") + 
      #geom_histogram(alpha=0.3,position="dodge",binwidth=maxc/nbin) +    
    scale_x_continuous(name="total spending") +
    scale_y_continuous(name="portion") + 
    scale_fill_manual(values=colors) + 
    theme(legend.position = "bottom",legend.title=element_blank()) +
    annotate("text",x=500,y=.06,
             label=sprintf(" observed: P(0)=%.3f median=%.2f, mean=%.2f, and sd=%.2f",
               tab$p0[1],tab$median[1],tab$mean[1],tab$sd[1]), size=4, hjust=0,color=colors[1]) + 
    annotate("text",x=500,y=.05,
             label=sprintf("estimated: P(0)=%.3f median=%.2f, mean=%.2f, and sd=%.2f",
               tab$p0[2],tab$median[2],tab$mean[2],tab$sd[2]), size=4, hjust=0,color=colors[2]) 
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot)
  dev.off()
  ## save plot data to create xls file for Liran
  figData <- c(figData, list(ggplot_build(fitPlot)$data[[1]]))
  names(figData)[length(figData)] <- "Fit.Histogram"
} else {
  warning(paste(filename,"exists, not recreating"))
}
    
## total spending near icl
filename <- paste(outDir,"bunchFit.pdf",sep="")
if (!file.exists(filename) || forceFig) {  
  fd <- rbind(data.frame(x=(subset(costData,t==T)$cumCost-conData$icl),w=conData$weight,
                         group="observed"),
              data.frame(x=(subset(simCost,t==T)$cumCost-conData$icl),w=conData$weight,
                         group="estimated"))
  xlim <- c(-1000,1000)
  xld <- xlim
  fd <- subset(fd,x>=xlim[1] & x<=xlim[2])
  bw <- 20
  fitPlot <- ggplot(data=fd, aes(x=x,y=..density..,fill=group, weight=w)) + themeLE() + 
    geom_histogram(alpha=1.0,position="dodge",binwidth=bw) +
    scale_x_continuous(name="total spending - ICL") +
    scale_fill_manual(values=colors) +
    scale_colour_manual(values=colors) +
    theme(legend.position = "bottom", legend.title=element_blank())  
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot)
  dev.off()
  ## save plot data to create xls file for Liran :(
  figData <- c(figData, ggplot_build(fitPlot)$data[1:2])  
  names(figData)[(length(figData)-1):length(figData)] <-
    c("Fit.Histogram.NearICL","Fit.Density.NearICL")
} else {
  warning(paste(filename,"exists, not recreating"))
}
################################################################################
# timing moments
## Timing moments
month.names <- c("January","February","March","April",
                 "May","June","July","August","September",
                 "October","November","December")
filename <- paste(outDir,"timing",month.names[12],".pdf",sep="")
if (!file.exists(filename) || forceFig) { 
  fd <- rbind(data.frame(x=(subset(costData,t==T)$cumCost-conData$icl),
                         group="observed",w=conData$weight),
              data.frame(x=(subset(simCost,t==T)$cumCost-conData$icl),
                         group="estimated",w=conData$weight))
  xlim <- c(-500,500)
  bins <- seq(-500,500,length.out=21)
  fd$bin <- NA
  for(i in 1:(length(bins)-1)) {
    fd$bin[fd$x>=bins[i] & fd$x<bins[i+1]] <- i
  }
  for (m in 6:12) {
    t <- T-(13-m)*4
    fd$y <- c((rowsum(costData$cost*(costData$t>=t),group=costData$id)>0),
              (rowsum(simCost$cost*(simCost$t>=t),group=costData$id)>0))
    y.obs <- rep(NA,length(bins)-1)
    y.sim <- y.obs
    for(i in 1:(length(bins)-1)) {
      y.obs[i] <- sum(fd$y[fd$group=="observed" & fd$bin==i],na.rm=TRUE) /
        sum(fd$bin==i & fd$group=="observed",na.rm=TRUE)
      y.sim[i] <- sum(fd$y[fd$group=="estimated" & fd$bin==i],na.rm=TRUE) /
        sum(fd$bin==i & fd$group=="estimated",na.rm=TRUE)
    }
    gd <- data.frame(x=(bins[2:length(bins)]+bins[1:(length(bins))-1])/2,
                     observed=y.obs,estimated=y.sim)
    filename <- paste(outDir,"timing",month.names[m],".pdf",sep="")
    fitPlot <- ggplot(data=gd, aes(x=x, y=observed,
                        color="observed", linetype="observed")) +
      themeLE() + 
      geom_line() + 
      geom_line(aes(y=estimated,color="estimated",linetype="estimated")) + 
      scale_x_continuous(name="total spending - ICL") +
      scale_y_continuous(name=paste("P(claim in ",
                         month.names[m],
                         " or later | total spending)",sep="")) + 
      scale_fill_manual(values=colors) +
      scale_colour_manual(values=colors) +
      theme(legend.position = "bottom", legend.title=element_blank())  
    printDev(filename,width=fdims[1],height=fdims[2])
    print(fitPlot)
    dev.off()
  }
  p.obs <- matrix(NA,nrow=13,ncol=2)
  p.sim <- matrix(NA,nrow=13,ncol=2)
  fd$bin <- NA
  fd$bin[fd$x>=-800 & fd$x<=-500] <- 1
  fd$bin[fd$x>=-150 & fd$x<=150] <- 2
  fd$bin[is.na(fd$bin)] <- 0
  for (m in 1:13) {
    m.ind <- costData$t==(m-1)*4 | costData$t==(m-1)*4+1 |
      costData$t==(m-1)*4+2 | costData$t==(m-1)*4+3
    y <- rowsum(costData$cost*m.ind,group=costData$id)>0
    for(i in 1:2) {
      p.obs[m,i] <- mean(y[fd$bin[fd$group=="observed"]==i])
    }
    m.ind <- simCost$t==(m-1)*4 | simCost$t==(m-1)*4+1 |
      simCost$t==(m-1)*4+2 | simCost$t==(m-1)*4+3
    y <- rowsum(simCost$cost*m.ind,group=simCost$id)>0
    for(i in 1:2) {
      p.sim[m,i] <- mean(y[fd$bin[fd$group=="estimated"]==i])
    }
  }
  gd <- data.frame(month=1:13,obs.pre=p.obs[,1],obs.kink=p.obs[,2],
                   sim.pre=p.sim[,1], sim.kink=p.sim[,2])
  fitPlot <- ggplot(data=gd, aes(x=month, y=obs.pre,
                    color="pre")) + 
    themeLE() + 
    geom_line(linetype=1) + 
    geom_line(aes(y=obs.kink,color="kink"),linetype=1) +
    geom_line(aes(y=sim.pre,color="pre"),linetype=2) +
    geom_line(aes(y=sim.kink,color="kink"),linetype=2) +
    scale_x_continuous(name="month") +
    scale_y_continuous(name="P(claim in month|total spending)") +
    scale_fill_manual(values=colors) +
    scale_colour_manual(values=colors) +
    theme(legend.position = "bottom", legend.title=element_blank())
  filename <- paste(outDir,"timingAll.pdf",sep="")
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot)
  dev.off()
} else {
  warning(paste(filename,"exists, not recreating"))
}


################################################################################
# Histogram of spending under each "elasticity" counterfactual
filename <- paste(outDir,"hist-rates.pdf",sep="")
if (!file.exists(filename) || forceFig) {
  maxc <- 8000
  fd <- data.frame(x=totalSpend(dcf[[1]]),w=conData$weight,group=cfname[1])
  for (j in 2:length(dcf)) {
    fd <- rbind(fd,data.frame(x=totalSpend(dcf[[j]]),w=conData$weight,group=cfname[j]))
  }
  fd$x[fd$x>maxc] <- maxc
  fitPlot <- ggplot(data=fd, aes(x=x,y=2*..count../sum(..count..),color=group,weight=w)) +
    themeLE() + 
      stat_bin(breaks=seq(0.0,maxc+1,by=100),alpha=1.0,geom="line") + 
      scale_x_continuous(name="total spending") +
      scale_y_continuous(name="portion") + 
      scale_colour_manual(values=colors, breaks=cfname) + 
      theme(legend.position = "bottom",legend.title=element_blank())
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot)
  dev.off()
} else {
  warning(paste(filename,"exists, not recreating"))
}
################################################################################
# Plot budget for each "elasticity" counterfactual
filename <- paste(outDir,"rates.pdf",sep="")
xhi <- 8000
budget <- function(rate, dic) {
  x <- c(0,dic[1], dic[2], dic[2] + (dic[3]-(dic[2]-dic[1])*rate[2]-dic[1]*rate[1])/rate[3],xhi)
  y <- c(0,dic[1]*rate[1], (dic[2]-dic[1])*rate[2]+dic[1]*rate[1],dic[3], (xhi-x[4])*rate[4] + dic[3])
  return(data.frame(x=x,y=y))
}

if (!file.exists(filename) || forceFig) {
  fd <- budget(rate[1,],dic[1,])
  fd$group <- cfname[1]
  for (j in 2:length(dcf)) {
    b <- budget(rate[j,],dic[j,])
    b$group <- cfname[j]
    fd <- rbind(fd,b)
  }
  fitPlot <- ggplot(data=fd, aes(x=x,y=y,color=group)) + geom_line() +
    themeLE() + 
      scale_colour_manual(values=colors, breaks=cfname) +
      scale_x_continuous(name="total spending") +
      scale_y_continuous(name="insuree spending") + 
      theme(legend.position = "bottom",legend.title=element_blank())
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot)
  dev.off()
} else {
  warning(paste(filename,"exists, not recreating"))
}


################################################################################
# Conditional elasticity
filename <- paste(outDir,"cond-elasticities.pdf",sep="")
if (!file.exists(filename) || forceFig) {
  library(data.table)
  bins <- seq(0,8000, by=500)
  spend <- totalSpend(dcf[[1]])
  bin <- sapply(spend,function(s) {
    return(bins[which.min(abs(bins-s))])
  })
  condMeanSpend <- lapply(dcf, function(x) {
    dt <- data.table(data.frame(spend=totalSpend(x),bin=bin),key="bin")
  return(as.data.frame(dt[,list(spend=mean(spend,na.rm=TRUE)),by=bin]))
  })
  
  fd <- data.frame()
  for(j in 2:length(condMeanSpend)) {
    ei <- ((totalSpend(dcf[[j]])-totalSpend(dcf[[1]])) /
           totalSpend(dcf[[1]]) / (-drate[j-1]))
    ei[is.infinite(ei)] <- NA
    dt <- data.table(data.frame(ei=ei,bin=bin),key="bin")
    fd <- rbind(fd,data.frame(spend=condMeanSpend[[1]]$bin,
                              dmean=condMeanSpend[[j]]$spend-condMeanSpend[[1]]$spend,
                              elasticity=((condMeanSpend[[j]]$spend-condMeanSpend[[1]]$spend) /
                                          condMeanSpend[[1]]$spend/(-drate[j-1])),
                              ei = dt[,list(spend=mean(ei,na.rm=TRUE)),by=bin]$spend,
                              drate=100*drate[j-1]))
  }
  fitPlot <- ggplot(data=fd, aes(x=spend,y=elasticity,colour=as.factor(drate))) + themeLE() + 
    geom_line() +
    geom_point(aes(y=ei,colour=as.factor(drate))) +
    scale_x_continuous(name="Baseline spending") +
    scale_y_continuous(name="Elasticity") + 
    scale_colour_manual(values=colors) +
    theme(legend.position = "bottom", legend.title=element_blank())  
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot)
  dev.off()
  
  filename <- paste(outDir,"cond-elasticities-zoom.pdf",sep="")
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot+ylim(c(-1,NA)))
  dev.off()
  

  filename <- paste(outDir,"cond-change.pdf",sep="")
  fitPlot <- ggplot(data=fd, aes(x=spend,y=dmean,colour=as.factor(drate))) + themeLE() + 
    geom_line() + 
    scale_x_continuous(name="Baseline spending") +
    scale_y_continuous(name="Change in mean spending") + 
    scale_colour_manual(values=colors) +
    theme(legend.position = "bottom", legend.title=element_blank())  
  printDev(filename,width=fdims[1],height=fdims[2])
  print(fitPlot)
  dev.off()

} else {
  warning(paste(filename,"exists, not recreating"))
}


save(figData,file=paste(outDir,"figures.Rdata",sep=""))

source("createXLS.R")
createXLS(outDir)
