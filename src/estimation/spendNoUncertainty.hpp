#ifndef _SPENDNOUNCERTAINTY_HPP
#define _SPENDNOUNCERTAINTY_HPP

#include<vector>
using std::vector;
#include "oopFunc.hpp"

vector<double> spendNoUncertainty // Calculate spending when there is no uncertainty 
(const vector<double> theta,
 const vector<double> omega,
 const oopFunc *oopf,
 const size_t t0=0);

#endif
