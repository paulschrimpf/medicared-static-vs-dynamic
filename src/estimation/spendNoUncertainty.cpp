#include <omp.h>
#include <vector>
#include <fstream>
#include <iostream> 
#include <iomanip>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <bitset>
#include <assert.h>
#include <limits.h>
using namespace std;

#include "oopFunc.hpp"
#define MAXT 52
vector<double> spendNoUncertaintyBruteForce // Calculate spending when there is no uncertainty 
(const vector<double> theta,
 const vector<double> omega,
 const oopFunc *oopf,
 const size_t t0=0) 
{
  size_t T = theta.size();
  vector<double> slope = oopf->s();
  vector<double> xcut = oopf->x();
  vector<double> ycut = oopf->y();
  vector<double> spend(T,0), val(slope.size(),0), totalSpend(slope.size(),0);
  size_t kink = slope.size()==3? 0:1; // index for where slope[kink]<slope[kink+1];
  int bestSeg = -1;
  double bestVal = -1e300;
#ifndef NDEBUG
  static size_t nkink = 0;
  static size_t nmarg = 0;
  static size_t maxmarg=0;
  assert(slope[kink]<=slope[kink+1]);
  assert(omega.size()==T);
#endif
  for(size_t seg=0;seg<slope.size();seg++) {
    val[seg] -= (ycut[seg]-slope[seg]*xcut[seg]);
    for (size_t t=t0;t<T;t++) {
      if (omega[t]>=slope[seg]*theta[t]) {
        val[seg] += -slope[seg]*theta[t];
        totalSpend[seg] += theta[t];
      } else {
        val[seg] += -omega[t];
      }
    }
    if (totalSpend[seg]>=xcut[seg] &&
        ((seg+1)>=xcut.size() || totalSpend[seg]<=xcut[seg+1])) {
      if (val[seg]>bestVal) {
        bestSeg = (int) seg;
        bestVal = val[seg];
      }
    }
  }
  // check kink value
  if (totalSpend[kink]>xcut[kink+1] && totalSpend[kink+1]<xcut[kink+1]) {
    vector<size_t> marginalClaim;
    double minSpend = 0;
    double minVal = 0;
    bitset<MAXT> bitmask; 
    bitset<MAXT> bestmask;
    double kinkValue = -1e300;
    for (size_t t=t0;t<T;t++) {
      if (omega[t]>slope[kink]*theta[t] && omega[t]<slope[kink+1]*theta[t]) {
        marginalClaim.push_back(t);
      } else {
        if (omega[t]<=slope[kink]*theta[t]) minVal -= omega[t];
        else if (omega[t]>=slope[kink+1]*theta[t]) minSpend += theta[t];
      }
    }
#ifndef NDEBUG
    assert(bitmask.size()<sizeof(unsigned long)*CHAR_BIT);
    assert(T <= bitmask.size());
    nkink++;
    nmarg += marginalClaim.size();
    maxmarg = marginalClaim.size()>maxmarg? marginalClaim.size():maxmarg;
    if ((nkink % 100)==0) {
      cout << "After " << nkink << " possible kinks, there are an average of " 
           << ((double) nmarg)/((double) nkink) << " marginal claims" << endl
           << " The max is " << maxmarg << endl;
    }
#endif
    // set unused bits to one
    for(size_t j=marginalClaim.size();j<MAXT;j++) bitmask[j] = 1; 
    // loop thorugh all combination of marginal claims
    //cout << " ---- " << endl;
    do {
      double totSpend = minSpend;
      double valk = minVal;
      for(size_t j=0;j<marginalClaim.size();j++) {
        //cout << bitmask[j];
        if (bitmask[j]) totSpend +=theta[marginalClaim[j]];
        else valk -= omega[marginalClaim[j]];
      }      
      //cout << endl;
      valk -= oopf->eval(totSpend,0);
      // check that combination satisfies first order condiotion
      bool valid = true;
      /*
      for(size_t j=0;j<marginalClaim.size();j++) {
        size_t t=marginalClaim[j];
        if (bitmask[j] ) {
          if (oopf->eval(theta[t],totSpend-theta[t])>omega[t]) {
            valid = false; 
            break;
          }
        } else {
          if (oopf->eval(theta[t],totSpend)<omega[t]) {
            valid = false;
            break;
          }
        }
      }
      */
      if (valid && valk>kinkValue) {       
        kinkValue = valk;
        bestmask=bitmask;        
      }
      if (bitmask.all()) break;
      bitmask = bitset<MAXT>(bitmask.to_ulong() + 1ULL); // increment
    } while (true);
    if (kinkValue>bestVal) {
      // fill in vector of spending 
      for (size_t t=0;t<t0;t++) spend[t] = 0;
      for (size_t t=t0;t<T;t++) {
        if (omega[t]>=slope[kink+1]*theta[t]) spend[t] = theta[t];
        else spend[t] = 0;
      }
      for(size_t j=0;j<marginalClaim.size();j++) {
        if (bestmask[j]) spend[marginalClaim[j]] = theta[marginalClaim[j]];
      }
      return(spend);
    } else { 
      if (bestSeg<0) { 
        cout << "ERROR: kinkValue = " << kinkValue << " but bestVal = " << bestVal 
             << " bestSeg = " << bestSeg << endl;
      }
    }
    
  } //else {
  if (bestSeg==-1) {
    cout << "ERROR: no segment had interior spending and kink is not possible" << endl;
    cout << "theta = ";
    for(size_t t=0;t<theta.size();t++ ) { 
        cout << setw(22) << setprecision(16) << theta[t];
    }
    cout << endl;
    
    cout << "omega = ";
    for(size_t t=0;t<omega.size();t++ ) { 
      cout << setw(22) << setprecision(16) << omega[t];
    }
    cout << endl;
    
    cout << "xcut = ";
    for(size_t t=0;t<xcut.size();t++ ) { 
      cout << setw(22) << setprecision(16) << xcut[t];
    }
    cout << endl;
    
    cout << "ycut = ";
    for(size_t t=0;t<ycut.size();t++ ) { 
      cout << setw(22) << setprecision(16) << ycut[t];
    }
    cout << endl;

    cout << "slope = ";
    for(size_t t=0;t<slope.size();t++ ) { 
      cout << setw(22) << setprecision(16) << slope[t];
    }
    
    cout << endl;
    
    
    exit(1);
  }
  
  for (size_t t=0;t<t0;t++) spend[t] = 0;
  for (size_t t=t0;t<T;t++) {
    if (omega[t]>=slope[bestSeg]*theta[t]) spend[t] = theta[t];
    else spend[t] = 0;
  }
  return(spend);
}

/********************************************************************************/
class omegaThetaT {
public: 
  double omega;
  double theta;
  size_t t;
};
bool comparator ( const omegaThetaT& l, const omegaThetaT& r)
{ return (l.theta>0 && (r.theta==0 || l.omega/l.theta > r.omega/r.theta)) ; }


class spendSolution {
public:
  double value;
  vector<double> d;
  size_t tstar;
  size_t rstar;
  bool integral;
};

spendSolution spendPartial
(const vector<omegaThetaT> ot,
 const double xlo, const double xhi, 
 const double slope, const double yvirt,
 const vector<int> fixedD) 
{
  spendSolution sol;
  sol.d = vector<double>(ot.size(),0.0);
  double sumSpend = 0;
  double sumOmega = 0;
  double maxSpend = 0;
  size_t rstar= -1;
  size_t r;
  double dstar;
  for(r=0;r<fixedD.size();r++) {
    if (fixedD[r]>0) {
      sol.d[r]=1.0;
      sumSpend += ot[r].theta;
      maxSpend += ot[r].theta;
    }
    else if (fixedD[r]<0) {
      sol.d[r]=0.0;
      sumOmega += ot[r].omega;
    } else {
      maxSpend += ot[r].theta;
    }
  }
  if (sumSpend>xhi) {
    //cout << "Problem is infeasible" << endl;
    sol.value = -1e300;
    return(sol);
  }
  if (maxSpend<xlo)  {
    //cout << "Problem is infeasible" << endl;
    sol.value = -1e300;
    return(sol);
  }

  sol.integral = true;
  for(r=0;r<ot.size();r++) {
    if (fixedD[r]==0) {
      double newSum = sumSpend + ot[r].theta;
      if (sumSpend>=xlo) {
        if (sumSpend<=xhi && ot[r].omega<slope*ot[r].theta) {
          dstar = 0;
          rstar = r;
          sol.d[rstar]=dstar;
          sol.tstar = ot[r].t;
          sumOmega += ot[rstar].omega*(1-dstar);
          sol.integral = true;
          break;
        } else if (newSum>xhi) {
          dstar = (xhi-sumSpend)/ot[r].theta;
          rstar = r;
          sol.d[rstar]=dstar;
          sol.tstar = ot[r].t;
          sumOmega += ot[rstar].omega*(1-dstar);
          sumSpend = xhi;
          sol.integral=false;
          break;
        }
      } else if (newSum>xlo && (ot[r].omega <slope*ot[r].theta)) { 
        dstar = (xlo-sumSpend)/ot[r].theta;
        rstar = r;
        sol.d[rstar]=dstar;
        sol.tstar = ot[r].t;
        sumOmega += ot[r].omega*(1-dstar);
        sumSpend = xlo;
        sol.integral = false;
        break;
      }
      sol.d[r]=1;
      sumSpend += ot[r].theta;
    }
  }
  for (r=rstar+1;r<ot.size();r++) {
    if (fixedD[r]==0) {
      sol.d[r]=0;
      sumOmega += ot[r].omega;
    }
  }
  sol.value = -(slope*sumSpend+yvirt) - sumOmega;
  sol.rstar = rstar;
  return(sol);
}

spendSolution branchBound
(const vector<omegaThetaT> ot,
 const double xlo, const double xhi, 
 const double slope, const double yvirt, 
 const vector<int> fixedD,
 const double lowerBound) 
{
  spendSolution sol = spendPartial(ot,xlo,xhi,slope,yvirt, fixedD);
  if (sol.value<lowerBound) return(sol); // cannot be optimal, so no need to meet integer constraints 
  if (sol.integral) return(sol); // found integer solution
  // else we have a non-integer solution with value higher that lowerBound, need to branch
  // rounding d either up or down gives feasible integer d, see if results in greater lower bound
  double val = 0;
  bool roundUp;
  double sumSpend = 0;
  double newLB;
  for(size_t r=0;r<ot.size();r++) {
    if (r!=sol.rstar) {
      sumSpend+=sol.d[r]*ot[r].theta;
      val += -(1-sol.d[r])*ot[r].omega;
    }
  }
  if (sumSpend<xlo) {
    roundUp = true;
    sumSpend += ot[sol.rstar].theta;
  } else {
    roundUp=false;
    val += -ot[sol.rstar].omega;
  }
  val += -(sumSpend*slope+yvirt);
#ifndef NDEBUG
  double ss = 0;
  for(size_t r=0;r<ot.size();r++) {
    ss+=sol.d[r]*ot[r].theta;
  }  
  //cout << ".";
  //cout << "brackets: " << xlo << " " << sumSpend << " " << xhi << " " << ss << endl;
  //cout << "val from rounding = " << val << endl;
#endif
  if (val>lowerBound) newLB = val; 
  else newLB = lowerBound;
  vector<int> newFixedD=fixedD;
  newFixedD[sol.rstar] = -1;
  spendSolution sol0 = branchBound(ot,xlo,xhi,slope,yvirt, newFixedD, newLB);
  newLB = sol0.value>newLB? sol0.value:newLB;
  newFixedD[sol.rstar] = 1;
  spendSolution sol1 = branchBound(ot,xlo,xhi,slope,yvirt, newFixedD, newLB);
  if (val >= sol0.value && val >= sol1.value) {
    sol.value = val;
    sol.d[sol.rstar] = 1.0*roundUp;
    sol.integral=true;
  } else if (sol0.value>val && sol0.value>=sol1.value) {
    sol = sol0;
  } else if (sol1.value>val && sol1.value>sol0.value) {
    sol = sol1;
  }
  return(sol);             
}

/* Calculate spending when there is no uncertainty using an integer
   linear programming algorithm. */
vector<double> spendNoUncertaintyILP 
(const vector<double> theta,
 const vector<double> omega,
 const oopFunc *oopf,
 const size_t t0=0) 
{
  size_t T = theta.size();
  vector<double> slope = oopf->s();
  vector<double> xcut = oopf->x();
  vector<double> ycut = oopf->y();
  vector<double> spend(T,0), val(slope.size(),0), totalSpend(slope.size(),0);
  size_t kink = slope.size()==3? 0:1; // index for where slope[kink]<slope[kink+1];
  int bestSeg = -1;
  double bestVal = -1e300;
#ifndef NDEBUG
  assert(slope[kink]<=slope[kink+1]);
  assert(omega.size()==T);
#endif
  for(size_t seg=0;seg<slope.size();seg++) {
    val[seg] -= (ycut[seg]-slope[seg]*xcut[seg]);
    for (size_t t=t0;t<T;t++) {
      if (omega[t]>=slope[seg]*theta[t]) {
        val[seg] += -slope[seg]*theta[t];
        totalSpend[seg] += theta[t];
      } else {
        val[seg] += -omega[t];
      }
    }
    if (totalSpend[seg]>=xcut[seg] &&
        ((seg+1)>=xcut.size() || totalSpend[seg]<=xcut[seg+1])) {
      if (val[seg]>bestVal) {
        bestSeg = (int) seg;
        bestVal = val[seg];
      }
    }
  }
  // check kink value
  if (totalSpend[kink]>xcut[kink+1] && totalSpend[kink+1]<xcut[kink+1]) {
    // sort claims by omega/theta in decreasing order
    vector<omegaThetaT> ot;
    for (size_t t=t0;t<T;t++) {
      omegaThetaT tmp;
      tmp.theta=theta[t];
      tmp.t=t;
      tmp.omega=omega[t];
      ot.push_back(tmp);
    }
    sort(ot.begin(), ot.end(), comparator);
    vector<int> fixedD(ot.size(),0);  
    // find solution ignoring discrete constraint
    spendSolution sol = spendPartial(ot,
                                     xcut[kink],xcut[kink+1],
                                     slope[kink], ycut[kink]-slope[kink]*xcut[kink], fixedD);
    double maxVal = sol.value; // upper bound of kink value
    bool left  = false;
    if (maxVal>bestVal) { // kink might be optimal, need to solve
      // solve on segment left of kink
      spendSolution solLeft = branchBound(ot,xcut[kink],xcut[kink+1],
                                          slope[kink],ycut[kink]-slope[kink]*xcut[kink],
                                          fixedD,
                                          bestVal);
      // solve on segment right of kink
      spendSolution solRight = branchBound(ot,xcut[kink+1],xcut[kink+2],
                                           slope[kink+1],ycut[kink+1]-slope[kink+1]*xcut[kink+1],
                                           fixedD,
                                           solLeft.value>bestVal? solLeft.value:bestVal);
      left = solLeft.value>solRight.value;
      sol = left? solLeft:solRight;
    }
    if (sol.value>bestVal) { // kink is optimal
#ifndef NDEBUG
      cout << "Using ILP solution to the ";
      if (left) cout << " left ";
      else cout << " right ";
      cout << " of the kink" << endl;
#endif
      for(size_t t=0;t<t0;t++) spend[t]=0.0; 
      for(size_t r=0;r<ot.size();r++) spend[ot[r].t] = sol.d[r]*ot[r].theta;
      return(spend);
    }
  }
  if (bestSeg==-1) {
    cout << "ERROR: no segment had interior spending and kink is not possible" << endl;
    exit(1);
  }
  for (size_t t=0;t<t0;t++) spend[t] = 0;
  for (size_t t=t0;t<T;t++) {
    if (omega[t]>=slope[bestSeg]*theta[t]) spend[t] = theta[t];
    else spend[t] = 0;
  }
  return(spend);
}
/********************************************************************************/

/*  Calculate spending when there is no uncertainty.  Only finds an
    approximate solution. Looks at each segment, and rounded up and
    down kink solution with partial spending.
 */
vector<double> spendNoUncertaintyApprox 
(const vector<double> theta,
 const vector<double> omega,
 const oopFunc *oopf,
 const size_t t0=0) 
{
  size_t T = theta.size();
  vector<double> slope = oopf->s();
  vector<double> xcut = oopf->x();
  vector<double> ycut = oopf->y();
  vector<double> spend(T,0), val(slope.size(),0), totalSpend(slope.size(),0);
  size_t kink = slope.size()==3? 0:1; // index for where slope[kink]<slope[kink+1];
  int bestSeg = -1;
  static int nCall = 0;
  double bestVal = -1e300;
#ifndef NDEBUG
  assert(slope[kink]<=slope[kink+1]);
  assert(omega.size()==T);
#endif
  nCall++;
  if (nCall>5000) {
    cout << ".";
    nCall = 0;
  }    
  for(size_t seg=0;seg<slope.size();seg++) {
    val[seg] -= (ycut[seg]-slope[seg]*xcut[seg]);
    for (size_t t=t0;t<T;t++) {
      if (omega[t]>=slope[seg]*theta[t]) {
        val[seg] += -slope[seg]*theta[t];
        totalSpend[seg] += theta[t];
      } else {
        val[seg] += -omega[t];
      }
    }
    if (totalSpend[seg]>=xcut[seg] &&
        ((seg+1)>=xcut.size() || totalSpend[seg]<=xcut[seg+1])) {
      if (val[seg]>bestVal) {
        bestSeg = (int) seg;
        bestVal = val[seg];
      }
    }
  }
  // check kink value
  if (totalSpend[kink]>xcut[kink+1] && totalSpend[kink+1]<xcut[kink+1]) {
    // sort claims by omega/theta in decreasing order
    vector<omegaThetaT> ot;
    for (size_t t=t0;t<T;t++) {
      omegaThetaT tmp;
      tmp.theta=theta[t];
      tmp.t=t;
      tmp.omega=omega[t];
      ot.push_back(tmp);
    }
    sort(ot.begin(), ot.end(), comparator);
    vector<int> fixedD(ot.size(),0);  
    // find solution ignoring discrete constraint
    spendSolution sol = spendPartial(ot,
                                     xcut[kink],xcut[kink+1],
                                     slope[kink], ycut[kink]-slope[kink]*xcut[kink], fixedD);
    double maxVal = sol.value; // upper bound of kink value
    if (maxVal>bestVal) { // kink might be optimal, need to solve
      // solve on segment left of kink
      double val = 0;
      double sumSpend = 0;
      for(size_t r=0;r<ot.size();r++) {
        if (r!=sol.rstar) {
          sumSpend+=sol.d[r]*ot[r].theta;
          val += -(1-sol.d[r])*ot[r].omega;
        }
      }
      double val0 = val - ot[sol.rstar].omega - oopf->eval(sumSpend,0);
      double val1 = val - oopf->eval(sumSpend + ot[sol.rstar].theta,0);
#ifndef NDEBUG 
      if (oopf->eval(sumSpend,0)!=sumSpend*slope[kink]+yvirt) {
        cout << "Error calculating out of pocket cost" << endl;
      }
#endif
      if (val0>val1) {
        if (val0>bestVal) {
          sol.d[sol.rstar]=0.0;
          for(size_t t=0;t<t0;t++) spend[t]=0.0; 
          for(size_t r=0;r<ot.size();r++) spend[ot[r].t] = sol.d[r]*ot[r].theta;
          return(spend);
        }
      } else {
        if (val1>bestVal) {
          sol.d[sol.rstar]=1.0;
          for(size_t t=0;t<t0;t++) spend[t]=0.0; 
          for(size_t r=0;r<ot.size();r++) spend[ot[r].t] = sol.d[r]*ot[r].theta;
          return(spend);
        }
      }
    } // maxVal>bestVal
  } // kink optimum not possible
  if (bestSeg==-1) {
    cout << "ERROR: no segment had interior spending and kink is not possible" << endl;
    exit(1);
  }
  for (size_t t=0;t<t0;t++) spend[t] = 0;
  for (size_t t=t0;t<T;t++) {
    if (omega[t]>=slope[bestSeg]*theta[t]) spend[t] = theta[t];
    else spend[t] = 0;
  }
  return(spend);
}

vector<double> spendNoUncertainty // Calculate spending when there is no uncertainty 
(const vector<double> theta,
 const vector<double> omega,
 const oopFunc *oopf,
 const size_t t0=0) 
{
  //vector<double> spend = spendNoUncertaintyILP(theta,omega,oopf,t0);
  vector<double> spend = spendNoUncertaintyApprox(theta,omega,oopf,t0);
  //vector<double> spend =  spendNoUncertaintyBruteForce(theta,omega,oopf,t0);
#ifndef NDEBUG
  vector<double> spendbf =  spendNoUncertaintyBruteForce(theta,omega,oopf,t0);
  bool error = false;
  double val = 0, vbf=0;
  double ts = 0, tbf=0;
  for(size_t t=0;t<spend.size();t++) {
    val -= omega[t]*(spend[t]<0.001);
    ts += spend[t];
    vbf -= omega[t]*(spendbf[t]<0.001);
    tbf += spendbf[t];
    if (abs(spend[t]-spendbf[t])>0.001) {
      cout << "ERROR: spend!=spendbf " << spend[t] << " " << spendbf[t] << " " << theta[t] << " " 
           << t << endl; 
      error = true;
    }
  }
  if (error) {
    cout << "sum(omega) = " << val << " " << vbf << endl;
    cout << "sum(spend) = " << ts << " " << tbf << endl;
    cout << "value = " << val - oopf->eval(ts,0) << " " << vbf -oopf->eval(tbf,0) << endl;
    cout << endl;
  } else {
    //cout << "brute force and ILP solutions are the same " << endl;
  }
#endif 
  return(spend);
}


#undef MAXT
