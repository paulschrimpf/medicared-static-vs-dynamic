#include <omp.h>
#include <vector>
#include <fstream>
#include <iostream> 
#include <iomanip>
#include<algorithm>
#include <ctime>
#include <chrono>
#include <bitset>
#include <assert.h>
#include <limits.h>

#include "oopFunc.hpp"
#include "omegaDist.hpp"
#include "data.hpp"
#include "computeValueFunction.hpp"
#include "spendNoUncertainty.hpp"

#include "simMM.hpp"
#include "fastSpline.hpp"
using namespace fastSpline;

//using namespace std;
using std::cout;
using std::vector;
using std::endl;
using std::setprecision;
using std::setw;
using std::flush;
using std::ofstream;
using std::ios;
using std::bitset;

// Binary search for plan in up
// from: http://stackoverflow.com/a/446327
template<class Iter, class T>
Iter binary_find(const Iter &begin, const Iter &end, T val)
{
  // Finds the lower bound in at most log(last - first) + 1 comparisons
  Iter i = std::lower_bound(begin, end, val);

  if (i != end && *i == val)
    return i; // found
  else
    return end; // not found
}
int findPlan(const int p,   double *up,const int nplan) {
  double *loc = binary_find(up, up+nplan,(double) p);
  return((int) (loc - up));
}

template<typename T, size_t dim> 
int findPlan(const int p,const array<T,dim> &up,const int nplan)
{
  typename vector<T>::const_iterator loc = binary_find(up.begin(), up.end(), p);
  return((int) (loc - up.begin()));
}

template int findPlan<int,1>(const int, const array<int,1> &, const int);

// class randomDraws
randomDraws::randomDraws() {
  N = T = M = 0;
}

randomDraws::randomDraws(const data *din, const size_t mm,
                         const size_t s1, const size_t s2) 
{
  alglib::hqrndstate state;
  alglib::hqrndseed(s1,s2,state);
  dat = din;
  N = dat->N;
  T = dat->T;
  M = mm;
  Y = dat->Years;
  
  th = array<double,4>(N,T,M,Y);
  mixU = array<double,2>(N,Y);
  pOm = array<double,4>(N,T,M,Y);
  Om = array<double,4>(N,T,M,Y);
  transU = array<double,4>(N,T,M,Y);
  lam = array<double,5>(N,T,M,Y,dataNS::cfg.nLambda);
  jw = vector<int>(N);
  pclaim = 0.25;
  for(size_t i=0;i<N;i++) for(size_t y=0;y<Y;y++) {
    mixU(i,y) = alglib::hqrnduniformr(state);
    jw[i] = (int) (alglib::hqrnduniformr(state)*4);
    for(size_t t=0;t<T;t++) {
      for (size_t m=0;m<M;m++) {
        do {
          th(i,t,m,y) = alglib::hqrndnormal(state);
        } while (dataNS::cfg.maxThetaQuantile<1.0 && 
                 th(i,t,m,y) > alglib::invnormaldistribution(dataNS::cfg.maxThetaQuantile));        
        pOm(i,t,m,y) = alglib::hqrnduniformr(state);
        Om(i,t,m,y) = alglib::hqrnduniformr(state);
        for(int lambdaState=0;lambdaState<dataNS::cfg.nLambda;lambdaState++) 
          lam(i,t,m,y,lambdaState) = alglib::hqrnduniformr(state);
        transU(i,t,m,y) = alglib::hqrnduniformr(state);
      }
    }
  }
  
}


int randomDraws::type(const size_t i, const size_t y, const parameters &pm) const {
  double sump  = 0;
  int t = 0;
  //
  vector<double> cut(pm.nThetaMix,0);
  double sumExpCut = 0;
  for(int j=0;j<pm.nThetaMix;j++) {
    cut[j] = pm.betaMixP(j,0);
    for(size_t k=1;k < pm.betaMixP.dimSize(1); k++) {
      cut[j] += dat->X(i,k-1,y)*pm.betaMixP(j,k);
    }
    cut[j] = exp(cut[j]);
    sumExpCut += cut[j];
  }
  while (sump <= mixU(i,y) && t<(int) M) sump += cut[t++]/sumExpCut; //pm.thetaMixP[t++];    
  //printf("u=%.3f, m=%d\n",mixU[i],t);
  return(t-1);
}
  
inline double randomDraws::theta(const size_t i, const size_t t, const size_t m,  
                                 const size_t y, const parameters &pm) const {
  return(exp(pm.thetaD[m].sig*th(i,t,m,y) + pm.thetaD[m].mu));
}

inline double randomDraws::omega(const size_t i, const size_t t, const size_t m,
                                 const size_t y, const parameters &pm) const {
  if (pm.pOT==0) {
    return(pOm(i,t,m,y)<pm.omega[m].p? Om(i,t,m,y):1);
  } else {
    double pom = pm.omega[m].p;
    double thet = this->theta(i,t,m,y,pm);
    double p = (2*pom-1 + exp(pm.pOT*thet))/(1+exp(pm.pOT*thet));
    return(pOm(i,t,m,y)<p? Om(i,t,m,y):1);
  }
}

inline bool randomDraws::lambda(const size_t i, const size_t t, const size_t m,
                                const size_t y, const size_t lambdaState, 
                                const parameters &pm) const {
  return(lam(i,t,m,y,lambdaState) < pm.lambda(m,lambdaState));
}

inline size_t randomDraws::lambdaTrans
(const bool claim, const size_t lastLambda, const size_t i, const size_t t, const size_t m, 
 const size_t y, const parameters &pm) const {
  double u = transU(i,t,m,y);
  if (t>0) {
    double sumP = pm.transitionProb(claim,0,lastLambda);
    size_t newLambda = 0; 
    while(((int) newLambda)<(dataNS::cfg.nLambda-1) && u>sumP ) { 
      newLambda++;
      sumP += pm.transitionProb(claim, newLambda,lastLambda);
    }
    return(newLambda);
  } else { // t==0
    // draw from stationary distribution
    size_t newLambda = 0;
    double sumP = pm.startProb[newLambda];
    while(((int) newLambda)<(dataNS::cfg.nLambda-1) && u>sumP ) { 
      newLambda++;
      sumP += pm.startProb[newLambda];
    }
    return(newLambda);    
  }
}


inline int randomDraws::joinOffset(const size_t i, const size_t y) const {
  return(dat->age(i,y)==65? jw[i]:0);
}
inline int randomDraws::claim(const size_t i, const size_t t, const size_t m, 
                              const size_t y, const parameters &pm, 
                              const double cut) const {
  return(this->omega(i,t,m,y,pm)>=cut);
}

double randomDraws::weight(const size_t i, const size_t y,
                           const parameters &pm) const {
  return(1.0);
}

void randomDraws::zeroJoinOffset() {
  cout << "WARNING: randomDraws() setting joinOffset to zero" << endl;
  std::fill(jw.begin(),jw.end(),0);
}

//  end class randomDraws

// class simData
simData::simData() {}
  
simData::simData(const data &d) {
  dat = &d;
  cost = array<double,3>(dat->N,dat->T,dat->Years);  
  cumCost = array<double,3>(dat->N,dat->T,dat->Years);
  delay = array<double,3>(dat->N,dat->T,dat->Years);  
  lambdaIndex = array<int,3>(dat->N,dat->T,dat->Years);  
  weight = array<double,2>(dat->N,dat->Years);
  totalDelay = array<double,2>(dat->N,dat->Years);
  for(size_t i=0;i<cumCost.size();i++) delay[i] = cost[i] = cumCost[i] = 0; 
  vJoin =  array<double,2>(dat->N,dat->Years);
#ifdef DEBUG
  vtreat = array<double,3>(dat->N,dat->T,dat->Years);  
  vno = array<double,3>(dat->N,dat->T,dat->Years);  
  theta = array<double,3>(dat->N,dat->T,dat->Years);  
#endif
}

simData& simData::operator=(const data &d) {
  dat = &d;
  cost = array<double,3>(dat->N,dat->T,dat->Years);
  cumCost = array<double,3>(dat->N,dat->T,dat->Years);
  weight = array<double,2>(dat->N,dat->Years);
  delay = array<double,3>(dat->N,dat->T,dat->Years);  
  lambdaIndex = array<int,3>(dat->N,dat->T,dat->Years);  
  totalDelay = array<double,2>(dat->N,dat->Years);
  for(size_t i=0;i<cumCost.size();i++) delay[i] = cost[i] = cumCost[i] = 0; 
  vJoin =  array<double,2>(dat->N,dat->Years);
#ifdef DEBUG
  vtreat = array<double,3>(dat->N,dat->T,dat->Years);  
  vno = array<double,3>(dat->N,dat->T,dat->Years);  
  theta = array<double,3>(dat->N,dat->T,dat->Years);  
#endif
  return(*this);
}

void simData::computeValueJoin(const valueFunction* vf, 
                               const randomDraws &rand, 
                               const parameters &pm) {  
  vJoin =  array<double,2>(dat->N,dat->Years);
  if (vf==NULL || dataNS::cfg.noUncertainty) {
    cout << "WARNING (computeValueJoin) vf==NULL or cfg.noUncertainty, not doing anything" << endl;
    return;
  }
#pragma omp parallel for default(shared)
  for(int i=0;i<dat->N;i++)  {
    for(int y=0;y<dat->Years;y++) if(dat->in(i,y)) {
      int mix = rand.type(i,y,pm);
      int lambdaIndex = rand.lambdaTrans(false,0,i,0,mix,y,pm);
      int t = dat->joinweek(i,y) + rand.joinOffset(i,y);
      int vt = (dat->T-1-t);
      int p = findPlan(dat->plan(i,y),dat->up,dat->nplan);    
      vJoin(i,y) =  vf[lambdaIndex + mix*dataNS::cfg.nLambda + 
                       vt*dataNS::cfg.NMIX*dataNS::cfg.nLambda 
                       + p*(dat->T)*dataNS::cfg.NMIX*dataNS::cfg.nLambda].eval
        (0.0, (double*) NULL);
    }
  }
} 
// end class simData              

/************************************************************************/
valueFunction* createValueFunctions(const data &dat, const parameters &pm) 
{
  if (dataNS::cfg.noUncertainty) {
    static bool warned = false;
    if (!warned) {
      cout << "WARNING (createValueFunction): no value function needed without uncertainty," << endl
           << "        returning NULL" << endl;
      warned=true;
    }
    return(NULL);
  }
  int nv = dat.nplan*dat.valueT*pm.nThetaMix*dataNS::cfg.nLambda;
  valueFunction *vf = new valueFunction[nv];
  spline1dinterpolant **v;
  v = new spline1dinterpolant*[nv];
  v[0] = new spline1dinterpolant[nv];
  for(int i=0;i<nv;i++) { 
    if (i>0) v[i] = v[i-1]+1;
    vf[i].v = v[i];
    vf[i].nparam = dataNS::cfg.NPARAM;
  }
  computeValueFunction(v,&pm.omega[0], pm.delta, &pm.lambda[0], 
                       &pm.thetaV[0], &pm.thetaP[0], pm.nTheta, 
                       NULL, pm.nThetaMix, 
                       dat.valueT, dat.xsolve, dat.nx, 
                       dat.oop, dat.nplan, 
                       pm.transitionProb,
                       pm.riskAversion, 
                       dataNS::cfg.terminalV, 
                       pm.deltaTheta, 
                       pm.deltaOmega, 
                       pm.powDelta,
                       pm.powDeltaTheta,
                       pm.powDeltaOmega, pm.pOT);
  delete[] v;
  return(vf);
}

void destroyValueFunctions(valueFunction *vf) {
  if (vf !=NULL) {
    if (vf[0].v !=NULL) delete[] vf[0].v;
    delete[] vf;
  }
}
/************************************************************************/



vector<simData> simulateNoUncertainty
(const parameters&pm, 
 const data &dat,
 const vector<randomDraws> &rand // size = number of simulations
 ) 
{
  int T = dat.T;
  vector<simData> sdat(rand.size()); 
  for(size_t s = 0;s<rand.size();s++) sdat[s] = dat;
#pragma omp parallel default(shared) 
  {
#pragma omp parallel for
    for(int i=0;i<dat.N;i++) {
      for(int y=0;y<dat.Years;y++) if (dat.in(i,y)) {
        int p = findPlan(dat.plan(i,y),dat.up,dat.nplan);     
        const oopFunc *oopf = &(dat.oop[p]);
        for(size_t s=0;s<rand.size();s++) {
          int mix = rand[s].type(i,y,pm);
          bool claimLast = false;
          size_t lambdaIndex = rand[s].lambdaTrans(claimLast,lambdaIndex,i,0,mix,y,pm); 
          // lambdaTrans with t=0 will sample from marginal distribution correctly
          vector<double> theta(T, 0.0), omega(T, 0.0);
          for(int t=(dat.joinweek(i,y)+rand[s].joinOffset(i,y)); t<dat.T; t++) {
            lambdaIndex = rand[s].lambdaTrans(claimLast,lambdaIndex,i,t,mix,y,pm);
            sdat[s].lambdaIndex(i,t,y) = lambdaIndex;
            if (rand[s].lambda(i,t,mix,y,lambdaIndex,pm)) {
              theta[t] = rand[s].theta(i,t,mix,y,pm);
              omega[t] = rand[s].omega(i,t,mix,y,pm)*theta[t];
            }
          }
          vector<double> spend = spendNoUncertainty(theta, omega, oopf, 
                                                    dat.joinweek(i,y) + rand[s].joinOffset(i,y));
          double x = 0;
          for(int t=(dat.joinweek(i,y)+rand[s].joinOffset(i,y)); t<dat.T; t++) {
            sdat[s].cost(i,t,y) = spend[t];
            x = (sdat[s].cumCost(i,t,y) = spend[t] + x);
          }          
          sdat[s].weight(i,y) = dat.weight(i,y);
        } // for(s)
      } // for(y)
    } // for(i)
  } // parallel region
  return(sdat);
}

/************************************************************************/
vector<simData> simulate
(const parameters&pm, 
 const data &dat,
 const vector<randomDraws> &rand, // size = number of simulations
 valueFunction *value  // value functions for each plan and theta mixture component
 )
{
  if (dataNS::cfg.noUncertainty) {
    return(simulateNoUncertainty(pm, dat, rand));    
  }

  vector<simData> sdat(rand.size()); 
  for(size_t s = 0;s<rand.size();s++) sdat[s] = dat;
  int T = dat.T;
#pragma omp parallel default(shared) 
  {
#pragma omp parallel for
    for(int i=0;i<dat.N;i++) {
      for(int y=0;y<dat.Years;y++) if (dat.in(i,y)) {
        int p = findPlan(dat.plan(i,y),dat.up,dat.nplan);     
        //for (p=0;p<dat.nplan;p++) if (dat.up[p]==dat.plan[i]) break;
        const oopFunc *oopf = &(dat.oop[p]);
        for(size_t s=0;s<rand.size();s++) {
          int mix = rand[s].type(i,y,pm);
          size_t lambdaIndex = 0; // lambdaTrans with t=0 will sample from marginal distribution correctly
          bool claimLast = false;
          for(int t=(dat.joinweek(i,y)+rand[s].joinOffset(i,y)); t<dat.T; t++) {
            lambdaIndex = rand[s].lambdaTrans(claimLast,lambdaIndex,i,t,mix,y,pm);
            sdat[s].lambdaIndex(i,t,y) = lambdaIndex;
            sdat[s].totalDelay(i,y) = 0;
            if (rand[s].lambda(i,t,mix,y,lambdaIndex,pm)) {
              double x;          
              valueFunction *vtm1 = NULL;
              if ( t <(T-1)) { // >
                int vt = T-1-(t+1);
                vtm1 = value + (0 + mix*dataNS::cfg.nLambda + 
                                vt*pm.nThetaMix*dataNS::cfg.nLambda +
                                p*dat.valueT*pm.nThetaMix*dataNS::cfg.nLambda);
              }
              if (t==0) x = 0;
              else x = sdat[s].cumCost(i,t-1,y);
              double theta = rand[s].theta(i,t,mix,y,pm);
              //double omega = rand[s].omega(i,t,mix,pm)*theta;
              double pay = oopf->eval(theta,x);
              double vtreat = -pay, vno =0;
              if (t<dat.T-1) {
                if (pm.riskAversion<=0) {
                  // no risk aversion value function is v(x,lambda),
                  // need to calculate E[v(x,newlambda) |
                  // currentlambda, fill or not]
                  for (int li=0;li<dataNS::cfg.nLambda;li++) {
                    vtreat += pm.delta*pm.transitionProb(1,li,lambdaIndex) 
                      * vtm1[li].eval(x+theta, (double*) NULL);
                    vno    += pm.delta*pm.transitionProb(0,li,lambdaIndex)
                      * vtm1[li].eval(x, (double*) NULL);
                  }
                } else { // with risk aversion value function is
                         //  E[v(x,new lambda) | x, current lambda]  
                  vtreat += pm.delta*vtm1[lambdaIndex].eval(x+theta,(double*) NULL);
                  vno += pm.delta*vtm1[lambdaIndex].eval(x,(double*) NULL);
                }
              }
              if (dataNS::cfg.terminalV) {
                int vt = (T-1) - t;
                double vnever = vno, vdelay = vno;
                double omega = rand[s].omega(i,t,mix,y,pm)*theta;
                double delaySpend = theta*oopf->eoyPrice()*pm.powDeltaTheta[vt+1]; //pow(pm.deltaTheta,vt+1);
                vnever +=  -omega/(1 - pm.delta*pm.deltaOmega);
                vdelay +=  -omega*(1 - pm.powDelta[vt+1]*pm.powDeltaOmega[vt+1])/(1 - pm.delta*pm.deltaOmega) + 
                  -delaySpend*pm.powDelta[vt+1];
                if (vtreat>=vnever && vtreat>=vdelay) {
                  sdat[s].cost(i,t,y) = theta;
                  sdat[s].cumCost(i,t,y) = theta + x;
                  claimLast = true;
                } else {
                  sdat[s].cost(i,t,y) = 0;
                  sdat[s].cumCost(i,t,y) = x;
                  if (vdelay>=vnever) {
                    sdat[s].delay(i,t,y) = theta;
                    sdat[s].totalDelay(i,y) += theta*pm.powDeltaTheta[vt+1];
                  }
                  claimLast = false;
                }
              } else {
                double cut = (-vtreat + vno)/theta;
#ifdef DEBUG
                sdat[s].vtreat(i,t,y)=vtreat;
                sdat[s].vno(i,t,y)=vno;
                sdat[s].theta(i,t,y) = theta;                
#endif
                if (rand[s].claim(i,t,mix,y,pm,cut)) { //(vtreat>=vno) {
                  sdat[s].cost(i,t,y) = theta;
                  sdat[s].cumCost(i,t,y) = theta + x;
                  claimLast = true;
                } else {
                  sdat[s].cost(i,t,y) = 0;
                  sdat[s].cumCost(i,t,y) = x;
                  claimLast = false;
                }            
              }
            } else {
              sdat[s].cumCost(i,t,y) = t==0? 0.0: sdat[s].cumCost(i,t-1,y);
              claimLast = true; // no event -> same lambda transition as when treating
#ifdef DEBUG
              sdat[s].vtreat(i,t,y)=0;
              sdat[s].vno(i,t,y)=0;
              sdat[s].theta(i,t,y)=0;
#endif

            }// endif(lambda)          
          } // endfor(t)
          //wght=1.0;
          sdat[s].weight(i,y) = dat.weight(i,y);
        } // endfor(s)
      } // endfor(y)
    } // endfor(i)
  } // end parallel region
  return(sdat);
}


#define _USE_MATH_DEFINES
inline double kernel(const double x,const double h) {
  double k = x/h;
  k*=k;
  //return(k<1? .75*(1.0-k)/h:0.0);
  return(exp(-0.5*k)/(h*sqrt(2*M_PI)));  
}
#undef _USE_MATH_DEFINES

////////////////////////////////////////////////////////////////////////////////
const double censorSpend = 1.5e4;
const int censorWeek = 12;
const size_t ncdf = 9;
const double cdfcut[] = {100, 250, 500, 1000, 1500, 2000, 3000, 4000 , 6000};
size_t nMoments() {
  return(dataNS::cfg.nbinEM + 4 + dataNS::cfg.NX  + ncdf +
         (1 + dataNS::cfg.useSpendAutoCov)*(dataNS::cfg.nlags+1) + 
         dataNS::cfg.nbinTiming*(dataNS::cfg.nMonthlyProb-
                                 1*(dataNS::cfg.diffTiming==2))*(dataNS::cfg.diffTiming==0? 1.0:0.5)
         + 6*dataNS::cfg.useWeeks + 4*dataNS::cfg.useExcessJanuary);
}
const double spendScale = 1e3;

vector<double> iMoments(const array<double,3> &cc, const double mu, const double icl, 
                        const int age, const int joinweek, const double ded,
                        const int i, const int y, const array<double,3> &X,
                        const array<double,3> &cost, 
                        const double totalDelay, const double iclLastY, const bool inLastY) 
{
  vector<double> mi(nMoments(),0.0);
  const int T = cost.dimSize(1);
  double tot = cc(i,T-1,y);
  size_t m = 0;
  // total spending moments
  if (tot>=-1e-6) {    
    tot = tot>censorSpend? censorSpend:tot;
    mi[m++] = tot/spendScale;
    mi[m++] = (tot - mu)*(tot-mu)/(spendScale*spendScale);
    mi[m++] = (tot<=1e-6);    
    // approx covariance of total spending and spending in first half of year     
    mi[m++] = T/2<joinweek? 
      0.0 : 
      (tot-cc(i,T/2,y) - 0.5*mu)*(cc(i,T/2,y) - 0.5*mu)/(spendScale*spendScale);
    for(size_t k=0;k<X.dimSize(1);k++) {
      mi[m++] = (tot-mu)*X(i,k,y)/spendScale;
    }
    for(size_t k=0;k<ncdf;k++) {
      mi[m++] = (tot<cdfcut[k]);
    }
    // excess mass moments
    int nbin = dataNS::cfg.nbinEM;
    double lim = dataNS::cfg.limEM;
    double ccd = cc(i,T-1,y) - icl;
    if (ccd>=-lim && ccd<lim) {
      int bin = (int) (ccd+lim)/(2*lim)*nbin;
      mi[m+bin] = 1;
    }
    m += nbin;
    // autocovariance of weekly spending
    if (dataNS::cfg.useSpendAutoCov) {
      for (int lag=0;lag<=dataNS::cfg.nlags;lag++) {
        double meanSpend = tot; 
        meanSpend = meanSpend>censorSpend? censorSpend:meanSpend;
        meanSpend/=T;
        for(int t = lag;t<T;t++) {
          double spend=cost(i,t,y);
          spend = spend>censorSpend? censorSpend:spend;
          double spendLag = cost(i,t-lag,y);
          spendLag = spendLag>censorSpend? censorSpend:spendLag;
          mi[m + lag] += (spend - meanSpend)*(spendLag-meanSpend);
        }
        mi[m+lag] /= (T-lag);      
      }
      m += (dataNS::cfg.nlags+1);
    }
    
    // P(claim t & claim t-j)
    for(int lag=0;lag<=dataNS::cfg.nlags;lag++) {
      for(int t=lag;t<T;t++) {
        mi[m+lag] += (cost(i,t,y)>1e-6 && cost(i,t-lag,y)>1e-6);
      }
      mi[m+lag] /= (T-lag);
    }
    m += (dataNS::cfg.nlags+1);
    
    // "timing" moments, i.e. P(claim in month or later | total spending)
    if (dataNS::cfg.useMonthlyProb) {
      int nbin = (dataNS::cfg.diffTiming>0? 1:dataNS::cfg.nbinTiming);
      int bin = -1;              
      if (nbin==2 || nbin==1) {
        ccd = cc(i,T-1,y) - icl;
        if (ccd>=-800 && ccd <= -500) bin = 0;
        else if (ccd>=-150 && ccd<=150) bin = 1;
        else bin=-1;
      } else {
        double lim= dataNS::cfg.limTiming;
        ccd = cc(i,T-1,y) - icl;
        if (ccd>=-lim && ccd<lim) {
          bin = (int) (ccd+lim)/(2*lim)*nbin;
        } else bin = -1;
      }
      if (bin>=0) {
        //bool claim=false; // P(claim in month or later)
        for(int month=0;month<dataNS::cfg.nMonthlyProb;month++) {
          bool claim=false; // P(claim in month)
          if (claim==false) {
            for(int week=0;week<4;week++) {
              int t = T-4*month-week-1;
              if (cost(i,t,y)>1e-6) { 
                claim =true;
                break;
              }
            }
          }
          if (claim) {
            if (dataNS::cfg.diffTiming==0) {
              mi[m+bin+month*nbin] = 1;
            } else if (dataNS::cfg.diffTiming==1) {
              mi[m+month*nbin] = (bin==0? 1:-1);
            } else if (dataNS::cfg.diffTiming==2) {
              if (month<(dataNS::cfg.nMonthlyProb-1)) mi[m+month*nbin] += (bin==0? 1:-1);
              if (month>0) mi[m+(month-1)*nbin] += -1.0*(bin==0? 1:-1);
            }
          }
        }
      } // end if bin>=0
      m += dataNS::cfg.nbinTiming*(dataNS::cfg.nMonthlyProb-
                                   1*(dataNS::cfg.diffTiming==2))*(dataNS::cfg.diffTiming==0? 1.0:0.5);
    } // end if useMonthlyProb 
    else {
      if (dataNS::cfg.nMonthlyProb>0) {
        std::cout << "Error: useMonthlyProb==false, but nMonthlyProb>0" << std::endl;
        exit(2);
      }
    }
  } // end if tot>=0
  
  // join month moments
  if (dataNS::cfg.useWeeks) {
    if (age==65 && joinweek<40 && joinweek>3) {
      int q = 0; // join quarter
      if (joinweek>13 && joinweek<30) q = 1;
      else if (joinweek>=30) q = 2;
      int dedind =(ded>0); // deductible indicator      
      int t=joinweek;
      while(t<=joinweek+censorWeek && t<T 
            && cost(i,t,y)<=0) t++;
      t -= joinweek;
      mi[m+q+3*dedind] = t;
    }
    m += 6;
  }
  
  // excess January spending
  if (dataNS::cfg.useExcessJanuary && y>0 && inLastY) {
    double tot = cc(i,T-1,y-1);
    if (tot>iclLastY && tot<iclLastY+500) {
      mi[m] += (cc(i,3,y) + totalDelay);
      mi[m+1] +=  (cc(i,25,y)-cc(i,7,y) );
    } else if (tot>(iclLastY-2000) && tot<(iclLastY-500)) {
      mi[m+2] += (cc(i,3,y) + totalDelay);
      mi[m+3] += (cc(i,25,y)-cc(i,7,y) );
    }
    m += 4;
  }
  
  
  return(mi);
}

vector<double> calcMoments(const data &dat) {
  double mu=0,sumW=0;
  vector<double> moments(nMoments(),0);
  // means
#pragma omp parallel for default(shared) collapse(2) reduction(+:mu,sumW)
  for(int i=0;i<dat.N;i++) for(int year=0;year<dat.Years;year++) {
      if (dat.in(i,year) ) {
        double y = dat.cumCost(i,(dat.T-1),year);
        if (y>=-1e-6) {
          y = y>censorSpend? censorSpend:y;
          sumW += dat.weight(i,year);
          mu += y*dat.weight(i,year);
        }
      }
    }
  mu /= sumW;
  
#pragma omp parallel default(shared) 
  {
    vector<double> tm(nMoments(),0); // sum of moments for this thread
#pragma omp for collapse(2)
    for(int i=0;i<dat.N;i++) {
      for(int y=0;y<dat.Years;y++) {
        if (dat.in(i,y) ) {
          vector<double> mi = iMoments(dat.cumCost,mu,dat.icl(i,y),
                                       dat.age(i,y), dat.joinweek(i,y), dat.ded(i,y),
                                       i,y,dat.X,dat.cost,
                                       0.0, 
                                       y>0? dat.icl(i,y-1):0, 
                                       y>0 && dat.in(i,y-1)
                                       );
          double w = dat.weight(i,y);
          // add moments 
          for(size_t j=0;j<tm.size();j++) {
            tm[j] += mi[j]*w;                     
          }
        } // if(dat.in(i,y))
      } // for(y)
    }  // for(i)
    // sum moments across threads
#pragma omp critical 
    {
      for(size_t j=0;j<tm.size();j++) moments[j] += tm[j]/sumW;
    }
  } // end parallel region
  //for(size_t j=0;j<moments.size();j++) cout << "moments[j] " << moments[j] << endl;
  return(moments);

}

/********************************************************************************/
// calcMoments for simulated data
vector<double> calcMoments(const vector<simData> &sdat) {
  double mu=0,sumW=0;
  const data *dat = sdat[0].dat;
  vector<double> moments(nMoments(),0);
  // means
#pragma omp parallel for default(shared) collapse(2) reduction(+:mu,sumW)
  for(int i=0;i<dat->N;i++) for(int year=0;year<dat->Years;year++) {
      if (dat->in(i,year) ) {
        for(size_t s=0;s<sdat.size();s++) {
          double y = sdat[s].cumCost(i,(dat->T-1),year);
          if (y>=-1e-6) {
            y = y>censorSpend? censorSpend:y;
            sumW += sdat[s].weight(i,year);
            mu += y*sdat[s].weight(i,year);
          }
        }
      }
    }
  mu /= sumW;

#pragma omp parallel default(shared) 
  {
    vector<double> tm(nMoments(),0); // sum of moments for this thread
#pragma omp for collapse(2)
    for(int i=0;i<dat->N;i++) {
      for(int y=0;y<dat->Years;y++) {
        if (dat->in(i,y) ) {
          for(size_t s=0;s<sdat.size();s++) {
            vector<double> mi = iMoments(sdat[s].cumCost,mu,dat->icl(i,y),
                                         dat->age(i,y), dat->joinweek(i,y), dat->ded(i,y),
                                         i,y,dat->X,sdat[s].cost, 
                                         y>0? sdat[s].totalDelay(i,y-1):0,
                                         y>0? dat->icl(i,y-1):0, 
                                         y>0 && dat->in(i,y-1)
                                         );
            double w = sdat[s].weight(i,y);
            // add moments 
            for(size_t j=0;j<tm.size();j++) {
              tm[j] += mi[j]*w;                     
            }
          }
        } // if(dat.in(i,y))
      } // for(y)
    }  // for(i)
    // sum moments across threads
#pragma omp critical 
    {
      for(size_t j=0;j<tm.size();j++) moments[j] += tm[j]/sumW;
    }
  } // end parallel region
  //for(size_t j=0;j<moments.size();j++) cout << "moments[j] " << moments[j] << endl;
  return(moments);
}

alglib::real_2d_array varMoments(const data &dat, const vector<double> &moments) { 
  alglib::real_2d_array vm;
  double sumW = 0;
  double mu = moments[0];
  vm.setlength(moments.size(),moments.size());
  for(size_t i=0;i<moments.size();i++) {
    for(size_t j=0;j<moments.size();j++) vm(i,j) = 0.0; 
  }  
#pragma omp parallel default(shared)
  {
    double tsumW = 0;
    alglib::real_2d_array tvm = vm;
#pragma omp for collapse(2)
    for(int i=0;i<dat.N;i++) {
      for(int y=0;y<dat.Years;y++) {
        if (dat.in(i,y) ) {
          vector<double> mi = iMoments(dat.cumCost,mu,dat.icl(i,y),
                                       dat.age(i,y), dat.joinweek(i,y), dat.ded(i,y),
                                       i,y,dat.X,dat.cost,
                                       0.0, 
                                       y>0? dat.icl(i,y-1):0, 
                                       y>0 && dat.in(i,y-1)
                                       );
          double w = dat.weight(i,y);
          // add moments 
          for(size_t j=0;j<mi.size();j++) {
            for(size_t k=0;k<mi.size();k++) {
              tvm(j,k) += (mi[j] - moments[j])*(mi[k]-moments[k])*w;
            }
          }
          tsumW += w;
        } // if(dat.in(i,y))
      } // for(y)
    }  // for(i)
    // sum moments across threads
#pragma omp critical 
    {
      for(size_t j=0;j<moments.size();j++) {
        for(size_t k=0;k<moments.size();k++) vm(j,k) += tvm(j,k);
        //vm(j,j) += tvm(j,j);
      }  
      sumW += tsumW;
    }
  } // end parallel region
  for(size_t j=0;j<moments.size();j++) {
    for(size_t k=0;k<moments.size();k++) vm(j,k)/=sumW;
  }
  return(vm);
}

vector<double> calcJWratio(const data &dat) {
  double sw=0;
  vector<double> swj(6,0.0);
  for(int i=0;i<dat.N;i++) for(int year=0;year<dat.Years;year++) {
      if (dat.in(i,year) && dat.cumCost(i,dat.T-1,year)>=-1e-6 ) { 
        sw += dat.weight(i,year);
        if ( dat.age(i,year)==65 && dat.joinweek(i,year)<40 && dat.joinweek(i,year)>3) {
          int ded = dat.ded(i,year)>0? 1:0;
          int q = 0; // join quarter
          if (dat.joinweek(i,year)>13 && dat.joinweek(i,year)<30) q = 1;
          else if (dat.joinweek(i,year)>=30) q = 2;
          swj[q+3*ded] += dat.weight(i,year);
        }
      }
    }
  vector<double> jwRatio(6,0.0);
  for(size_t i=0;i<jwRatio.size();i++) jwRatio[i] = sw/swj[i];
  return(jwRatio);
}

alglib::real_2d_array weightMatrix(const data &dat, const vector<double> moments) {
  alglib::real_2d_array vm = varMoments(dat,moments);
  alglib::ae_int_t info;
  alglib::matinvreport rep;
    
  ofstream file;  
  file.open("V.csv",ios::out);
  for(size_t i=0;i<nMoments();i++) {
    for(size_t j = 0;j<nMoments();j++) {
      file << setprecision(16) << vm(i,j);
      if (j<(nMoments()-1)) file << ",";
      else file << endl;
    }
  }
  file.close();

  if (dataNS::cfg.regularize>0) 
    for(size_t i=0;i<nMoments();i++) vm(i,i) += dataNS::cfg.regularize;
  
  alglib::spdmatrixinverse(vm, info, rep);
  if (info!=1) cout << "WARNING alglib::spdmatrixinverse info = " 
                    << info << endl;  
  /*
  file.open("W.csv",ios::out);
  for(size_t i=0;i<nMoments();i++) {
    for(size_t j = 0;j<nMoments();j++) {
      file << setprecision(16) << vm(i,j);
      if (j<(nMoments()-1)) file << ",";
      else file << endl;
    }
  }
  file.close();
  */
  if (dataNS::cfg.useWeeks) {
    vector<double> jwRatio = calcJWratio(dat);
    // overweight join month moments
    for(size_t k=0;k<jwRatio.size();k++)  {
      size_t i = nMoments()-jwRatio.size() + k;
      cout << "changing weight on joinmonth from " << vm(i,i) 
           << " to ";
      for(size_t j=0;j<nMoments();j++) {
        vm(i,j)*=sqrt(jwRatio[k]);
        vm(j,i)*=sqrt(jwRatio[k]);
      }
      cout << vm(i,i) << endl;
    }
  }
  return(vm);
}

vector<double> momentTrans(const vector<double> &m) {
  if (dataNS::cfg.useExcessJanuary) {
    vector<double> tm;
    tm = vector<double>(m.begin(),m.end()-3);
    size_t j = m.size()-4;
    tm[j] = m[j]/m[j+1] - m[j+2]/m[j+3];
    return(tm);
  } else {
    return(m); 
  }
}

array<double,2> gradTrans(const vector<double> &m) {
  if (dataNS::cfg.useExcessJanuary) {
    array<double,2> grad(m.size()-3,m.size());
    for(size_t j=0;j<m.size()-3;j++) {
      grad(j,j) = 1.0;
    }
    size_t j = m.size()-4;
    grad(j,j) = 1/m[j+1];
    grad(j,j+1) = -m[j]/(m[j+1]*m[j+1]);
    grad(j,j+2) = -1/m[j+3];
    grad(j,j+3) = m[j+2]/(m[j+3]*m[j+3]);
    return(grad);
  } else {
    array<double,2> grad(m.size(),m.size());
    for(size_t j=0;j<m.size();j++) {
      grad(j,j) = 1.0;
    }
    return(grad); 
  }
  
}

////////////////////////////////////////////////////////////////////////////
double simMMobjective(const vector<double> &x, vector<double> &grad, void *vptr)
{
  //time_t startTime = time(0); 
  auto startTime = std::chrono::high_resolution_clock::now();
  momentData *md = (momentData*) vptr;
  data *dat = md->dat;
  double obj = 0;  
  parameters pm(x);
  valueFunction *vf = NULL;  
  static time_t lastPrint = 0;
  static double smallestObj = 1e300;
  int display = 0;
  if (lastPrint==0 || difftime(time(0),lastPrint)>60) { // print every 60 seconds
    display = 1;
    lastPrint = time(0);
  }
  if (vf==NULL) { // This is always true, but for debugging purposes,
                  // I sometimes wanted to hold the value function
                  // constant, so the previous line would be static
                  // valueFunction, and then this would always be
                  // true. Note that also need to remove delete[] if
                  // want to keep vf constant.
    if (display) cout << "creating value functions " << endl << flush;
    vf = createValueFunctions(*dat,pm);
  }
  if (display) {
    cout << "Done. Elasped time this evaluation=" << 
      (double) std::chrono::duration_cast<std::chrono::milliseconds>
      (std::chrono::high_resolution_clock::now()-startTime).count()*0.001
         << " seconds." << endl;  
    cout << "simulating data" << endl << flush;
  }
  vector<simData> sdat = simulate(pm,*dat,md->rand,vf);
  destroyValueFunctions(vf);
  if (display) {
    cout << "Done. Elasped time this evaluation=" << 
      (double) std::chrono::duration_cast<std::chrono::milliseconds>
      (std::chrono::high_resolution_clock::now()-startTime).count()*0.001
         << " seconds." << endl;
    cout << "calculating moments" << endl << flush;
  }
  
  /// Compute moments  
  static bool observedSaved = false;
  static vector<double> moments, tMoments;
  static alglib::real_2d_array vm;
  static vector<double> jwRatio;
  static array<double,2> gradtm;
  static alglib::real_2d_array W;
  
  if (!observedSaved) {
    moments = calcMoments(*dat);
    vm = varMoments(*dat,moments);
    jwRatio = calcJWratio(*dat);
    tMoments = momentTrans(moments);
    gradtm = gradTrans(moments);
    W.setlength(tMoments.size(),tMoments.size());
    for (size_t i=0;i<tMoments.size();i++) {
      for(size_t j=0;j<tMoments.size();j++) {
        W(i,j) = 0;
        for(size_t k=0;k<moments.size();k++) {
          for(size_t l=0;l<moments.size();l++) {
            W(i,j) += gradtm(i,k)*vm(k,l)*gradtm(j,l);
          }
        }
      }
    }
    if (dataNS::cfg.regularize>0) 
      for(size_t i=0;i<tMoments.size();i++) W(i,i) += dataNS::cfg.regularize;
    alglib::ae_int_t info;
    alglib::matinvreport rep;
    alglib::spdmatrixinverse(W, info, rep);
    if (info!=1) cout << "WARNING alglib::spdmatrixinverse info = " 
                      << info << endl;      
    observedSaved=true;   
  }
  vector<double> sm = calcMoments(sdat);
  vector<double> tsm = momentTrans(sm);
  
  for(size_t j=0;j<tMoments.size();j++) {
    for(size_t k=0;k<tMoments.size();k++) {
      obj += (tMoments[j] - tsm[j])*W(j,k)*(tMoments[k]-tsm[k]);
    }
  }
  if (obj<=smallestObj) {
    smallestObj = obj;
    display = 1;
    cout << "Smallest objective value so far" << endl;
  }
  if (display) {
    cout << "Spending moments" << endl;
    size_t m = 0;  
    double thism, der;
    const char names[][13] ={"mean","var","p0","C(halves)","C(rs)","C(65)","C2","C3"};
    for(m=0;m<4+((size_t) dataNS::cfg.NX);m++) {
      cout << setw(12) << names[m] 
           << setw(14) << setprecision(6) << moments[m] 
           << setw(14) << setprecision(6) << sm[m];
      thism=0; 
      der=0;
      for(size_t j=0;j<tMoments.size();j++) {
        der += (moments[j]-sm[j])*(W(j,m) + W(m,j));
      }
      thism = (moments[m]-sm[m])*(moments[m]-sm[m])*W(m,m);
      cout << setw(12) << setprecision(4) << thism
           << setw(12) << setprecision(4) << der << endl;
    }
    for(size_t k=0;k<ncdf;k++) {
      char buf[50];
      sprintf(buf,"P(tc<%.0f)",cdfcut[k]);
      cout << setw(12) << buf 
           << setw(14) << setprecision(6) << moments[m] 
           << setw(14) << setprecision(6) << sm[m];
      thism=0; 
      der=0;
      for(size_t j=0;j<tMoments.size();j++) {
        der += (moments[j]-sm[j])*(W(j,m) + W(m,j));
      }
      thism = (moments[m]-sm[m])*(moments[m]-sm[m])*W(m,m);
      cout << setw(12) << setprecision(4) << thism
           << setw(12) << setprecision(4) << der << endl;      
      m++;
    }
    // excess mass moments
    {
      double total = 0;
      for(int j=0;j<dataNS::cfg.nbinEM;j++) {
        total += (moments[m+j]-sm[m+j])*(moments[m+j]-sm[m+j])*W(m+j,m+j);
      }
      cout << "Contribution of excess mass moments = "
           << total << endl;
    }
    m+=dataNS::cfg.nbinEM;
    // autocovariance
    if (dataNS::cfg.useSpendAutoCov) {
      cout << "Autocovariance of weekly spending" << endl;
      for (int lag=0;lag<=dataNS::cfg.nlags;lag++) {
        cout << setw(6) << " lag " << setw(6) << lag
             << setw(14) << setprecision(6) << moments[m] 
             << setw(14) << setprecision(6) << sm[m];
        thism=0; 
        der=0;
        for(size_t j=0;j<tMoments.size();j++) {
          der += (moments[j]-sm[j])*(W(j,m) + W(m,j));
        }
        thism = (moments[m]-sm[m])*(moments[m]-sm[m])*W(m,m);
        cout << setw(12) << setprecision(4) << thism
             << setw(12) << setprecision(4) << der << endl;
        m++;
      } 
    }
    // P(claim t & claim t-j)
    cout << " P(claim t & claim t-lag)" << endl;
    for(int lag=0;lag<=dataNS::cfg.nlags;lag++) {
      cout << setw(6) << " lag " << setw(6) << lag
           << setw(14) << setprecision(6) << moments[m] 
           << setw(14) << setprecision(6) << sm[m];
      thism=0; 
      der=0;
      for(size_t j=0;j<tMoments.size();j++) {
        der += (moments[j]-sm[j])*(W(j,m) + W(m,j));
      }
      thism = (moments[m]-sm[m])*(moments[m]-sm[m])*W(m,m);
      cout << setw(12) << setprecision(4) << thism
           << setw(12) << setprecision(4) << der << endl;
      m++;
    }
    // "timing moments"
    {
      int nbin = dataNS::cfg.nbinTiming*(dataNS::cfg.diffTiming==0? 1.0:0.5);
      for (int month=0;month<(dataNS::cfg.nMonthlyProb -
                              1*(dataNS::cfg.diffTiming==2));month++) {
        double total = 0.0;
        for(int k=0;k<nbin;k++) { 
          int j = k+month*nbin;
          cout << setw(3) << k
               << setw(12) << setprecision(4) << moments[m+j]
               << setw(12) << setprecision(4) << sm[m+j] 
               << setw(12) << setprecision(4) << W(m+j,m+j)
               << endl;
          total += (moments[m+j]-sm[m+j])*(moments[m+j]-sm[m+j])*W(m+j,m+j);
        }
        cout << "Contribution of timing moments in month " 
             << 12-month << " = " << total << endl;
      }
    }
    m += dataNS::cfg.nbinTiming*(dataNS::cfg.nMonthlyProb-
                                 1*(dataNS::cfg.diffTiming==2))*(dataNS::cfg.diffTiming==0? 1.0:0.5);
    
    if (dataNS::cfg.useWeeks) {
      cout << "Average weeks to first claim" << endl;
      for(int d=0;d<2;d++) {
        for(int q=0;q<3;q++) {
          char buf[13];
          int l = m + q + 3*d;        
          sprintf(buf,"q=%d ded=%d",q,d);
          cout << setw(12) << buf
               << setw(14) << setprecision(6) << moments[l]*jwRatio[q+3*d] 
               << setw(14) << setprecision(6) << sm[l]*jwRatio[q+3*d];
          thism=0;
          der = 0;
          for(size_t j=0;j<tMoments.size();j++) {
            der += (moments[j] - sm[j])*(W(j,l)+W(l,j));
            //thism += (moments[j] - sm[j])*W(j,l)*(moments[l]-sm[l]);
            //if (j!=m) thism += (moments[l] - sm[l])*W(l,j)*(moments[j]-sm[j]);
          }
          thism = (moments[l]-sm[l])*(moments[l]-sm[l])*W(l,l);
          cout << setw(12) << setprecision(4) << thism 
               << setw(12) << setprecision(4) << der << endl;
        }
      }
      m += 6;
    }

    if (dataNS::cfg.useExcessJanuary) {
      for (int k=0;k<4;k++) {
        cout << "Excess January " 
             << setw(14) << setprecision(6) << moments[m+k] 
             << setw(14) << setprecision(6) << sm[m+k] << endl;        
      }
      cout << "Excess January "             
           << setw(14) << setprecision(6) << tMoments[m] 
           << setw(14) << setprecision(6) << tsm[m];
      thism=0; 
      der=0;
      for(size_t j=0;j<tMoments.size();j++) {
        der += (tMoments[j]-tsm[j])*(W(j,m) + W(m,j));
      }
      thism = (tMoments[m]-tsm[m])*(tMoments[m]-tsm[m])*W(m,m);
      cout << setw(12) << setprecision(4) << thism
           << setw(12) << setprecision(4) << der << endl;
      m += 4;
    }
    
    if (m!=nMoments()) {
      cout << "ERROR: wrong number of moments " << nMoments()
           << " != " << m << endl;
      exit(1);
    }
   
    cout << " PARAMETERS " << endl;
    cout << setw(14) << " ";
    for(int m = 0; m<dataNS::cfg.NMIX; m++) cout << setw(14) << m;
    cout << endl << setw(14) << "mu";
    for(int m = 0; m<dataNS::cfg.NMIX; m++) cout << setw(14) << setprecision(6) << pm.thetaD[m].mu;
    cout << endl << setw(14) << "sig";
    for(int m = 0; m<dataNS::cfg.NMIX; m++) cout << setw(14) << setprecision(6) << pm.thetaD[m].sig;
    cout << endl << setw(14) << "p";
    for(int m = 0; m<dataNS::cfg.NMIX; m++) cout << setw(14) << setprecision(6) << pm.omega[m].p;
    for (size_t j=0;j<pm.betaMixP.dimSize(1);j++) {
      cout << endl << setw(14) << "beta";
      for(int m = 0; m<dataNS::cfg.NMIX; m++) cout << setw(14) << setprecision(6) << pm.betaMixP(m,j);
    }
    for (size_t j=0;j<pm.lambda.dimSize(1);j++) {
      cout << endl << setw(14) << "lambda";
      for(int m = 0; m<dataNS::cfg.NMIX; m++) cout << setw(14) << setprecision(6) << pm.lambda(m,j);
    }
    cout << endl << " delta " <<  setw(14) << setprecision(6) << pm.delta << endl;
    cout <<  " deltaTheta " <<  setw(14) << setprecision(6) << pm.deltaTheta << endl;
    cout <<  " deltaOmega " <<  setw(14) << setprecision(6) << pm.deltaOmega << endl;
    cout << " riskAversion " <<  setw(14) << setprecision(6) << pm.riskAversion << endl;
    for(size_t c=0;c<pm.transitionProb.dimSize(0);c++) {
      cout << "transitionProb | claim=" << c << endl;
      for(size_t j=0;j<pm.transitionProb.dimSize(1);j++) {
        for(size_t k=0;k<pm.transitionProb.dimSize(2);k++) {
          cout << setw(14) << setprecision(6) << pm.transitionProb(c,j,k);
        }
        cout << endl;
      }
    }
    cout << " pOT " <<  setw(14) << setprecision(6) << pm.pOT << endl;
    cout << "simMMobj = " << setprecision(12) << obj << endl;    
    cout << "Evaluation time=" << 
      (double) std::chrono::duration_cast<std::chrono::milliseconds>
      (std::chrono::high_resolution_clock::now()-startTime).count()*0.001
         <<  " seconds." << endl
         << 
      "----------------------------------------------------------------------"
         << endl << flush;
  } // endif(display)
  return(obj);
}

