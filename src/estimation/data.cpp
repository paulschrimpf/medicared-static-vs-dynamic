#include "data.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <string>
#include <sqlite3.h>
#include <sstream>
#include <set>
#include <cmath>

#include <fstream>
#include <iostream> 
#include <iomanip>
#include <ctime>

#include "alglib/integration.h"
#include "alglib/solvers.h"
#include "alglib/statistics.h"
#include "gaussQuad.hpp"

//using namespace std;
using std::cout;
using std::endl;
using std::flush;
using std::vector;
using std::set;
using std::string;
using std::stringstream;
using std::ifstream;
using std::setw;
using std::setprecision;


using namespace dataNS;
namespace dataNS {
  config cfg = config();
}

/**********************************************************************/
// array class
template<typename T, size_t dim> array<T,dim>::array() { 
  dims = vector<size_t>(dim,0); 
}

template<typename T, size_t dim>
array<T,dim>::array(size_t N1,size_t N2, size_t N3, size_t N4, size_t N5) {
  switch(dim) {
  case 1: 
    elements = vector<T>(N1,0.0);
    dims = vector<size_t>(dim);    
    dims[0] = N1;
    return;
  case 2: 
    elements = vector<T>(N1*N2,0.0);
    dims = vector<size_t>(dim);    
    dims[0] = N1; dims[1] = N2;
    return;
  case 3: 
    elements = vector<T>(N1*N2*N3,0.0);
    dims = vector<size_t>(dim);    
    dims[0] = N1; dims[1] = N2; dims[2] = N3;
    return;
  case 4: 
    elements = vector<T>(N1*N2*N3*N4,0.0);
    dims = vector<size_t>(dim);    
    dims[0] = N1; dims[1] = N2; dims[2] = N3; dims[3] = N4;
    return;
  case 5: 
    elements = vector<T>(N1*N2*N3*N4*N5,0.0);
    dims = vector<size_t>(dim);    
    dims[0] = N1; dims[1] = N2; dims[2] = N3; dims[3] = N4; dims[4] = N5;
    return;
  default:
    cout << "ERROR: array() only implements for dim<=5" << endl;
    exit(1);    
  }
}

template<typename T, size_t dim>
array<T,dim>::array(const vector<size_t> &d) {
  if (d.size() != dim) {
    cout << "ERROR: wrong number of dimensions passed to array" << endl;
    exit(1);
  }
  dims = d;  
  size_t N = dims[0]; 
  for(size_t i=1;i<dim;i++) N *= dims[i];
  elements = vector<T>(N);
}

template<typename T, size_t dim>
T& array<T,dim>::operator[](size_t i) {
  return(elements[i]);  
}

template<typename T, size_t dim>
const T& array<T,dim>::operator[](size_t i) const {
  return(elements[i]);  
}

template<typename T, size_t dim>
const T& array<T,dim>::operator()(size_t s1, size_t s2, size_t s3, size_t s4, size_t s5) const {
  switch(dim) {
  case 1: return(elements[s1]);
  case 2: return(elements[s1 + s2*dims[0]]);
  case 3: return(elements[s1 + s2*dims[0] + s3*(dims[0]*dims[1])]);
  case 4: return(elements[s1 + s2*dims[0] + s3*(dims[0]*dims[1]) + 
                      s4*(dims[0]*dims[1]*dims[2])]);
  case 5: return(elements[s1 + s2*dims[0] + s3*(dims[0]*dims[1]) + 
                          s4*(dims[0]*dims[1]*dims[2]) +
                          s5*(dims[0]*dims[1]*dims[2]*dims[3])]);
  default: 
    cout << "ERROR: array only support indexing for dim<=5" << endl;
    exit(1);
  };
}

template<typename T, size_t dim>
T& array<T,dim>::operator()(size_t s1, size_t s2, size_t s3, size_t s4, size_t s5) {
  switch(dim) {
  case 1: return(elements[s1]);
  case 2: return(elements[s1 + s2*dims[0]]);
  case 3: return(elements[s1 + s2*dims[0] + s3*(dims[0]*dims[1])]);
  case 4: return(elements[s1 + s2*dims[0] + s3*(dims[0]*dims[1]) + 
                      s4*(dims[0]*dims[1]*dims[2])]);
  case 5: return(elements[s1 + s2*dims[0] + s3*(dims[0]*dims[1]) + 
                          s4*(dims[0]*dims[1]*dims[2]) +
                          s5*(dims[0]*dims[1]*dims[2]*dims[3])]);
  default: 
    cout << "ERROR: array only support indexing for dim<=5" << endl;
    exit(1);
  };
}

template<typename T, size_t dim> 
typename vector<T>::iterator array<T,dim>::begin() { return(elements.begin()); }
template<typename T, size_t dim> 
typename vector<T>::const_iterator array<T,dim>::begin() const { return(elements.begin()); }

template<typename T, size_t dim> 
typename vector<T>::iterator array<T,dim>::end() { return(elements.end()); }
template<typename T, size_t dim> 
typename vector<T>::const_iterator array<T,dim>::end() const { return(elements.end()); }

template<typename T, size_t dim> 
void array<T,dim>::copyVec(vector<T> in) {
  if (in.size() != elements.size()) {
    dims = vector<size_t>(1, in.size());
  }
  elements = in;
}

template<typename T, size_t dim> size_t array<T,dim>::size() const 
{ return(elements.size()); }

template<typename T, size_t dim> size_t array<T,dim>::dimSize(size_t i) const 
{ return(dims[i]); }

// instantiate
template class array<double, 1>;
template class array<double, 2>;
template class array<double, 3>;
template class array<double, 4>;
template class array<double, 5>;
template class array<int, 1>;
template class array<int, 2>;
template class array<int, 3>;
template class array<int, 4>;
/**********************************************************************/

data::data() {
  N = T = 0;
  Years = 1;
}

void data::allocate() {
  if (N==0 || T==0) {
    cout << "WARNING: data::allocate() sizes not set, doing nothing" << endl;
    return;
  }
  
  joinweek = array<int,2>(N,Years);
  plan = array<int,2>(N,Years);
  in = array<int,2>(N,Years);
  cost = array<double,3>(N,T,Years);
  cumCost = array<double,3>(N,T,Years);
  weight = array<double,2>(N,T,Years);
  for(size_t i=0;i<weight.size();i++) weight[i] = 1.0;
  ded = array<double,2>(N,Years);
  icl = array<double,2>(N,Years);
  cat = array<double,2>(N,Years);  
  eoyPrice = array<double,2>(N,Years);
  riskScore = array<double,2>(N,Years);
  if (cfg.NX>0) {
    X = array<double,3>(N,cfg.NX,Years); // X(i,0,y) = riskscore*age>65
    // X(i,1,y) = age==65
  }
  age = array<int,2>(N,Years);

  oop = vector<oopFunc>(nplan);
  // xsolve[i] cannot be allocated until nx read
}
/***********************************************************************/
/*** Find "search" in "subject" and replace with "replace" 
     source: http://stackoverflow.com/a/14679003
 */
std::string ReplaceString(std::string subject, const std::string& search,
                          const std::string& replace) {
  size_t pos = 0;
  while((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
  return subject;
}
/***********************************************************************/
set<string> sqlColumnSet(sqlite3 *db, const string &col, 
                         const string &tab)
{
  stringstream query;
  sqlite3_stmt *stmt;
  set<string> resultSet;
  query << "SELECT DISTINCT " << col << " FROM " << tab;
  sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  while(SQLITE_ROW == sqlite3_step(stmt)) {
    if (sqlite3_column_count(stmt)!=1) {
      cout << "SQL error while getting " << col << " from " 
           << tab << cout;
      exit(1);
    }
    string result = string(reinterpret_cast<const char*>
                           (sqlite3_column_text(stmt, 0)));
    resultSet.insert(result);
  }
  sqlite3_finalize(stmt); 
  return(resultSet);
}

set<string> sqlColumnSet(sqlite3 *db, const string &col, 
                         const string &tab, const string &where)
{
  stringstream query;
  sqlite3_stmt *stmt;
  set<string> resultSet;
  query << "SELECT DISTINCT " << col << " FROM " << tab << " WHERE " << where;
  sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  while(SQLITE_ROW == sqlite3_step(stmt)) {
    if (sqlite3_column_count(stmt)!=1) {
      cout << "SQL error while getting " << col << " from " 
           << tab << cout;
      exit(1);
    }
    string result = string(reinterpret_cast<const char*>
                           (sqlite3_column_text(stmt, 0)));
    resultSet.insert(result);
  }
  sqlite3_finalize(stmt); 
  return(resultSet);
}

set<int> sqlColumnIntSet(sqlite3 *db, const string &col, 
                         const string &tab)
{
  stringstream query;
  sqlite3_stmt *stmt;
  set<int> resultSet;
  query << "SELECT DISTINCT " << col << " FROM " << tab;
  sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  while(SQLITE_ROW == sqlite3_step(stmt)) {
    if (sqlite3_column_count(stmt)!=1) {
      cout << "SQL error while getting " << col << " from " 
           << tab << cout;
      exit(1);
    }
    int result = sqlite3_column_int(stmt, 0);
    resultSet.insert(result);
  }
  sqlite3_finalize(stmt); 
  return(resultSet);
}

size_t sqlColumnCount(sqlite3 *db, const string &col, 
                      const string &tab)
{
  stringstream query;
  sqlite3_stmt *stmt;
  set<string> resultSet;
  query << "SELECT Count(DISTINCT " << col << ") AS colCount FROM " << tab;
  sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  if (SQLITE_DONE != sqlite3_step(stmt)) {
    cout << "SQL error while getting Count(" << col << ") from " 
         << tab << endl;
  }
  size_t count=(size_t) sqlite3_column_int(stmt, 0);
  sqlite3_finalize(stmt); 
  return(count);
}
/**********************************************************************/
class identifier {
public:
  string id;
  mutable size_t index;
};
bool operator<(const identifier & i1, const identifier &i2) {
  return(i1.id<i2.id);
}
bool operator<(const identifier & i1,const string &s2) {
  return(i1.id<s2);
}
bool operator<(const string &s1,const identifier &i2) {
  return(s1<i2.id);
}

class planId : public identifier {
public:
  int year;
  int count;
};
bool operator<(const planId & i1, const planId &i2) {
  return(i1.count>i2.count || 
         (i1.count==i2.count && 
          (i1.id<i2.id || 
           (i1.id==i2.id && i1.year < i2.year)
           )));
}

int setIndex(const set<identifier> &haystack, const string &needle) {
  identifier nid;
  nid.id=needle;
  set<identifier>::iterator it = haystack.find(nid);
  if (it==haystack.end()) return(-1);
  else return((int) (*it).index);
}

int setIndex(const set<identifier> &haystack, const identifier &needle) {
  set<identifier>::iterator it = haystack.find(needle);
  if (it==haystack.end()) return(-1);
  else return((int) (*it).index);
}
int setIndex(const set<planId> &haystack, const planId &needle) {
  set<planId>::iterator it = haystack.begin();
  while(it!=haystack.end() && (it->year!=needle.year || it->id!=needle.id)) it++;
  if (it==haystack.end()) return(-1);
  else return((int) (*it).index);
}

template<class id>
void setIndices(set<id> &s) {
  size_t i =0;
  for(typename set<id>::iterator it = s.begin();it!=s.end();it++) {    
    (*it).index=i;
    i++;
  }
}

int monthToWeek(int month, // month starting from 0
                int year) // year
{
  struct tm date; // noon on 1st day of month
  date.tm_sec = 0;
  date.tm_min = 0;
  date.tm_hour = 12; 
  date.tm_mday = 1;
  date.tm_mon = month;
  date.tm_year = year-1900;
  if (mktime(&date)==-1) { // sets remaining fields of structure
    cout << "Error converting dates" << endl;
    exit(1);
  }
  int week = date.tm_yday/dataNS::cfg.weekLength;
  int T = 365/dataNS::cfg.weekLength;
  if (week>(T-1)) week=(T-1); // add extra days to last week of year
  return(week);  
}

/***********************************************************************/
/*** Load data from SQLite database. 
     The data should be in two files. One should be named
     basename-payHist.sl3 and the other should be named
     basename-const.sl3. "fname" can be either one of these or
     basename.sl3. 

     basename-payHist.sl3 contains a single table named "data" with 
     fields "bene_id", "srvc_dt", "totalcst" (additional fields may be
     included, but will not be used). 
     
     basename-const.sl3 contains a single table named "data" with
     fields "bene_id", "yr", "plan", "ded_amt","icl_amt", "oopt_amt",
     "coin_ded", "coin_pre", "coin_gap", "coin_cat", "riskscore_CE","nogapcov",
     "partd_joinmont" (additional fields may be included, but will not
     be used).

     The Stata program "createSQLite.do" creates databases with the
     required fields.     
 */
void data::loadSQLite(const string &fname, const int nplanUse, const int nplanSkip,
                      const bool differentObs, 
                      const int minYear, const int maxYear)
{
  string baseName = ReplaceString(fname, string(".sl3"), string(""));
  baseName = ReplaceString(baseName, string("-const"), string(""));
  baseName = ReplaceString(baseName, string("-payHist"), string(""));
  string fileName = baseName;
  fileName.append("-const.sl3");
  cout << "Reading constant data from " << fileName << endl;
  // Read constant data
  sqlite3 *db;
  int error = 0;
  error = sqlite3_open(fileName.c_str(),&db);
  if (error) {
    cout << "Failed to open SQLite database " 
         << fileName << endl;
    exit(1);
  }
  // Get set of plans 
  stringstream query;
  sqlite3_stmt *stmt;
  set<planId> plans;
  query << "SELECT plan,yr,count(*) FROM data WHERE yr >=" 
        << minYear << " AND yr <= " << maxYear 
        << " GROUP BY plan,yr";
  cout << query.str().c_str() << endl;
  int resultCode = sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  cout << resultCode << endl;
  while(SQLITE_ROW == sqlite3_step(stmt)) {
    if (sqlite3_column_count(stmt)!=3) {
      cout << "SQL error while reading plans from data" << endl;
      exit(1);
    }
    planId pid;
    pid.id = string(reinterpret_cast<const char*> (sqlite3_column_text(stmt, 0))) ;
    pid.year = sqlite3_column_int(stmt,1);
    pid.count = sqlite3_column_int(stmt,2);
    plans.insert(pid);
  }
  sqlite3_finalize(stmt);
  setIndices(plans);
  cout << "Database has observations from " << plans.size() << " plans" << endl;
  // Get set of ID's
  size_t idCount  = sqlColumnCount(db,"bene_id","data");
  cout << "Found " << idCount << " unique IDs" << endl;
  // Sample ID's 
  set<identifier> idSet;
  alglib::hqrndstate state;
  alglib::hqrndseed(4151,43156,state);
  set<planId>::iterator pit = plans.begin();
  if (nplanUse>0 && (nplanUse + nplanSkip) < (int) plans.size()) {
    if (nplanSkip>0) {
      pit = plans.begin();
      for(int i=0;i<nplanSkip;i++) pit++;
      plans.erase(plans.begin(),pit);
    }
    pit = plans.begin();
    for(int i=0;i<nplanUse;i++) pit++;
    plans.erase(pit,plans.end());  
    setIndices(plans);
  }
  this->nplan = plans.size();
  cout << "Using observations from " << this->nplan << " plans." << endl;
  query.str(string()); // clear query
  query << "SELECT bene_id,age,plan,yr FROM data WHERE yr >=" 
        << minYear << " AND yr <= " << maxYear;
  resultCode = sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  int nrow=0;
  while(SQLITE_ROW == sqlite3_step(stmt)) {
    if (sqlite3_column_count(stmt)!=4) {
      cout << "SQL error while sampling ids from data" << endl;
      exit(1);
      }
    int age = sqlite3_column_int(stmt,1);
    double u = alglib::hqrnduniformr(state);      
    if ( differentObs? 
         ((age==65 && u>1-probKeep65) || (u>1-probKeep)) :
         ((age==65 && u<probKeep65) || (u<probKeep))
         ) {
      planId pid;
      pid.id = string(reinterpret_cast<const char*> (sqlite3_column_text(stmt, 2))) ;
      pid.year = sqlite3_column_int(stmt,3);
      pid.count = -1;
      int p = setIndex(plans, pid);
      if (p>=0) {
        identifier id;
        id.id = string(reinterpret_cast<const char*> (sqlite3_column_text(stmt, 0))) ;
        idSet.insert(id);
      }
    }
    nrow++;
    if (nrow%1000000==0) cout << endl << " Sampled ids from " << nrow << " of about " 
                              << idCount << " rows." << endl;
    else if (nrow%50000==0) cout << "." << flush;
  }
  sqlite3_finalize(stmt);  
  
  idCount = idSet.size();
  setIndices(idSet);
  if (idCount<=0) {
    cout << "Error: no beneficiaries id's meet inclusion criteria" << endl;
  }
  cout << "Using " << idCount << " beneficiaries" << endl;
  this->N = (int) idCount;
  this->valueT = this->T = 365/dataNS::cfg.weekLength;
  // Find out number of years
  set<int> yrSet = sqlColumnIntSet(db,"yr","data");
  this->Years = yrSet.size();
  int firstYear = *(min_element(yrSet.begin(),yrSet.end()));
  if (this->Years > (maxYear - minYear + 1)) {
    cout << "Database contains years outside of [minYear, maxYear]" << endl;
    firstYear = minYear;
    this->Years = maxYear - minYear + 1;
  }
  cout << "Database has observations from " << this->Years << " years" << endl;
  // allocate arrays to hold data
  this->allocate();

  // read data
  query.str(string()); // clear query
  //                  0     1   2   3    4                   5      
  query << "SELECT bene_id,yr,plan,age,partd_joinmont,riskscore_CE,"
    //        6         7     8         9        10      11        12      13        14
        << "ded_amt,icl_amt,oopt_amt,coin_ded,coin_pre,coin_gap,coin_cat,nogapcov,eoyPrice FROM data"
        << " WHERE yr >=" << minYear << " AND yr <= " << maxYear;
  resultCode = sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  if (SQLITE_OK != resultCode) {
    cout << "Error when reading const data" << endl;
  }
  int count = 0;
  int nout = 0;
  for(int i=0;i<N;i++) for(int y=0;y<Years;y++) this->in(i,y) = 0;
  vector<bool> needToSetup(this->nplan,true);
  vector<bool> dropPlan(this->nplan,false);
  int nors = 0;
  int nin = 0;
  while(SQLITE_ROW == sqlite3_step(stmt)) {
    if (sqlite3_column_count(stmt)!=15) {
      cout << "SQL error while reading const data" << endl;
      exit(1);
    }
    int i = setIndex(idSet, string(reinterpret_cast<const char*>
                                   (sqlite3_column_text(stmt, 0))) ) ;
    if (i>=0) { // else id not in idSet, so skip it
      count++;
      if (i > this->N || i < 0) {
        cout << "Error: id index out of range" << endl;
        exit(1);
      }
      int year = sqlite3_column_int(stmt,1);
      int y = year - firstYear;
      if (y<0 || y>=this->Years) {
        cout << " year=" << year << " firstYear=" << firstYear 
             << " this->Years=" << this->Years << endl;
        cout << "Error: year out of range, this should be impossible" << endl;
        exit(1);
      }
      planId pid;
      pid.id = string(reinterpret_cast<const char*> (sqlite3_column_text(stmt, 2))) ;
      pid.year = year;
      pid.count = -1;
      int p = setIndex(plans, pid);
      int age = sqlite3_column_int(stmt,3);
      if (p > this->nplan || p < 0) { 
        // if we're not using all the plans, some ids might be
        // included because they chose included plans in some years
        // but not all. In those cases, this warning will display.
        nout++;
        continue;
      } else { //if (age==65 || year !=2009) { 
        this->plan(i,y) = p;
        this->age(i,y) = age;
        int jm = sqlite3_column_int(stmt,4);
        this->joinweek(i,y) = monthToWeek(jm-1, year);
        double rs = sqlite3_column_double(stmt,5);
        int nogapcov = sqlite3_column_int(stmt,13);
        if (rs<=-999 && this->age(i,y)!=65) {
          nors++;
          continue; // skip id-year combinations with missing risk scores        
        }
        this->riskScore(i,y) = rs;
        if (dataNS::cfg.NX>=2) {
          this->X(i,0,y) = this->age(i,y)!=65? rs:0;
          this->X(i,1,y) = (this->age(i,y)==65);
          if (dataNS::cfg.NX>=3) this->X(i,2,y)= (int) nogapcov;
        }
        this->ded(i,y) = sqlite3_column_double(stmt,6);
        this->icl(i,y) = sqlite3_column_double(stmt,7);
        this->cat(i,y) = sqlite3_column_double(stmt,8);
        this->eoyPrice(i,y) = sqlite3_column_double(stmt,14);
        if ((count % 100000)==0) cout << endl << " processed " << count << " of roughly " 
                                      << idSet.size()*yrSet.size() << " records" 
                                      << endl << flush;
        else if ((count % 5000)==0) cout << "." << flush;
        this->in(i,y) = 1;
        nin++;
        if (needToSetup[p]) { // setup plan
          double rate[4];
          cout << "ded icl cat = " 
               << setw(8) << setprecision(4) << this->ded(i,y) 
               << setw(8) << setprecision(4) << this->icl(i,y) 
               << setw(8) << setprecision(4)<< this->cat(i,y) << endl; 
          cout << "rate = ";
          for(int k=0;k<4;k++) {
            rate[k] = sqlite3_column_double(stmt,9+k);
            cout << setw(10) << setprecision(4) << rate[k];
          }
          cout << endl;
          const int kink = 1;
          for(int k=0;k<3;k++) {
            if (rate[k]>0 && k!=kink && rate[k]<rate[k+1]) {
              cout << "WARNING : found a plan with convex kink not at the hole. Dropping it from the data. " 
                   << k << " " << kink
                   << endl; 
              dropPlan[p] = true;
            }
          }
          this->oop[p].setup(this->ded(i,y),this->icl(i,y),this->cat(i,y),rate,this->eoyPrice(i,y));
          needToSetup[p] = false;
        }
      }
    }
  }
  sqlite3_finalize(stmt); 
  sqlite3_close(db);  
  cout << endl << " Read " << count << " id*year observations" << endl;
  cout << " Number in = " << nin << endl;
  if (nout>0) {
    cout << "Warning: " << nout 
         << " ids chose included plans in some years, but not others " 
         << endl;
  }
  if (nors>0) {
    cout << "Warning: " << nors
         << " ids are missing risk scores " << endl;
  }
  for(int p=0;p<this->nplan;p++) {
    if (needToSetup[p]) {
      cout << "Warning: failed to setup plan " << p;
      for (set<planId>::iterator pit=plans.begin();pit!=plans.end();pit++) {
        if (pit->index == (size_t) p) {
          cout << " pid="<<pit->id << " year=" << pit->year << endl;
          cout << " This plan was not chosen by anyone included in this estimation sample, so it will be omitted." 
               << endl;
          break;
        }
      }
    }
    else if (dropPlan[p]) {
      for (set<planId>::iterator pit=plans.begin();pit!=plans.end();pit++) {
        if (pit->index == (size_t) p) {
          cout << " pid="<<pit->id << " year=" << pit->year << endl;
          cout << " This plan has convex kink not at the hole" << endl;
          break;
        }
      }
      for(int i=0;i<N;i++) for(int y=0;y<Years;y++) { 
          if(this->plan(i,y) == p) this->in(i,y) = 0;
        }
    }
  }
  // divide plans by risk score
  if (dataNS::cfg.terminalV) {    
    // compute quantiles as cutoffs for bins
    cout << "Dividing plans into " << cfg.nRiskBin 
         << " bins based on risk score to calculate E[eoy price]" 
         << endl;
    vector<double> riskCutOff(dataNS::cfg.nRiskBin);

    if(dataNS::cfg.nRiskBin>1) {
      int count = 0;
      for (int i=0;i<N;i++)  for (int y=0;y<Years;y++) count += (riskScore(i,y)>=0 && in(i,y));
      alglib::real_1d_array rs;
      rs.setlength(count);
      count = 0;
      for (int i=0;i<N;i++)  for (int y=0;y<Years;y++) {
          if (riskScore(i,y) >= 0 && in(i,y)) rs[count++] = riskScore(i,y);
        }
      for(int c=0;c<(dataNS::cfg.nRiskBin-1);c++) {
        alglib::samplepercentile(rs,((double) (c+1))/((double) cfg.nRiskBin), riskCutOff[c]);
      }
    }
    riskCutOff[(dataNS::cfg.nRiskBin-1)] = 1e300;
    cout << " bin cutoffs = ";
    for(int c=0;c<(dataNS::cfg.nRiskBin);c++) {
      cout << riskCutOff[c] << " ";
    }
    cout << endl;
    
    // create new plans and compute average eoy price for each plan
    int nplanOld = nplan;
    nplan = nplanOld*cfg.nRiskBin;
    array<double,2> exEOYprice(nplanOld,cfg.nRiskBin);
    array<int,2> nobs(nplanOld,cfg.nRiskBin);
    for(int rb=0;rb<cfg.nRiskBin;rb++) {
      for (int p=0;p<nplanOld;p++) {
        exEOYprice(p,rb) = 0.0;
        nobs(p,rb) = 0.0;
      }
    }
    for(int i=0;i<N;i++) {
      for(int y=0;y<Years;y++) {
        if (in(i,y)) {
          int rb=0;
          while(riskScore(i,y) >= riskCutOff[rb] && rb<(cfg.nRiskBin-1)) { rb++; }
          exEOYprice(plan(i,y), rb) += eoyPrice(i,y);
          nobs(plan(i,y),rb)++;
          plan(i,y) += rb*nplanOld;
        }
      }
    }
    // insert new plans
    for(int rb=0;rb<cfg.nRiskBin;rb++) {
      for (int p=0;p<nplanOld;p++) {
        if (nobs(p,rb) == 0) {
          cout << "WARNING: no observations of plan " << p << " bin " << rb 
               << endl;
          exEOYprice(p,rb) = -1.0;
        } else {
          exEOYprice(p,rb) /= nobs(p,rb);
        }
        if (rb==0) { 
          oop[p].resetEeoyPrice(exEOYprice(p,rb));
        }
        else { 
          oopFunc newplan=oop[p]; 
          newplan.resetEeoyPrice(exEOYprice(p,rb));
          oop.push_back(newplan);          
        }
      }
    }
  }
  
  // sampling weights
  double sumw = 0;
  count = 0;
  for(int i=0;i<this->N;i++) {
    for(int y=0;y<this->Years;y++) {
      if (this->in(i,y)) {
        sumw += (this->weight(i,y) = this->age(i,y)==65? 1.0/probKeep65:1.0/probKeep);
        count++;
      }
    }
  }  
  // make weights have mean 1
  sumw /= count;
  for(int i=0;i<this->N;i++) {
    for(int y=0;y<this->Years;y++) {
      if (this->in(i,y)) {
        this->weight(i,y) /= sumw;
      }
    }
  }
  
  // Read claim history
  fileName = baseName;
  fileName.append("-payHist.sl3");
  cout << "Reading claim history from " << fileName << endl;
  error = sqlite3_open(fileName.c_str(),&db);
  if (error) {
    cout << "Failed to open SQLite database " 
         << fileName << endl;
    exit(1);
  }
  query.str(string()); // clear query
  //                 0        1       2
  query << "SELECT bene_id,srvc_dt,totalcst FROM data";
  sqlite3_prepare_v2(db, query.str().c_str(), query.str().length(), &stmt, NULL);
  count = 0;
  for(size_t i=0;i<cost.size();i++) { cumCost[i] = cost[i] = 0; }
  int warnings = 0;
  while(SQLITE_ROW == sqlite3_step(stmt)) {
    if (sqlite3_column_count(stmt)!=3) {
      cout << "SQL error while reading claim history" << endl;
      exit(1);
    }
    int i = setIndex(idSet, string(reinterpret_cast<const char*>
                                   (sqlite3_column_text(stmt, 0))) ) ;
    if (i>=0) { 
      // else id not in idSet, so skip it
      count++;
      if (i > this->N || i < 0) {
        cout << "Error: id index out of range" << endl;
        exit(1);
      }
      int stataDateInt = sqlite3_column_int(stmt, 1); 
      double c = sqlite3_column_double(stmt,2);
      if (stataDateInt!=-9999 && c>0) {
        // date in days since Jan 1, 1960
        struct tm date; // noon on given date
        date.tm_sec = 0;
        date.tm_min = 0;
        date.tm_hour = 12; 
        date.tm_mday = 1+stataDateInt;
        date.tm_mon = 0;
        date.tm_year = 60;
        if (mktime(&date)==-1) { // sets remaining fields of structure
          cout << "Error converting dates" << endl;
          exit(1);
        }
        int year = date.tm_year+1900;
        int y = year-firstYear;
        int week = date.tm_yday/dataNS::cfg.weekLength;
        if (week>(T-1)) week=T-1; // add extra days to last week of year
        if (week>T || y>=Years || week<0 || y<0) {
          if (warnings++<10) cout << "Warning: year out of range" << endl;
          continue;
        }
        this->cost(i,week,y) += c;
      }              
      if ((count % 1000000)==0) cout << endl << " processed " << count << " of roughly " 
                                     << N*T*Years/10 << " records" << endl << flush;
      else if ((count % 50000)==0) cout << "." << flush;
    }
  }
  sqlite3_finalize(stmt); 
  cout << " Read " << count << " payment records" << endl;
  sqlite3_close(db);
  // set cumCost
  for(int i=0;i<N;i++) {
    for(int y=0;y<Years;y++) {
      for(int t=0;t<this->joinweek(i,y);t++) this->cost(i,t,y) = 0.0;
      this->cumCost(i,0,y) = this->cost(i,0,y);
      for(int t=1;t<T;t++) {
        this->cumCost(i,t,y) = this->cumCost(i,t-1,y) + this->cost(i,t,y);
      }
    }
  }
  
  // set xsolve
  up = array<int,1>(nplan);
  nx = vector<int>(nplan);
  xsolve = vector< vector<double> >(nplan);
  this->resetXsolve();
  for(int i=0;i<nplan;i++) this->up[i] = i;  
}
/***********************************************************************/

/***********************************************************************/
data::data(const char *filename, 
           const double pk, const double pk65, 
           const int nplanUse, const int nplanSkip, 
           const bool differentObs,
           const int minYear, const int maxYear)
{
  this->probKeep = pk;
  this->probKeep65 = pk65;
  string fname(filename);
  if (fname.find(".sl3") != string::npos) {
    cout << "Reading data from the SQLite database " << fname << endl;
    this->loadSQLite(fname, nplanUse, nplanSkip, differentObs,
                     minYear, maxYear);
    return;
  } else {
    cout << "Reading data from text file no longer supported" << endl;
    exit(1);
  }
}

#define NX_CAT 4

vector<double> dataNS::setXsolve(const oopFunc &oop, const double maxx) {
  vector<double> xsolve;
  if (oop.ready()) {
    int nseg = oop.x().size();
    int i = 0;
    int nx = cfg.nxSegment*(nseg-1) + NX_CAT;
    xsolve = vector<double>(nx);
    for (int j=0;j<nseg-1;j++) {
      double dx = (oop.x()[j+1] - oop.x()[j])/cfg.nxSegment;
      if (isinf(dx) || isnan(dx)) {
        printf("j=%d, dx=%f, nxSegment=%d\n", j, dx,cfg.nxSegment);
        printf(" xcut = ");
        for(size_t k=0;k < oop.x().size();k++) printf(" %8.4g ",oop.x()[k]);
        printf(" ycut = ");
        for(size_t k=0;k < oop.y().size();k++) printf(" %8.4g ",oop.y()[k]);
        printf(" s = ");
        for(size_t k=0;k < oop.s().size();k++) printf(" %8.4g ",oop.s()[k]);
      }
      for(int k=0;k<cfg.nxSegment;k++) {
        xsolve[i++] = oop.x()[j] + dx*k;
      }
    }
    double dx = (maxx - oop.x()[nseg-1])/(NX_CAT-1);
    if (isinf(dx) || isnan(dx)) {
      printf(" j=%d, dx=%f, nxSegment=%d\n", nseg-1, dx,NX_CAT);
      printf(" xcut = ");
      for(size_t k=0;k<oop.x().size();k++) printf(" %8.4g ",oop.x()[k]);
    }
    for(int k=0;k<NX_CAT;k++) {
      xsolve[i++] = oop.x()[nseg-1] + dx*k;
    }
    if (i != nx) {
      cout << "ERRROR data::resetXsolve() bad indices" << endl;      
      exit(1);
    }
  } else { // if(ready)
    xsolve = vector<double>();
  }  
  return(xsolve);
}

void data::resetXsolve() {
  double maxx = 0, maxc = 0;
  for(size_t i=0;i<cumCost.size();i++) {
    maxx = cumCost[i]>maxx? cumCost[i]:maxx;
    maxc = cost[i]>maxc? cost[i]:maxc;
  }
  maxx += maxc;
  double maxo = 0;
  for(size_t i=0;i<oop.size();i++) {
    if (oop[i].ready()) maxo = oop[i].x().back()>maxo? oop[i].x().back():maxo;
  }
  maxo += 100;
  maxx = maxo>maxx? maxo:maxx;
  cout << "maximum x in value function grid = " << maxx << ", max oop.xcut=" << maxo << endl << flush;
  xsolve = vector< vector<double> >(nplan);
  nx = vector<int>(nplan);
  for(int p=0;p<nplan;p++) {
    xsolve[p] = setXsolve(oop[p],maxx);
    nx[p] = xsolve[p].size();
  } // for(p)
}

void data::resetEeoyPrices(array<double,3> cumCost) {
  if (!dataNS::cfg.terminalV) return; // nothing to do if not terminalV model
  // in data creation, plans were already created for each real plan
  // and risk score bin, so can just calculate average price for each
  // plan
  vector<size_t> nobs(nplan);
  vector<double> exEOYprice(nplan);
  for(int i=0;i<N;i++) {
    for(int y=0;y<Years;y++) {
      if (in(i,y)) {
        exEOYprice[plan(i,y)] += oop[plan(i,y)].marginalPrice(cumCost(i,y));
        nobs[plan(i,y)]++;
      }
    }
  }
  for(int p=0;p<nplan;p++) {
    exEOYprice[p] /= nobs[p];
    oop[p].resetEeoyPrice(exEOYprice[p]);
  }
}

/********************************************************************************/

parameters::parameters(const vector<double> &xin) {
  static vector<double> xall;
  vector<double> x;
  if (dataNS::cfg.onlyTrans) {
    size_t np = (dataNS::cfg.nLambda*(dataNS::cfg.nLambda-1) + dataNS::cfg.claimLambda);
    if (xin.size()>np) {
      cout << "WARNING: parameters passed full vector even though estimating onlyTrans" << endl << flush;
      xall = xin;
      x = xall;
    } else {
      if ( xin.size()!=np) {
        cout << "ERROR parameters: xin wrong size" << endl << flush;
        exit(1);
      }
      x = xall;
      for (size_t i=0;i<np;i++) x[x.size()-np+i] = xin[i];
    }
  } else {
    x = xin;
  }
  int j=0;
  this->xvec = x;
  this->nThetaMix = dataNS::cfg.NMIX;
  this->omega = vector< omegaDist<double> > (this->nThetaMix);
  this->thetaD = vector< thetaDist<double> >(this->nThetaMix);
  this->lambda = array<double,2>(this->nThetaMix,dataNS::cfg.nLambda);
  this->nTheta = cfg.nint; 
  this->thetaV = vector<double>(this->nTheta*this->nThetaMix);
  this->thetaP = vector<double>(this->nTheta*this->nThetaMix);
    
  this->thetaD[0].mu = x[j++];
  for(int i=1;i<this->nThetaMix;i++) this->thetaD[i].mu = (x[j++] + thetaD[i-1].mu);
  for(int i=0;i<this->nThetaMix;i++) this->thetaD[i].sig = x[j++];

  const int nx = dataNS::cfg.NX+1;
  this->betaMixP = array<double,2>(nThetaMix,nx);
  for(int k=0;k<nx;k++) this->betaMixP(0,k) = 0;
  for(int i=1;i<this->nThetaMix;i++) {
    for(int k=0;k<nx;k++) this->betaMixP(i,k) = x[j++];
  }
  
  for(int i=0;i<this->nThetaMix;i++) { double p = x[j++]; this->omega[i].setparam(&p); }
  vector<double> lr(this->lambda.dimSize(1));
  for(int i=0;i<this->nThetaMix;i++) {
    for(int k=0;k<(int) this->lambda.dimSize(1);k++) {
      if (dataNS::cfg.singleLambdaRatio) {
        if (k==0) this->lambda(i,k) = x[j++];
        else {
          if (i==0) lr[k] = x[j++];
          this->lambda(i,k) = this->lambda(i,k-1)*lr[k];
        }
      } else {     
        this->lambda(i,k) = x[j++];
        if (k>0) this->lambda(i,k)*=this->lambda(i,k-1); 
      }
    }
  }
  if (dataNS::cfg.fixedDelta>=0) {
    this->delta = dataNS::cfg.fixedDelta;
  } else {
    if (dataNS::cfg.noUncertainty) {
      this->delta = 1.0;
    } else {
      this->delta = x[j++];
    }
  }
  if (dataNS::cfg.terminalV) {
    this->deltaTheta = x[j++];
    this->deltaOmega = this->deltaTheta;
  } else {
    this->deltaTheta=this->deltaOmega = 0;
  }
  
  if (dataNS::cfg.estRiskAversion) this->riskAversion = exp(x[j++]);
  else this->riskAversion = dataNS::cfg.riskAversion;

  this->transitionProb = array<double,3>(2,dataNS::cfg.nLambda,dataNS::cfg.nLambda);
  for(int mold=0;mold<dataNS::cfg.nLambda;mold++) {
    double sum = 0;
    for(int mnew=0;mnew<(dataNS::cfg.nLambda);mnew++) {
      if (mnew==mold) sum += (this->transitionProb(0,mnew,mold)=1.0) ;
      else sum += (this->transitionProb(0,mnew,mold)=x[j++]) ;
    }
    for(int mnew=0;mnew<dataNS::cfg.nLambda;mnew++) {
      this->transitionProb(0,mnew,mold) /= sum;
      this->transitionProb(1,mnew,mold) = this->transitionProb(0,mnew,mold);
    }
  }
  if (dataNS::cfg.claimLambda) {
    double rescale = x[j++];
    if (dataNS::cfg.nLambda!=2) {
      cout << "claims affecting lambda only implemented for two lambda types" << endl;
    }
    for(int mold=0;mold<dataNS::cfg.nLambda;mold++) {
      this->transitionProb(0,1,mold) *= rescale;
      this->transitionProb(0,0,mold) = 1.0 - this->transitionProb(0,1,mold);
    }
  }
  if (dataNS::cfg.omegaThetaDep) {
    this->pOT = x[j++];
  } else {
    this->pOT = 0;
  }
  // find stationary distribution of lambdaIndex
  this->startProb = vector<double>(dataNS::cfg.nLambda);
  { 
    alglib::real_2d_array PmI;
    alglib::real_1d_array b,sp;
    PmI.setlength(dataNS::cfg.nLambda+1,dataNS::cfg.nLambda);
    b.setlength(dataNS::cfg.nLambda+1);
    for(int i=0;i<dataNS::cfg.nLambda;i++) {
      b[i]  = 0;
      for(int k=0;k<dataNS::cfg.nLambda;k++) {
        PmI(i,k) = this->transitionProb(1,i,k) - 1.0*(i==k);
      }
    }
    int i = dataNS::cfg.nLambda;
    b[i] = 1.0;
    for(int k=0;k<dataNS::cfg.nLambda;k++) {
      PmI(i,k) = 1.0;
    }
    alglib::ae_int_t info;
    alglib::densesolverlsreport rep;
    alglib::rmatrixsolvels(PmI,dataNS::cfg.nLambda+1,dataNS::cfg.nLambda,b, 0.0, info,rep,sp);
    if (info!=1) {
      cout << "WARNING: alglib::rmatrixsovlels info=" << info << endl;
    }
    for(int i=0;i<dataNS::cfg.nLambda;i++) {
      this->startProb[i] = sp[i];
    }
  }  
    
  if (j!=(int) x.size()) {
    cout << "Error in parameters() j=" << j << " x.size()=" << x.size() << endl;
    exit(10);
  }
  this->gaussHermiteQuad = dataNS::cfg.gaussHermiteQuad;
  this->maxThetaQuantile = dataNS::cfg.maxThetaQuantile;
  
  this->resetIntegration();

  // compute powers of delta
  powDelta.resize(53);
  powDeltaTheta.resize(powDelta.size());
  powDeltaOmega.resize(powDelta.size());
  powDelta[0] = 1;
  powDeltaTheta[0] = 1;
  powDeltaOmega[0] = 1;
  for (size_t t=1;t<powDelta.size();t++) {
    powDelta[t] = delta*powDelta[t-1];
    powDeltaTheta[t] = deltaTheta*powDeltaTheta[t-1];
    powDeltaOmega[t] = deltaOmega*powDeltaOmega[t-1];
  }

  if (dataNS::cfg.noUncertainty && this->delta < (1 - 1e-12)) {
    cout << "ERROR: noUncertainty requires delta = 1" << endl;
    exit(1);
  }}


void parameters::resetIntegration() {
  if (this->gaussHermiteQuad) {
    static alglib::real_1d_array xint;
    static int nx =0;
    static alglib::real_1d_array wint;
    if (nx!=this->nTheta) {
      alglib::ae_int_t info;
      nx = this->nTheta;      
      this->thetaV.resize(this->nTheta*this->nThetaMix);
      this->thetaP.resize(this->nTheta*this->nThetaMix);
      alglib::gqgenerategausshermite(nx,info,xint,wint);
      if (info!=1) {
        cout << "Error gqgenerategausshermite info = " << info << endl;
        exit(info);
      }
    }
    for(int m=0;m<this->nThetaMix;m++) {
      for(int i=0;i<this->nTheta;i++) {
        this->thetaV[i + m*this->nTheta] = exp(xint[i]*sqrt(2)*this->thetaD[m].sig + this->thetaD[m].mu);
        this->thetaP[i + m*this->nTheta] = wint[i]/sqrt(M_PI);
      }
    }  
  } else {
    this->thetaV.resize(this->nTheta*this->nThetaMix);
    this->thetaP.resize(this->nTheta*this->nThetaMix);
    if (this->maxThetaQuantile<1) {
      double maxz = alglib::invnormaldistribution(this->maxThetaQuantile);
      vector<double> xint, wint;
      for(int m=0;m<this->nThetaMix;m++) {
        gaussQuad::lognormalPDF pdf( this->thetaD[m].mu, this->thetaD[m].sig, 
                                     exp( (maxz + this->thetaD[m].mu) * 
                                          this->thetaD[m].sig) ); 
        gaussQuad::gaussianQuadrature(&pdf, (unsigned int) this->nTheta, xint, wint);
        for(int i=0;i<this->nTheta;i++) {
          this->thetaV[i + m*this->nTheta] = xint[i];
          this->thetaP[i + m*this->nTheta] = wint[i];
        }
      }
    } else {
      vector<double> xint, wint;
      for(int m=0;m<this->nThetaMix;m++) {
        gaussQuad::lognormalPDF pdf( this->thetaD[m].mu, this->thetaD[m].sig);
        gaussQuad::gaussianQuadrature(&pdf, (unsigned int) this->nTheta, xint, wint);
        for(int i=0;i<this->nTheta;i++) {
          this->thetaV[i + m*this->nTheta] = xint[i];
          this->thetaP[i + m*this->nTheta] = wint[i];
        }
      } 
    }
  } // end else (! gaussHermiteQuad)
  
  // debugging info
  /*
    if (1) {
    for(int m=0;m<this->nThetaMix;m++) {
    cout << "mu = " << thetaD[m].mu << " sig = " << thetaD[m].sig << " E[x]=" << 
    exp(thetaD[m].mu + thetaD[m].sig*thetaD[m].sig*0.5) << endl << "      ";
    double e0=0, e1=0, e2=0;
    for(int i=0;i<nTheta;i++) {
    e0 += thetaP[i + m*nTheta];
    e1 += thetaV[i+m*nTheta]*thetaP[i+m*nTheta];
    e2 += thetaV[i+m*nTheta]*thetaV[i+m*nTheta]*thetaP[i+m*nTheta];
    }
    cout << " E[1]=" << e0 << " E[x]=" << e1 << " E[x^2]=" << e2 << endl;
    }
    }
  */

} // end resetIntegration()

// config class
void config::setDefaults() {
  // set defaults
  NMIX = 5;
  NX = 2;
  estRiskAversion = false;
  nLambda = 2; 
  singleLambdaRatio = true;
  claimLambda = false;

  probKeep_default = 0.1;
  probKeep65_default = 0.1;
  nplanUse_default = 500;  // make negative to use all plans
  useHistEM = true;  // use histogram or kernel excess mass moments
  nbinEM = 21;  // number of bins for excess mass
  limEM = 500; // excess mass bins go this far from ICL on each side
  nbinTiming = 2; // set to 2 to just use before ICL & near ICL
  diffTiming = 0; // set to 0 for levels, 1 for diff pre-kink, 2 for diff across adjacent months 
  limTiming = 500;
  regularize = 0; // adds I*regularize to V(m) for weighting
  nlags = -1; // number of lags to match P(claim t & claim t-lag), set to -1 for none
  useWeeks = false; // use weeks to first claim moments?
  useSpendAutoCov = false; // use autocovariance of spending as moments
  useMonthlyProb = true; // use P(claim in month or later | total spending) moments?
  nMonthlyProb = 6; // number of monthly timing moments December, November, etc
  nxSegment = 15;
  nint = 20;
  gaussHermiteQuad = true; // use gauss hermite quadrature?
  maxThetaQuantile = 1.1; // anything>=1 means no maximum
  riskAversion = 0.0;
  useSimulatedData = false; // estimate on simulated data?
  positiveCor = false; // constrain autocorrelation to be positive?
  onlyTrans = false; // only estimate transition matrix?

  terminalV = false; // include extra terminal value function
  useExcessJanuary = false; 

  omegaThetaDep = false;
  noUncertainty = false;
  fixedDelta = -1;
  weekLength = 7;

  nRiskBin = 1;
  //deltaTheta = 1.0; // 1-depreciation of theta stock
  //deltaOmega = 1.0; // 1-depreciation of omega stock

  NPARAM = NMIX*(4+NX) - NX + 1*estRiskAversion +  
    nLambda*(nLambda-1) +
    (singleLambdaRatio?(nLambda-1+NMIX):(NMIX*nLambda) ) + 
    terminalV + claimLambda + omegaThetaDep + (noUncertainty? -1:0);  
}

config::config() {
  setDefaults();
}

config::config(const char *filename) {
  setDefaults(); 

  std::string id, eq, val;
  ifstream cfgfile;
  int line=0;
  cfgfile.open(filename,std::ios::in);
  
  while(cfgfile >> id >> eq >> val) {
    line++;
    if (id[0] == '#') continue;  // skip comments
    if (eq != "=") { 
      std::cerr << "Parse error on line " << line 
                << " expected \"=\", found " << eq
                << std::endl;
      exit(1);
    }
    std::istringstream iv(val);
    iv.setf(std::ios_base::boolalpha);
    if (id == "NMIX") iv >> NMIX;   
    else if (id == "NX") iv >> NX;
    else if (id == "estRiskAversion") iv >> estRiskAversion;
    else if (id == "nLambda") iv >> nLambda;
    else if (id == "singleLambdaRatio") iv >> singleLambdaRatio;
    else if (id == "probKeep_default") iv >> probKeep_default;
    else if (id == "probKeep65_default") iv >> probKeep65_default;
    else if (id == "nplanUse_default") iv >> nplanUse_default;
    else if (id == "useHistEM") iv >> useHistEM;
    else if (id == "nbinEM") iv >> nbinEM;
    else if (id == "limEM") iv >> limEM;
    else if (id == "nbinTiming") iv >> nbinTiming;
    else if (id == "diffTiming") iv >> diffTiming;
    else if (id == "limTiming") iv >> limTiming;
    else if (id == "regularize") iv >> regularize;
    else if (id == "nlags") iv >> nlags;
    else if (id == "useWeeks") iv >> useWeeks;
    else if (id == "useSpendAutoCov") iv >> useSpendAutoCov;
    else if (id == "useMonthlyProb") iv >> useMonthlyProb;
    else if (id == "nMonthlyProb") iv >> nMonthlyProb;
    else if (id == "nxSegment") iv >> nxSegment;
    else if (id == "nint") iv >> nint;
    else if (id == "gaussHermiteQuad") iv >> gaussHermiteQuad;
    else if (id == "maxThetaQuantile") iv >> maxThetaQuantile;
    else if (id == "riskAversion") iv >> riskAversion;
    else if (id == "useSimulatedData") iv >> useSimulatedData;
    else if (id == "terminalV") iv >> terminalV;
    else if (id == "nRiskBin") iv >> nRiskBin;
    else if (id == "useExcessJanuary") iv >> useExcessJanuary;
    else if (id == "onlyTrans") iv >> onlyTrans;
    else if (id == "positiveCor") iv >> positiveCor;
    else if (id == "omegaThetaDep") iv >> omegaThetaDep;
    else if (id == "noUncertainty") iv >> noUncertainty;
    else if (id == "fixedDelta") iv >> fixedDelta;
    else if (id == "weekLength") iv >> weekLength;
    //else if (id == "deltaTheta") iv >> deltaTheta;
    //else if (id == "deltaOmega") iv >> deltaOmega;
    else {
      std::cerr << "option " << id << " not recognized." << std::endl;
      exit(1);
    }
  }
  cfgfile.close();
  
  NPARAM = NMIX*(4+NX) - NX + 1*estRiskAversion +  
    nLambda*(nLambda-1) +
    (singleLambdaRatio?(nLambda-1+NMIX):(NMIX*nLambda) ) + 
    terminalV + claimLambda + omegaThetaDep + ((noUncertainty || fixedDelta>=0)? -1:0);  
  if ((estRiskAversion || riskAversion>0) && maxThetaQuantile>=1) {
    std::cerr << "ERROR: estimating with risk aversion and maxThetaQuantile>=1 results " << std::endl
              << " in infinite expected utility. Set maxThetaQuantile<1 (e.g. 0.99) for "  << std::endl
              << "estimating with risk aversion" << std::endl;
    exit(1);
  }
}

void config::save() {
  time_t now = time(0);
  char name[60];
  strftime(name,sizeof(name),"config_%Y-%m-%d_%H.%M.%S.txt", localtime(&now));
  this->save(name);
}

void config::save(const char *filename) {
  std::ofstream cfgfile;
  cfgfile.open(filename,std::ios::out);
  cfgfile  << std::boolalpha 
           << "NMIX = " << NMIX << std::endl
           << "NX = " << NX << std::endl
           << "estRiskAversion = " << estRiskAversion << std::endl
           << "nLambda = " << nLambda << std::endl
           << "singleLambdaRatio = " << singleLambdaRatio << std::endl
           << "probKeep_default = " << probKeep_default << std::endl
           << "probKeep65_default = " << probKeep65_default << std::endl
           << "nplanUse_default = " << nplanUse_default << std::endl
           << "useHistEM = " << useHistEM << std::endl
           << "nbinEM = " << nbinEM << std::endl
           << "limEM = " << limEM << std::endl
           << "nbinTiming = " << nbinTiming << std::endl
           << "diffTiming = " << diffTiming << std::endl
           << "limTiming = " << limTiming << std::endl
           << "regularize = " << regularize << std::endl
           << "nlags = " << nlags << std::endl
           << "useWeeks = " << useWeeks << std::endl
           << "useSpendAutoCov = " << useSpendAutoCov << std::endl
           << "useMonthlyProb = " << useMonthlyProb << std::endl
           << "nMonthlyProb = " << nMonthlyProb << std::endl
           << "nxSegment = " << nxSegment << std::endl
           << "nint = " << nint << std::endl
           << "gaussHermiteQuad = " << gaussHermiteQuad << std::endl
           << "maxThetaQuantile = " << maxThetaQuantile << std::endl
           << "riskAversion = " << riskAversion << std::endl
           << "useSimulatedData = " << useSimulatedData << std::endl
           << "terminalV = " << terminalV << std::endl
           << "useExcessJanuary = " << useExcessJanuary << std::endl
           << "onlyTrans = " << onlyTrans << std::endl
           << "positiveCor = " << positiveCor << std::endl
           << "nRiskBin = " << nRiskBin << std::endl
           << "omegaThetaDep = " << omegaThetaDep << std::endl
           << "noUncertainty = " << noUncertainty << std::endl
           << "fixedDelta = " << fixedDelta << std::endl
           << "weekLength = " << weekLength << std::endl; 
    //<< "deltaTheta = " << deltaTheta << std::endl
    //      << "deltaOmega = " << deltaOmega << std::endl
    ;
  cfgfile.close();
}
