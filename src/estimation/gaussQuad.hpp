#ifndef _GAUSSQUAD_HPP
#define _GAUSSQUAD_HPP

#include<vector>

namespace gaussQuad {

  class domain {
  public:
    bool loFinite;
    bool hiFinite;
    double lo;
    double hi ;
  };

  class PDF {
  public:
    virtual double operator()(double)  const = 0;
    virtual domain getDomain() const = 0;
  };
  
  class normalPDF : public PDF {
  public:
    double mu;
    double sig;
    normalPDF();
    normalPDF(double m, double s);
    double operator()(double x) const;
    domain getDomain() const;
  };

  class lognormalPDF : public PDF {
  public:
    double mu;
    double sig;
    bool truncated;
    double max;
    double probNotTruncated;
    lognormalPDF();
    lognormalPDF(double m, double s);
    lognormalPDF(double m, double s, double hi);
    double operator()(double x) const;
    domain getDomain() const;
  };
  
  int gaussianQuadrature(PDF *f, unsigned int n, 
                         std::vector<double> &x, std::vector<double> &w);
  /* 
     Given pdf f and number of points n, returns Gaussian Quadrature
     absisca x and weights w. Return value is 0 on sucess, nonzero on error.
  */  
  
}; // end namespace gaussQuad
#endif
