#ifndef _OOPFUNC_HPP
#define _OOPFUNC_HPP

#include <vector>
using std::vector;

class oopFunc
{
 public:
  oopFunc();
  void setup(const double* m,const double *x,const double *y,const int nseg,
             double eoyPrice=0);
  void setup(const double ded, const double icl, const double cat, 
             const double *rate, double eoyPrice=0);
  double eval(const double theta, const double x) const;
  void eval(double *dy, const double *theta, const double *x, int nx) const;
  int closeHole(const int keepCatOop);
  // set keepCatOop to 0 to keep the catastrophic the same
  // in terms of total spending, 1 to keep same in terms of out of
  // pocket spending  
  int zeroDed(); // reset deductible to 0
  int ded(const double d, const int keepCatOop);
  // getters
  double ded() const;
  double icl() const;
  double cat() const;

  vector<double> const& x() const;
  vector<double> const& y() const;
  vector<double> const& s() const;
  bool ready() const;
  double eoyPrice() const;
  double marginalPrice(double x);
  void resetEeoyPrice(double newprice);
 private:
  void check();
  vector<double> xcut,ycut,slope;
  double EeoyPrice;
  bool initialized;
};

#endif
