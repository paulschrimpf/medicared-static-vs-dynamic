#include <omp.h>
#include <vector>
#include <fstream>
#include <iostream> 
#include <sstream> 
#include <iomanip>
#include <ctime>
#include <string>

#include "oopFunc.hpp"
#include "omegaDist.hpp"
#include "data.hpp"
#include "computeValueFunction.hpp"

#include "simMM.hpp"
#include "fastSpline.hpp"

extern "C" {
#include "cmaes_interface.h"
}

using namespace fastSpline;

using namespace std;

/**********************************************************************/
double cmaObj(double const *x, int n) {
  static momentData *md=NULL;
  if (n<0) {
    md = (momentData*) x;
    return(0.0);
  }
  if (md==NULL) {
    cout << "ERROR" << endl;
    exit(-1);
  }
  vector<double> xv(x, x + (n));
  vector<double> g;
  return(simMMobjective(xv,g,(void*) md));
}

int is_feasible(double const *x) {
  const int nmix = cfg.NMIX;
  const int nparam =  cfg.NPARAM;
  vector<double> lb(nparam); // lower bound
  vector<double> ub(nparam); // upper bound
  size_t j = 0;
  // bounds on mu
  lb[j] = -5;  ub[j++] = 10; 
  for (int i=1;i<nmix;i++) {
    lb[j] = 0.01; ub[j++] = 5;
  }
  // bounds on sigma
  for (int i=0;i<nmix;i++) {
    lb[j] = 0.05; ub[j++] = 7;
  }
  // bounds on logit(p)
  for (int i=0;i<(nmix-1)*(cfg.NX+1);i++) {
    lb[j] = -10; ub[j++] = 10;
  }
  // bounds on p_omega
  for (int i=0;i<nmix;i++) {
    lb[j] = 0.0; ub[j++] = 1.0;
  }
  // bounds on lambda
  if (dataNS::cfg.singleLambdaRatio) {
    for(int i=0;i<nmix;i++) {
      lb[j]=0.0; ub[j++]=1.0;
    } 
    for (int k=0;k<(dataNS::cfg.nLambda-1);k++) {
      lb[j]=0.0; ub[j++]=1.0;
    }
  } else {
    for (int i=0;i<nmix;i++) {
      for(int k=0;k<dataNS::cfg.nLambda;k++) {
        lb[j]=0.0; ub[j++]=1.0;
      }
    }
  }
  // delta
  if (!dataNS::cfg.noUncertainty && dataNS::cfg.fixedDelta<0) {
    lb[j] = 0.3; ub[j++] = 1.0;
  }
  // deltaTheta = deltaOmega
  if (dataNS::cfg.terminalV) {
    lb[j] = 0.1; ub[j++] = 1.0;
  }
  // riskAversion
  if (dataNS::cfg.estRiskAversion) {
    lb[j] = -20.0; ub[j++] = 0.0;
  }
  // transition probabilities
  for (int i=0;i<dataNS::cfg.nLambda;i++) {
    for (int k=0;k<(dataNS::cfg.nLambda-1);k++) {
      lb[j] = 0.0; 
      if (dataNS::cfg.positiveCor) ub[j++] = 1.0; 
      else ub[j++] = 50.0;
    }
  }
  if (dataNS::cfg.omegaThetaDep) {
    lb[j] = -200.0;
    ub[j++] = 200.0;
  }
  if (j!=lb.size() || j!=ub.size()) {
    cout << "bounds are wrong size " << j << " " << lb.size() << " " << ub.size() << endl;
  }
  
  if (dataNS::cfg.onlyTrans) {
    size_t np = (dataNS::cfg.nLambda*(dataNS::cfg.nLambda-1) + dataNS::cfg.claimLambda);
    for (size_t j=0;j<np;j++) {
      if (x[j]<lb[lb.size()-np+j] || x[j]>ub[ub.size()-np+j]) return(0);
    }
  } else {
    for(j=0;j<(size_t) nparam;j++) {
      if (x[j]<lb[j] || x[j]>ub[j]) return(-j);
    }
  }
  return(1);
}
/**********************************************************************/
int main(int argc, char *argv[])
{
  if (argc<2) {
    cout << "must provide 2 input arguments" << endl;
    return(10);    
  }
  
  // read configuration
  if (argc<3) {
    cout << "warning: no configuration file specified, using default values" << endl;
  } else {
    cfg = config(argv[2]);
  }  
  // load data
  data dat(argv[1]);

  const int nmix = dataNS::cfg.NMIX;
  int nparam = dataNS::cfg.NPARAM;
  vector<double> x0(nparam); // initial value
  vector<double> x; // maximizer
  
  // read initial values from file
  // comments begining with "#" are ignored
  {
    std::ifstream infile;
    infile.open("x0.txt",ifstream::in);
    if (!infile.is_open()) {
      cout << "WARNING: Failed to open x0.txt." << endl ;
      exit(10);
    } else {
      int i = 0;
      std::string line; 
      while (std::getline(infile, line) && i<nparam) {
        size_t comment = line.find("#");
        if (comment!=std::string::npos) line.erase(comment, std::string::npos);
        std::istringstream iss(line);
        double num;
        while (i < nparam && iss>>num) {
          x0[i++] = num;
        }
      }
      if (i<nparam) {
        cout << "WARNING: Found " << i 
             << " initial values in x0.txt, but expected " 
             << nparam << endl;
      }
    }
    infile.close();
  }
  

  
  momentData md;
  parameters pm(x0);
  md.dat = &dat;
  const int nsim = 1;
  md.rand = vector<randomDraws>(nsim);
  for(int i=0;i<nsim;i++) {
    md.rand[i] = randomDraws(&dat,nmix,452 + i*3984852, 3925 + i*9789);
  }
  
  if (dataNS::cfg.useSimulatedData) {
    // simulate data and then estimate to test the code
    vector<randomDraws> rand(1);
    // magic numbers are just rng seeds and are not magic
    rand[0] = randomDraws(&dat,nmix,49586, 120549741325);
    valueFunction *vf;
    vf = createValueFunctions(dat,pm);
    vector<simData> sd = simulate(pm,dat,rand,vf);
    destroyValueFunctions(vf);
    data originalData(dat);
    cout << " Option useSimulatedData selected, replacing observed drug " 
         << "spending with simulated spending. " << endl;
    // replace cost and cumulative cost with their simulated counterparts
    dat.cost = sd[0].cost;
    dat.cumCost = sd[0].cumCost;  
    // modify x0 by up to 10%
    alglib::hqrndstate state;
    alglib::hqrndseed(120893,138,state);
    for(size_t i =0;i<x0.size();i++) {
      x0[i] *= (1.0 + (alglib::hqrnduniformr(state)-0.5)*0.1);
    }
  }
  
  // optimization settings  
  vector<double> g;
  cout << "calling objective" << endl << flush;
  cout << "obj(x0) = " << simMMobjective(x0,g,(void*) &md) << endl;
  cmaObj((double*) & md, -1);
  cout << "obj(x0) = " << cmaObj(&x0[0], x0.size()) << endl;
  
  // use CMA to minimize
  if (1) {
    cmaes_t evo; /* an CMA-ES type struct or "object" */
    double *arFunvals, *const*pop, *xfinal;
    int i; 
    bool firstTime=true;
    cmaObj((double*) & md, -1);
    
    /* Initialize everything into the struct evo, 0 means default */
    arFunvals = cmaes_init(&evo, 0, NULL, NULL, 0, 0, "initials.par"); 
    printf("%s\n", cmaes_SayHello(&evo));
    cmaes_ReadSignals(&evo, "signals.par");  /* write header and initial values */
    
    /* Iterate until stop criterion holds */
    while(!cmaes_TestForTermination(&evo))
      { 
        /* generate lambda new search points, sample population */
        pop = cmaes_SamplePopulation(&evo); /* do not change content of pop */
        
        if (firstTime && !dataNS::cfg.onlyTrans) { 
          /* Insert x0 into pop */
          for(size_t j=0;j<x0.size();j++) {
            // pop[0][j] = x0[j]; 
            evo.rgrgx[0][j] = x0[j];
            cout << "x0[" << j << "] = " << x0[j] << " , pop[0][" << j << "]=" << pop[0][j] << endl;
          }
          firstTime = false;
        }

        /* Here you may resample each solution point pop[i] until it
	 becomes feasible, e.g. for box constraints (variable
	 boundaries). function is_feasible(...) needs to be
	 user-defined.  
	 Assumptions: the feasible domain is convex, the optimum is
	 not on (or very close to) the domain boundary, initialX is
	 feasible and initialStandardDeviations are sufficiently small
	 to prevent quasi-infinite looping.
        */
        for (i = 0; i < cmaes_Get(&evo, "popsize"); ++i)  {
          int tries=0;
          while (is_feasible(pop[i])<0 && tries<500) {
            tries++;
            cmaes_ReSampleSingle(&evo, i); 
          }
          if (tries==500) {
            cout << "WARNING: Failed to sample pop["<< i << "] after 500 tries" << endl 
                 << "problem with parameter " << -is_feasible(pop[i]) << endl;
          }
        }
        
        /* evaluate the new search points using fitfun from above */ 
        for (i = 0; i < cmaes_Get(&evo, "lambda"); ++i) {
          arFunvals[i] = cmaObj(pop[i], (int) cmaes_Get(&evo, "dim"));
        }
        
        /* update the search distribution used for cmaes_SampleDistribution() */
        cmaes_UpdateDistribution(&evo, arFunvals);  
        
        /* read instructions for printing output or changing termination conditions */ 
        cmaes_ReadSignals(&evo, "signals.par");   
        fflush(stdout); /* useful in MinGW */
      }
    printf("Stop:\n%s\n",  cmaes_TestForTermination(&evo)); /* print termination reason */
    cmaes_WriteToFile(&evo, "all", "allcmaes.dat");         /* write final results */
    
    /* get best estimator for the optimum, xmean */
    xfinal = cmaes_GetNew(&evo, "xbestever"); /* "xbestever" might be used as well */
    cmaes_exit(&evo); /* release memory */ 
    
    // save results 
    time_t now = time(0);
    char name[60];
    ofstream estfile;
    vector<double> x(xfinal,xfinal+x0.size());
    vector<double> g;
    strftime(name,sizeof(name),"smmEstCMA_%Y-%m-%d_%H.%M.%S.txt", localtime(&now));
    estfile.open(name,ios::out);
    int prec = 16;
    int wid = 22;
    estfile << "x0 ";
    for(size_t i=0;i<x0.size();i++) estfile << setw(wid) << setprecision(prec) << x0[i];
    estfile << endl;
    estfile << "x ";
    for(size_t i=0;i<x.size();i++) estfile << setw(wid) << setprecision(prec) <<  x[i];
    estfile << endl;
    estfile << "obj " << setw(wid) << setprecision(prec) << simMMobjective(x,g,(void*) &md) << endl;;
    estfile.close(); 
    // save config
    cfg.save();
    /* do something with final solution and finally release memory */
    free(xfinal); 
  }


  return(0);
}

