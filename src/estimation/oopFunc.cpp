#include "oopFunc.hpp"
#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <cmath>

using std::cout;
using std::endl;

oopFunc::oopFunc() { 
  initialized = false;
  xcut = ycut = slope = vector<double>(3,-999.0);
}  
void oopFunc::setup(const double* m,const double *x,const double *y,const int nseg, double eoyP) {
  slope = vector<double>(m,m+nseg);
  xcut = vector<double>(x,x+nseg);
  ycut = vector<double>(y,y+nseg);
  EeoyPrice = eoyP;
  initialized = true;
  this->check();
}

void oopFunc::check() {
  for(size_t i=0;i<slope.size();i++) {
    if (slope[i]<0) cout << "warning: slope[" << i << "] = " << slope[i] << endl;
    if (xcut[i]<0) cout << "warning: xcut[" << i << "] = " << xcut[i] << endl;
    if (ycut[i]<0) cout << "warning: ycut[" << i << "] = " << ycut[i] << endl;
    if (i>0) {
      double ev = ycut[i-1] + slope[i-1]*(xcut[i]-xcut[i-1]);
      if (fabs(ev-ycut[i])>1e-4) {
        cout << "warning: discontinuous at xcut[" << i << "] = " << xcut[i] 
             << ". ycut[" << i << "] = " << ycut[i] << ", limit from below = "
             << ev << endl;
      }
    }
  }

}


void oopFunc::setup(const double ded, const double icl, const double cat, 
                    const double *rate, double eoyP) 
{
  double m[4];
  std::copy(rate,rate+4,m);
  if (m[0]<0 && ded>0) {
    // there are ten plans with m[0]<0 and ded>0. These plans have a
    // coin_icl similar to the deductible plans, therefore for these
    // ten plan I assign coin_ded to its median among deductible plan,
    // which is 0.986.
    m[0] = 0.986; 
    cout << "Warning: setting missing coin_ded to 0.986" << endl;
  }
  double x[4] = {0, ded, icl, icl+(cat- (icl-ded)*m[1]-ded*m[0])/(m[2]==0? 0.01:m[2])};
  double y[4] = {0, ded*m[0], (icl-ded)*m[1]+ded*m[0], cat};
  if (m[2]==0) {
    printf("warning: icl-cat coins = 0\n");
    printf(" icl=%f cat=%f rate = %f %f %f %f\n",icl,cat,m[0],m[1],m[2],m[3]);
    if (ded<=0 || m[0]<0) this->setup(m+1,x+1,y+1,2,eoyP);
    else this->setup(m,x,y,3,eoyP);
  } else {
    if (ded<=0 || m[0]<0) this->setup(m+1,x+1,y+1,3,eoyP);
    else this->setup(m,x,y,4,eoyP);
  }
}

double oopFunc::eval(const double theta, const double x) const {
#ifndef NDEBUG
  if (!initialized) {
    cout << "Error double oopFunc::eval() not initialized";
    exit(1);
  }
#endif
  double yx=0, yxpt=0;
  int nseg = xcut.size();
  for(int j=0;j<nseg;j++) {
    if (j==(nseg-1) || x<=xcut[j+1]) {
      yx = ycut[j] + (x-xcut[j])*slope[j];
      for(int k=j;k<nseg;k++) {
        if (k==(nseg-1) || x+theta <= xcut[k+1]) {
          yxpt = ycut[k] + (x+theta-xcut[k])*slope[k];
          break;
        }
        }
      break;
    }
  }
  return(yxpt-yx);
}

void oopFunc::eval(double *dy, const double *theta, const double *x, int nx) const {
  if (!initialized) {
    cout << "Error void oopFunc::eval() not initialized";
    exit(1);
  }
  double yx=0, yxpt=0;
  int nseg = xcut.size();
  for(int i=0;i<nx;i++) {
    for(int j=0;j<nseg;j++) {
      if (j==(nseg-1) || x[i]<=xcut[j+1]) {
        yx = ycut[j] + (x[i]-xcut[j])*slope[j];
        for(int k=j;k<nseg;k++) {
          if (k==(nseg-1) || x[i]+theta[i] <= xcut[k+1]) {
            yxpt = ycut[k] + (x[i]+theta[i]-xcut[k])*slope[k];
            break;
          }
        }
        break;
      }
    }
    dy[i] = yxpt-yx;
  }
  return;
}



int oopFunc::closeHole(const int keepCatOop) 
// set keepCatOop to 0 to keep the catastrophic the same
// in terms of total spending, 1 to keep same in terms of out of
// pocket spending  
{   
  if (!initialized) {
    return(1);
  }
  size_t h=(xcut.size()==4? 2:1); // index of icl in xcut
  if (slope[h-1]<slope[h]) { // do not do anything if "gap" coinsurance less than pre-gap
    if (slope[h-1]<slope[h+1]) { // 
      printf("WARNING: pre-icl coins=%.3f < %.3f=cat coins, using cat coins to fill gap\n",
             slope[h-1],slope[h+1]);
      slope[h] = slope[h+1];
    } else slope[h] = slope[h-1];
    h++;
    if (keepCatOop) {
      xcut[h] = (ycut[h]-ycut[h-1])/slope[h-1] + xcut[h-1];    
    } else {
      ycut[h] = ycut[h-1] + (xcut[h]-xcut[h-1])*slope[h-1];
    }  
  }
  return(0);
}

int oopFunc::ded(const double d, const int keepCatOop) {
  if (!initialized) {
    return(1);
    //cout << "Error oopFunc::ded() not initialized";
    //exit(1);
  }
  if (xcut.size() != 4) {
    //cout << "ERROR: oopFunc::ded() this plan has no deductible" << endl;
    //exit(1);
    return(0);
  }
  if (d<xcut[0] || d>xcut[2]) {
    //cout << "ERROR: oopFunc::ded() attempted to set deductible below 0 or above ICL" << endl;
    //exit(2);
  }
  xcut[1] = d;
  if (keepCatOop) {
    for (size_t j=1;j<(xcut.size()-1);j++) {
      ycut[j] = ycut[j-1] + (xcut[j]-xcut[j-1])*slope[j-1];
    }
    size_t j = xcut.size()-1;
    xcut[j] = (ycut[j]-ycut[j-1])/slope[j-1] + xcut[j-1];    
  } else {
    for (size_t j=1;j<xcut.size();j++) {
      ycut[j] = ycut[j-1] + (xcut[j]-xcut[j-1])*slope[j-1];
    }
  }
  return(0);
} 

int oopFunc::zeroDed() {
  if (!initialized) {
    return(1);
    //cout << "Error oopFunc::ded() not initialized";
    //exit(1);
  }
  if (xcut.size() != 4) {
    //cout << "ERROR: oopFunc::ded() this plan has no deductible" << endl;
    //exit(1);
    return(0);
  }
  double cat = ycut[3];
  double icl = xcut[2];
  double ded = 0.0;
  const double *rate = &(this->s()[0]);
  this->setup(ded,icl,cat,rate);  
  return(0);
} 

double oopFunc::ded() const
{   
  if (!initialized) {
    cout << "Error oopFunc::ded() oopFunc not initialized" << endl;
    exit(1);
  }
  return(xcut.size()==4? xcut[1]:0.0);  
}

double oopFunc::icl() const
{   
  if (!initialized) {
    cout << "Error oopFunc::icl() oopFunc not initialized" << endl;
    exit(1);
  }
  return(xcut.size()==4? xcut[2]:xcut[1]);  
}

double oopFunc::cat() const
{   
  if (!initialized) {
    cout << "Error oopFunc::cat() oopFunc not initialized" << endl;
    exit(1);
  }
  return(xcut.size()==4? ycut[3]:ycut[2]);  
}


vector<double> const& oopFunc::x() const {   
  if (!initialized) {
    cout << "Warning oopFunc::x() oopFunc not initialized" << endl;
  } 
  return(xcut);
 }
vector<double> const& oopFunc::y() const { 
  if (!initialized) {
    cout << "Warning oopFunc::y() oopFunc not initialized" << endl;
  }
  return(ycut);
}
vector<double> const& oopFunc::s() const { 
  if (!initialized) {
    cout << "Warning oopFunc::s(): oopFunc not initialized" << endl;
  }
  return(slope); 
}
bool oopFunc::ready() const { return(initialized); }

double oopFunc::eoyPrice() const { return(EeoyPrice); } 

void oopFunc::resetEeoyPrice(double newprice) { 
  EeoyPrice=newprice;
  initialized = (xcut[0]>=-0.01 && EeoyPrice>=0.0);
}

double oopFunc::marginalPrice(double x) {
  size_t j=0;
  while(x>xcut[j] && j<(xcut.size()-1)) j++;
  return(slope[j]);
}
