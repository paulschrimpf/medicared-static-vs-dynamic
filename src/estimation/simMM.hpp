#ifndef SIMMM_HPP
#define SIMMM_HPP
//#include "alglib/interpolation.h"
//#include "alglib/specialfunctions.h"

#include "data.hpp"
using namespace dataNS;
#include "oopFunc.hpp"
#include "omegaDist.hpp"
#include "fastSpline.hpp"

using fastSpline::spline1dinterpolant;

int findPlan(const int p,   double *up,const int nplan);

template<typename T, size_t dim> 
int findPlan(const int p,const array<T,dim> &up,const int nplan);


double simMM(vector<double> &grad,
             
             double *likei, 
             omegaDist<double> *omega, 
             thetaDist<double> *thetaD,
             const double *lambda, const double &delta,
             const double *thetaV, const double *thetaP,
             const int nTheta, 
             const double *thetaMixP, const int nThetaMix,
             oopFunc **oop,
             const int noop,
             spline1dinterpolant **value,
             const double *joinweek, 
             const double *plan,
             const double *cost, const double *cumCost,
             const double *costBin,
             const double *weight,
             const double *up,
             const int N, const int T,
             const int nplan,
             const int valueT);

class randomDraws {
private: 
  array<double,4> th;
  array<double,2> mixU;
  array<double,4> pOm;
  array<double,4> Om;
  array<double,5> lam;
  array<double,4> transU;
  vector<int> jw;
  
public:
  const data *dat;  
  size_t N; // observations
  size_t T; // time
  size_t M; // number of mixtures
  size_t Y; // number of years

  double pclaim;
  // constructors
  randomDraws();
  randomDraws(const data *dat,const size_t mm,
              const size_t s1, const size_t s2);
  // default copy, assignment, and destructor are thread safe 
  int type(const size_t i, const size_t y, const parameters &pm) const;
  double theta(const size_t i, const size_t t, const size_t m, const size_t y, const parameters &pm) const; 
  double omega(const size_t i, const size_t t, const size_t m, const size_t y, const parameters &pm) const;
  bool lambda(const size_t i, const size_t t, const size_t m, const size_t y, const size_t lambdaIndex, 
              const parameters &pm) const;
  size_t lambdaTrans(const bool claim,const size_t lastLambda, 
                     const size_t i, const size_t t, const size_t m, const size_t y, const parameters &pm) const;
  int joinOffset(const size_t i, const size_t y) const;
  int claim(const size_t i, const size_t t, const size_t m, const size_t y, 
                   const parameters &pm, const double cut) const;
  double weight(const size_t i, const size_t y, const parameters &pm) const;
  void zeroJoinOffset(); // sets joinOffset to zero
}; //  end class randomDraws

class simData {
public:
  array<double,3> cost;
  array<double,3> cumCost;
  array<double,3> delay;
#ifdef DEBUG
  array<double,3> vtreat, vno, theta;
#endif
  array<int,3> lambdaIndex;
  array<double,2> weight;
  array<double,2> totalDelay;
  array<double,2> vJoin; // value at joinweek
  const data *dat;
  
  simData();
  simData(const data &d);
  simData& operator=(const data &d);
  void computeValueJoin(const valueFunction* vf, const randomDraws &, 
                        const parameters&); 
};

vector<simData> simulate
(const parameters&pm, 
 const data &dat,
 const vector<randomDraws> &rand, // size = number of simulations
 valueFunction *value  // value functions for each plan and theta mixture component
 );

double simMMobjective(const vector<double> &x, vector<double> &grad, void *vpt);
struct momentData {
  vector<randomDraws> rand;
  data *dat;
};

vector<double> calcMoments(const vector<simData> &sdat);
vector<double> calcMoments(const data &dat);
alglib::real_2d_array varMoments(const data &dat, const vector<double> &moments);
alglib::real_2d_array weightMatrix(const data &dat, const vector<double> moments);

valueFunction* createValueFunctions(const data &dat, const parameters &pm);
void destroyValueFunctions(valueFunction *vf);

#endif
