#ifndef DATA_HPP
#define DATA_HPP

#include "oopFunc.hpp"
#include "omegaDist.hpp"
#include <string>
#include <cmath>


namespace dataNS {

class config {
private:
  void setDefaults();
public: 
  // configureable constants
  int NMIX;
  int NX;
  bool estRiskAversion;
  int nLambda; 
  bool singleLambdaRatio;
  bool claimLambda; // whether (or not) claiming affects lambda transitions
  int NPARAM;
  double probKeep_default;
  double probKeep65_default;
  int nplanUse_default;  // make negative to use all plans
  bool useHistEM;  // use histogram or kernel excess mass moments
  int nbinEM;  // number of bins for excess mass
  double limEM; // excess mass bins go this far from ICL on each side
  int nbinTiming; // set to 2 to just use before ICL & near ICL
  int diffTiming; // set to 0 for levels, 1 for diff pre-kink, 2 for diff across adjacent months 
  double limTiming;
  double regularize; // adds I*regularize to V(m) for weighting
  int nlags; // number of lags to match P(claim t & claim t-lag), set to -1 for none
  bool useWeeks; // use weeks to first claim moments?
  bool useSpendAutoCov; // use autocovariance of spending as moments
  bool useMonthlyProb; // use P(claim in month or later | total spending) moments?
  bool useExcessJanuary; // use excess January spending as moment?
  int nMonthlyProb; // number of monthly timing moments December, November, etc
  int nxSegment;
  int nint;
  bool gaussHermiteQuad; // use gauss hermite quadrature?
  double maxThetaQuantile; // anything>=1 means no maximum
  double riskAversion;
  bool useSimulatedData; // estimate on simulated data?
  bool positiveCor; // constrain serial correlation to be positive?
  bool onlyTrans; // only estimate transition matrix?
  bool omegaThetaDep; // should p_omega depend on theta?
  bool noUncertainty; // no uncertainty in model? (requires delta=1 for now)
  double fixedDelta; // if >= 0 delta is fixed to this value, if < 0, then delta is estimated
  int weekLength; // data is aggregated to the weekly level, how many
                  // days in a "week"?

  bool terminalV; 
  int nRiskBin;
  
  
  config(); // sets defaults
  config(const char *filename);
  void read(const char *filename);
  void save(); 
  void save(const char *filename);  
};
  
  extern config cfg;
  
template<typename T, size_t dim> 
class array {
private:
  vector<T> elements;
  vector<size_t> dims;
public:
  array();
  array(size_t N1,size_t N2=0, size_t N3=0, size_t N4=0, size_t N5=0);
  array(const vector<size_t> &d);
  T& operator[](size_t i);
  const T& operator[](size_t i) const;
  T& operator()(size_t s1, size_t s2=0, size_t s3=0, size_t s4=0, size_t s5=0);
  const T& operator()(size_t s1, size_t s2=0, size_t s3=0, size_t s4=0,size_t s5=0) const;
  typename vector<T>::iterator begin();
  typename vector<T>::const_iterator begin() const;
  typename vector<T>::iterator end();
  typename vector<T>::const_iterator end() const;  
  void copyVec(vector<T> in);
  size_t size() const;
  size_t dimSize(size_t i) const;
};

vector<double> setXsolve(const oopFunc &oop, const double maxx);
  
class data {
private:
  void allocate();
  //void deallocate();
  //void copy(const data &in);
  double probKeep;
  double probKeep65;
public:
  data(const char *filename, 
       const double pk=cfg.probKeep_default, const double pk65=cfg.probKeep65_default, 
       const int nplanUse = cfg.nplanUse_default, const int nplanSkip=0, 
       const bool differentObs=false,
       const int minYear=2007, const int maxYear=2009);
  void loadSQLite(const std::string &fname, const int nplanUse = cfg.nplanUse_default, 
                  const int nplanSkip=0, const bool differentObs=false,
                  const int minYear=2007, const int maxYear=2009);
  data(); 
  // default copy, assignment, and destructor are fine.
  //data(const data &in);
  //~data();
  //data& operator=(const data &in);  

  int N;     // number of unique beneficiaries
  int T;     // number of period per year 
  int Years; // number of years per beneficiary
  int valueT; // = T
  int nplan;  // number of plans

  // order of subscripts = i,week,year
  array<int,2> joinweek;
  array<int,2> in; // observed indicator
  array<int,2> plan;
  array<double,3> cost;
  array<double,3> cumCost;
  array<double,2> weight;
  array<double,2> ded;
  array<double,2> icl;
  array<double,2> cat;  
  array<double,3> X; // i, component, year
  array<double,2> riskScore; // i , year
  array<int,2> age;  
  array<double,2> eoyPrice; // end of year price
  
  array<int,1> up;
  vector<oopFunc> oop;
  vector< vector<double> > xsolve;
  vector<int> nx;
  
  void resetXsolve();
  void resetEeoyPrices(array<double,3> cumCost);
};

class parameters {
public: 
  vector<double> xvec;
  
  vector< omegaDist<double> > omega;
  vector< thetaDist<double> > thetaD;
  array<double,2> betaMixP; 
  array<double,3> transitionProb; //(int claim, int newLambdaIndex, int oldLambdaIndex);
  vector<double> startProb; 
  array<double,2> lambda;
  double delta;
  double riskAversion;
  double deltaTheta;
  double deltaOmega;
  double pOT; // p_omega(theta) = (2p-1+exp(theta*pOT))/(1+exp(theta*pOT)

  vector<double> powDelta;
  vector<double> powDeltaTheta;
  vector<double> powDeltaOmega;
  
  bool gaussHermiteQuad;
  double maxThetaQuantile;
  
  vector<double> thetaV; // integration points
  vector<double> thetaP; // integration weights
  int nTheta; // number of integration points
  int nThetaMix; 
  parameters(const vector<double> &x);  
  void resetIntegration(); // calculates integration points and weights for value function calculation  
};



}; // end namespace dataNS

#endif
