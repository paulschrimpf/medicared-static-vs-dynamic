#include<omp.h>
#include<iostream>
#include<cmath>

#include "alglib/interpolation.h"
#include "alglib/specialfunctions.h"

#include "data.hpp"
#include "oopFunc.hpp"
#include "omegaDist.hpp"
#include "fastSpline.hpp"
#include "computeValueFunction.hpp"

//using namespace alglib;
using namespace fastSpline;
using alglib::real_1d_array;
using std::cout;
using std::endl;
using std::isfinite;


double valueFunction::eval(const double &x, double *type) const
{  // double *type is for overloading return value
  return(spline1dcalc(*v,x)); 
}
/****************************************************************************/

inline double evalSpline(spline1dinterpolant *v,
                         double x) 
{
  return(spline1dcalc(*v,x)); 
}

int nwarn; 

template<typename T>
vector<T> computeVgrid(spline1dinterpolant *vtm1, const int t,
                       const omegaDist<T> &omega, const T &delta, 
                       const T &lambda, const T *thetaV, 
                       const double *thetaP, const int nTheta,
                       const vector<double> &x, const int nx, 
                       const oopFunc *oopfs,
                       /* f and finv allow Epstein-Zin type recursive
                          preferences with risk aversion but perfect
                          intemporal substitution  

                          For additively separable preferences, set
                          f=finv=Identity. 
                          
                          For CARA, set 
                            f(x) = exp(-r x)  
                          and 
                            finv(x) = -1/r log(x)                          

                          The resulting value function satisfies
                            V = finv( E[ f(max { c + beta V }) ] )

                          F is used to calculate E[f(omega+beta V) | omega/theta < cut]
                          It should be such that dF/dx(x) = f(x)
                       */
                       const array<double,3> &transProb,
                       const size_t lambdaIndex, 
                       const vector<double> &Lambda,
                       transforms<T> *trans,
                       const bool terminalV ,
                       const double deltaTheta ,
                       const double deltaOmega ,
                       const vector<double> powDelta,
                       const vector<double> powDeltaTheta,
                       const vector<double> powDeltaOmega,
                       const double pOT // omega p = (2p-1 + exp(pOT*theta))/(1+exp(pOT*theta))
                       )
{
  size_t nLambda = Lambda.size();
  omegaDist<T> od = omega;
  const double pom=omega.p;
  if ((trans==NULL || trans->getRA()==0) && terminalV==false) {
    IDtransforms<T> id;
    trans = &id;
    vector<T> v(nx);
    //static int nout=0;
    for(int g=0;g<nx;g++) {
      T vnotreat = 0;
      T vnoshock = 0;
      if (vtm1) {
        for (size_t m=0;m<nLambda;m++) {
          double evl = evalSpline(vtm1+m,(T) x[g]);
          vnotreat += delta*transProb(0,m,lambdaIndex)*evl;
          vnoshock += delta*transProb(1,m,lambdaIndex)*evl;
        }
      }
      T emax = 0;     
      for(int i=0;i<nTheta;i++) {
        if (pOT != 0) {          
          double pnew=(exp(pOT*thetaV[i]))/(1/pom-1+exp(pOT*thetaV[i]));
          if (!isfinite(pnew)) pnew=pOT*thetaV[i]>0? 1:0;            
          od.setparam(&pnew);
        }
        T vtreat = -oopfs->eval(thetaV[i],x[g]);
        if (vtm1) {
          for (size_t m=0;m<nLambda;m++) {
            vtreat += delta*transProb(1,m,lambdaIndex)*evalSpline(vtm1+m,(T) x[g]+thetaV[i]); 
          }
        }
        T cut = -(vtreat-vnotreat)/(thetaV[i]);
        //cut = cut>1? 1:cut;
        T ptreat = 1.0 - od.cdf(cut);
        T eono = -thetaV[i]*od.truncEx(cut,-1);
        T tmp = (vtreat*ptreat + eono + vnotreat*(1-ptreat));        

        emax += tmp*thetaP[i];
      } // end for(i)
      v[g] = (1-lambda)*vnoshock + lambda*emax;
    } // end for(g)
    return(v);
  } else if (terminalV==false) { // & risk aversion > 0
    // Note: assumes decision to treat or not does not affect lambda
    // evolution, ie transProb(0,m,l) = transProb(1,m,l)
    vector<T> v(nx);
    T ra = trans->getRA();
    //static int nout=0;
    for(int g=0;g<nx;g++) {
      v[g] = 0;
      for (size_t m=0;m<nLambda;m++) {
        T vnotreat = 0; // E[v|don't treat]
        T vnoshock = 0; // E[v|no shock to treat]
        if (vtm1) {
          double evl = evalSpline(vtm1+m,(T) x[g]);
          vnoshock = vnotreat = delta*evl;
        }
        T emax = 0;
        for(int i=0;i<nTheta;i++) {
          if (pOT != 0) {          
            double pnew=(exp(pOT*thetaV[i]))/(1/pom-1+exp(pOT*thetaV[i]));
            if (!isfinite(pnew)) pnew=pOT*thetaV[i]>0? 1:0;            
            od.setparam(&pnew);
          }
          T vtreat = -oopfs->eval(thetaV[i],x[g]);
          if (vtm1) {
            vtreat += delta*evalSpline(vtm1+m,(T) x[g]+thetaV[i]); 
          }
          T cut = -(vtreat-vnotreat)/(thetaV[i]);
          //cut = cut>1? 1:cut;
          T ptreat = 1.0 - od.cdf(cut);
          T evno;
          if (cut<=0) evno = 0;       
          else if (cut>=1) evno = od.p*(-exp(-ra*(-thetaV[i]+vnotreat)) + 
                                            exp(-ra*vnotreat)) / (-thetaV[i]*ra) +
                             (1-od.p)*exp(-ra*(-thetaV[i]+vnotreat));
          else evno =  od.p*(-exp(-ra*(-thetaV[i]*cut+vnotreat)) + exp(-ra*vnotreat))/(-thetaV[i]*ra);
          T evi = (exp(-ra*vtreat)*ptreat + evno);
          if (!isfinite(evi)) {
            T large = 400;
            if ((ra*thetaV[i])<large) {
              large = (ra*thetaV[i]);
            }
            large = exp(large);
            if (nwarn<10) {
#pragma omp critical 
              {
                nwarn++;
                if (nwarn<1) 
                  cout << "WARNING computeVgrid: E[v|theta=" << thetaV[i]
                       << "] is not finite" << endl
                       << "       ra=" << ra << " evno=" << evno << " vtreat=" 
                       << vtreat << " thetaP[i]=" << thetaP[i]
                       << endl
                       << "  setting E[v|theta] to " << large
                       << endl;   
              }
            }                
            emax += large*thetaP[i];
          } else {
            emax += evi*thetaP[i];
          }
        } // end for(i)
        v[g] += transProb(0,m,lambdaIndex)*(emax*Lambda[m] + (1-Lambda[m])*exp(-ra*vnoshock));
      } // end for(m)
      T tmp=v[g];
      v[g] = -1/ra*log(v[g]);
      if (!isfinite(v[g])) {
        if (nwarn<5) {
#pragma omp critical 
          {
            nwarn++;            
            cout << "WARNING computeVgrid v[" << g << "] is not finite, setting to 1e300" << endl;
            cout << tmp << endl;
            v[g] = 1e300;
          }
        }
      }      
      //v[g] = trans->finv( (1-lambda)*vno + lambda*emax );
    } // end for(g)
    return(v);
  } else if (terminalV==true) {
    IDtransforms<T> id;
    trans = &id;
    vector<T> v(nx);
    //static int nout=0;
    for(int g=0;g<nx;g++) {
      T vnotreat = 0;
      T vnoshock = 0;
      if (vtm1) {
        for (size_t m=0;m<nLambda;m++) {
          double evl = evalSpline(vtm1+m,(T) x[g]);
          vnotreat += delta*transProb(0,m,lambdaIndex)*evl;
          vnoshock += delta*transProb(1,m,lambdaIndex)*evl;
        }
      }
      T emax = 0;
      for(int i=0;i<nTheta;i++) {
        if (pOT != 0) {
          double pnew=(exp(pOT*thetaV[i]))/(1/pom-1+exp(pOT*thetaV[i]));          
          if (!isfinite(pnew)) pnew=pOT*thetaV[i]>0? 1:0;            
          od.setparam(&pnew);
        }        
        T vtreat = -oopfs->eval(thetaV[i],x[g]);
        T vdelay = vnotreat - //pow(delta*deltaTheta,t+1)*
          powDelta[t+1]*powDeltaTheta[t+1]*oopfs->eoyPrice()*thetaV[i];
        T vnever = vnotreat;
          
        if (vtm1) {
          for (size_t m=0;m<nLambda;m++) {
            vtreat += delta*transProb(1,m,lambdaIndex)*evalSpline(vtm1+m,(T) x[g]+thetaV[i]); 
          }
        }
        T cut1 = //pow(deltaTheta/deltaOmega,t+1)*
          //powDeltaTheta[t+1]/powDeltaOmega[t+1]*  
          // Assumming deltaTheta=deltaOmega
          oopfs->eoyPrice()*(1-delta*deltaOmega);
        T cut2 = -(vtreat-vdelay)/(thetaV[i])*(1-delta*deltaOmega);
        T cut3 = -(vtreat-vnever)/(thetaV[i])*(1-delta*deltaOmega)/ //(1-pow(delta*deltaOmega,t+1));
          (1 - powDelta[t+1]*powDeltaOmega[t+1]);
        T tmp;
        if (cut1 <= cut2)  {
          tmp = -thetaV[i]*cut1*cut1*0.5/(1-delta*deltaOmega) + cut1*vnever;
          if (cut2 <= cut3) {
            if (cut3>1) cut3 = 1;
            tmp += -thetaV[i]*0.5*(cut3*cut3 - cut1*cut1)* // (1-pow(delta*deltaOmega,t+1))/(1-delta*deltaOmega) +
              (1 - powDelta[t+1]*powDeltaOmega[t+1])/(1-delta*deltaOmega) + 
              (cut3-cut1)*vdelay;
            if (cut3<1) {
              tmp += (1-cut3)*vtreat;
            }
          } else {
            std::cout << "if cut1<=cut2, then cut2 > cut3 should be impossible" << std::endl;
            exit(1);
          }
        } else {
          if (cut2<0) tmp = vtreat;
          else if (cut2>1) tmp = -thetaV[i]*0.5/(1-delta*deltaOmega) + vnever;
          else tmp = -thetaV[i]*cut2*cut2*0.5/(1-delta*deltaOmega) + cut2*vnever + (1-cut2)*vtreat;
        }
        emax += tmp*thetaP[i];
        //if (!isfinite(tmp)) {
        //  cout << "boo";
        //}
      } // end for(i)
      v[g] = (1-lambda)*vnoshock + lambda*emax;
      //if (!isfinite(v[g])) {
      //  cout << "poo";
      //}      
    } // end for(g)
    return(v);    
  } else {
    std::cout << "This should never be reachable" << std::endl;
    exit(1);
  }
}

void computeValueFunction(spline1dinterpolant **vsplines, 
                          const omegaDist<double> *omega,                           
                          const double delta, const double *lambda, 
                          const double *thetaV, const double *thetaP, 
                          const int nTheta,
                          const double *thetaMixP, const int nThetaMix,
                          const int T, const vector< vector<double> > &x, const vector<int> &nx, 
                          const vector<oopFunc> &oopfs, 
                          const int nplan, 
                          const array<double,3> &transProb,
                          const double riskAversion, 
                          const bool terminalV,
                          const double deltaTheta, const double deltaOmega,
                          const vector<double> powDelta,
                          const vector<double> powDeltaTheta,
                          const vector<double> powDeltaOmega, 
                          const double pOT
                          )
{  
  //int maxThreads=nplan*nThetaMix;
  //if (maxThreads<24) omp_set_num_threads(maxThreads);
  transforms<double> *transformation;
  IDtransforms<double> id;
  CARAtransforms<double> cara(riskAversion);
  int nLambda = transProb.dimSize(1);
  vector<double> Lambda(nLambda);
  if (riskAversion<=0) {
    transformation=NULL;
  } else {
    transformation=&cara;
  }
  nwarn = 0;
#pragma omp parallel default(shared)
  {
#pragma omp for collapse(2)
    for(int p=0;p<nplan;p++) {
      for (int mix=0;mix<nThetaMix;mix++) {
        if (!(oopfs[p].ready())) continue;
        spline1dinterpolant *vspline, *vtm1=NULL;
        const oopFunc *oopf = &oopfs[p];
        real_1d_array v,xa;     // types for using alglib
        try {
          xa.setlength(nx[p]);
          xa.setcontent(nx[p],&(x[p][0]));
          v.setlength(nx[p]);    
        } catch(alglib::ap_error e)  {
          printf("error msg: %s\n", e.msg.c_str());
        }
        for (int i=0;i<nLambda;i++) Lambda[i] = lambda[mix + i*nThetaMix];
        for(int t=0;t<T;t++) {
          if (t>0) vtm1 = vsplines[0 + mix*nLambda + (t-1)*nThetaMix*nLambda + p*T*nThetaMix*nLambda];
          for (int lambdaIndex=0;lambdaIndex<nLambda;lambdaIndex++) {
            vspline = vsplines[lambdaIndex + mix*nLambda + t*nThetaMix*nLambda + p*T*nThetaMix*nLambda];
            vector<double> vvec = 
              computeVgrid<double>(vtm1,t,
                                   omega[mix],delta,lambda[mix+lambdaIndex*nThetaMix],
                                   thetaV+mix*nTheta,thetaP+mix*nTheta,
                                   nTheta,x[p],nx[p],oopf,
                                   transProb,
                                   lambdaIndex, Lambda,
                                   transformation,
                                   terminalV, deltaTheta, deltaOmega,
                                   powDelta,
                                   powDeltaTheta,
                                   powDeltaOmega, 
                                   pOT);
            for (int g=0;g<nx[p];g++) {
              v[g] = vvec[g];
            } // end for(g)
            
            try {
              buildspline(xa,v,*vspline);
            } catch(alglib::ap_error e)  {
              printf(" p = %d, mix = %d, t=%d\n",p,mix,t);
              for(int i =0;i<xa.length();i++) {
                printf(" %8.4g %8.4g\n", xa[i],v[i]);
              }
              printf("error msg: %s\n", e.msg.c_str());
            }
          } // end for(lambdaIndex)
        } // end for(t)
      } // end for(mix)
    } // end for(p)
  } // end omp  parallel  
  return;
}


