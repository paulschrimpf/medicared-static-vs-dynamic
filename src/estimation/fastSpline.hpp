#ifndef FASTSPLINE_HPP
#define FASTSPLINE_HPP

#include <vector>
using std::vector;

#include "alglib/interpolation.h"
using alglib::real_1d_array;

namespace fastSpline {

class spline1dinterpolant: public alglib::spline1dinterpolant
{
  public:
  vector<int> node;
  vector<double> xcut;
  vector<double> delx;
  spline1dinterpolant();
  spline1dinterpolant(const spline1dinterpolant &rhs);
};

void buildspline(const real_1d_array &x, const real_1d_array &y, spline1dinterpolant &c);  
double spline1dcalc(const spline1dinterpolant &spline, const double x);

} // end namespace fastSpline

#endif
