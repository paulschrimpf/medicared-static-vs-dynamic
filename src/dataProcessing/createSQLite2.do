clear
clear programs
program writeSQLite , plugin
//use /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf/Data/extract_new/Sample_Full.dta
local path /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf

use `path'/Data/inputs/FullSample_Plan_Bene, clear
keep bene_id yr age
sort bene_id yr
save `path'/ids, replace


use /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf/Data/inputs/Claims_2007_1_1.dta, clear
drop if 1
foreach year in 2007 2008 {

	forv x = 1/4 {
	forv y = 1/100 {
	
		append using /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf/Data/inputs/Claims_`year'_`x'_`y'
		display "year=`year' x=`x' y=`y'"
	}
	}
}

forv y = 1/100 {

	append using /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf/Data/inputs/Claims_2009_`y'
	display "year=2009 y=`y'"
} 

merge m:1 bene_id yr using `path'/ids
