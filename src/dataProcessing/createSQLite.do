
clear
clear programs
//use /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf/Data/extract_new/Sample_Full.dta
local path /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf
use `path'/Data/extract_new/Sample_Full, clear
//use /disk/agedisk3/medicare.work/finkels-DUA22559/tangb/contract_design/output/claims_sample/Claims_Sample_Full_07080910_riskscores.dta, clear
program writeSQLite , plugin

//!rm `path'/test.sl3
//
sort bene_id srvc_dt
gen eoy = 1 if (bene_id[_n] != bene_id[_n+1] | yr[_n] != yr[_n+1])
bys bene_id yr: egen yearCst = sum(totalcst)
bys bene_id yr: egen yearPtPay = sum(ptpayamt)
#delimit ;

/*
plugin call writeSQLite  bene_id srvc_dt ptpayamt totalcst bnftphas
 , "`path'/medD-full-payHist.sl3" "1 5"
  bene_id srvc_dt ptpayamt totalcst bnftphas;
*/
  
/*plugin call writeSQLite  bene_id srvc_dt ptpayamt totalcst bnftphas
  if age < 67, "`path'/medD-age6566-payHist.sl3" "1 5"
  bene_id srvc_dt ptpayamt totalcst bnftphas;
*/
  
drop if eoy!=1;
gen eoyPrice = .;
replace eoyPrice = plan_coin_ded if bnftphas=="DD";
replace eoyPrice = plan_coin_pre if bnftphas=="DP" |  bnftphas=="PP";
replace eoyPrice = plan_coin_gap if bnftphas=="DI" |  bnftphas=="PI" |  bnftphas=="II";
replace eoyPrice = plan_coin_cat if bnftphas=="DC" |  bnftphas=="PC" |  bnftphas=="IC" |  bnftphas=="CC";

replace eoyPrice = plan_coin_ded if missing(eoyPrice) & yearPtPay<ded_amt & ded_amt>0;
replace eoyPrice = plan_coin_pre if missing(eoyPrice) & yearCst<=icl_amt;
replace eoyPrice = plan_coin_gap if missing(eoyPrice) & yearPtPay<oopt_amt;
replace eoyPrice = plan_coin_cat if missing(eoyPrice) & yearPtPay>=oopt_amt;


/*
gen noded = 0;
replace noded = 1 if missing(plan_coin_ded);
replace plan_coin_ded=0 if missing(plan_coin_ded);
gen nors=0;
replace nors=1 if missing(riskscore);
replace riskscore=1 if missing(riskscore);

reg eoyPrice (c.riskscore c.plan_coin_ded#i.noded c.plan_coin_pre c.plan_coin_gap c.plan_coin_cat c.*_amt)##c.riskscore##c.riskscore##i.nors;
predict EeoyPrice;
// remove extreme predictions
centile EeoyPrice, centile(0.01 99.99);
replace EeoyPrice=r(c_1) if EeoyPrice<r(c_1);
replace EeoyPrice=r(c_2) if EeoyPrice>r(c_2);

encode plan, generate(pid);
reg eoyPrice i.pid;
predict eoyPlanPrice;
drop pid;

// restore missing values
replace plan_coin_ded = . if noded==1;
replace riskscore = . if nors==1;
*/
  
drop srvc_dt ptpayamt totalcst bnftphas;
duplicates drop;
sort bene_id yr;


plugin call writeSQLite  bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         plan_coin_ded 
                         plan_coin_pre
                         plan_coin_gap 
                         plan_coin_cat 
                         riskscore_CE
                         nogapcov
                         eoyPrice
   , "`path'/medD-full-const.sl3" "1 3"
                         bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         coin_ded
                         coin_pre
                         coin_gap 
                         coin_cat 
                         riskscore_CE
                         nogapcov
                         //eoyPlanPrice
                         eoyPrice
                         //EeoyPrice
;
/*
plugin call writeSQLite  bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         plan_coin_ded 
                         plan_coin_pre
                         plan_coin_gap 
                         plan_coin_cat 
                         riskscore_CE
                         nogapcov
                         eoyPrice
    if age < 67, "`path'/medD-age6566-const.sl3" "1 3"
                         bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         coin_ded
                         coin_pre
                         coin_gap 
                         coin_cat 
                         riskscore_CE
                         nogapcov
                         //eoyPlanPrice
                         eoyPrice
                         //EeoyPrice
;
*/
