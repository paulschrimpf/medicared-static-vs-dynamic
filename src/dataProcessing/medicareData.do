/*

*/
clear
use /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf/Data/extract/data_extract

sort bene_id srvc_dt
bysort bene_id yr (srvc_dt): gen cumCost=sum(totalcst)
bysort bene_id yr (srvc_dt): gen cumPay =sum(ptpayamt)

//assert(cumPay<=cumCost) // 36 observations fail this assertion
//assert(ptpayamt<=totalcst) // 22 observationa fail

gen segment = .
replace segment = 1 if cumCost<ded_amt
replace segment = 2 if cumCost>=ded_amt & cumCost<icl_amt
replace segment = 3 if cumCost>=icl_amt & cumPay<oopt_amt
replace segment = 4 if cumPay>=oopt_amt
label define  seglabel 1 "below deductible" 2 "b/t ded and icl" ///
  3 "donut hole" 4 "above catastrophic"
label values segment seglabel

tab segment bnftphas // Why do these differ?


gen relpay = ptpayamt/totalcst

  
bys seg plan: su relpay
bys bnftphas plan: su relpay

xi: reg cumPay i.seg|cumCost if plan=="xkxSrd8x", nocons
predict cphat
twoway (scatter cumPay cumCost) ///
  (line cphat cumPay if segment==1, sort) ///
  (line cphat cumPay if segment==2, sort) ///
  (line cphat cumPay if segment==3, sort) ///
  (line cphat cumPay if segment==4, sort)
graph export fig/nodeductFit.eps, replace

xi: reg relpay i.seg if plan=="xkxSrd8x"
drop cphat
predict cphat
twoway (scatter relpay cumCost) ///
  (line cphat relpay if segment==1, sort) ///
  (line cphat relpay if segment==2, sort) ///
  (line cphat relpay if segment==3, sort) ///
  (line cphat relpay if segment==4, sort)
graph export fig/nodeductRel.eps, replace


xi: reg cumPay i.seg|cumCost if plan=="xxkSreD9", nocons
drop cphat
predict cphat
twoway (scatter cumPay cumCost) ///
  (line cphat cumPay if segment==1, sort) ///
  (line cphat cumPay if segment==2, sort) ///
  (line cphat cumPay if segment==3, sort) ///
  (line cphat cumPay if segment==4, sort)
graph export fig/deductFit.eps, replace

xi: reg relpay i.seg if plan=="xxkSreD9"
drop cphat
predict cphat
twoway (scatter relpay cumCost) ///
  (line cphat relpay if segment==1, sort) ///
  (line cphat relpay if segment==2, sort) ///
  (line cphat relpay if segment==3, sort) ///
  (line cphat relpay if segment==4, sort)
graph export fig/deductRel.eps, replace
