clear
//program writeSQLite , plugin
local N = 5000
local path /disk/agedisk5/medicare.work/afinkels-DUA22559/schrimpf

set obs `N'
gen bene_id = string(_n)
gen yr = 2008
gen plan = bene_id
gen u = runiform()
gen noded = u<0.7
drop u
gen ded_amt = 0 if noded
replace ded_amt = 225 + runiform()*100 if ~noded
gen icl_amt = 2310 + runiform()*300
gen oopt_amt = 3750 + runiform()*500
gen plan_coin_ded = 1.0
gen plan_coin_pre = 0.25
gen plan_coin_gap = 1.0
gen plan_coin_cat = 0.07
gen age = floor(runiform()*10+65)
gen bene_dob = 0
gen female = 0
gen white = 1
gen partd_joinmont=0
gen riskscore_CE=1
gen nogapcov=1

#delimit ;
plugin call writeSQLite  bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         plan_coin_ded 
                         plan_coin_pre
                         plan_coin_gap 
                         plan_coin_cat 
                         riskscore_CE
                         nogapcov
   , "`path'/medD-fake-const.sl3" "1 3"
                         bene_id       // 1
                         yr            // 2
                         plan          // 3
                         ded_amt       // 4
                         icl_amt       
                         oopt_amt      
                         bene_dob      
                         age           
                         female        
                         white         
                         partd_joinmont
                         coin_ded
                         coin_pre
                         coin_gap 
                         coin_cat 
                         riskscore_CE
                         nogapcov
;
#delimit cr

forvalues t=1/51 {
  gen totalcst`t' = exp(rnormal(1.5,1)) if runiform()<0.3
}
keep bene_id totalcst*
reshape long totalcst, i(bene_id)
keep if ~missing(totalcst)
gen srvc_dt = date("1/1/2008","MDY") + floor(runiform()*365)
sort bene_id srvc_dt
gen ptpayamt = -9999
gen bnftphas="NA"
#delimit ;

plugin call writeSQLite  bene_id srvc_dt ptpayamt totalcst bnftphas
  , "`path'/medD-fake-payHist.sl3" "1 5"
  bene_id srvc_dt ptpayamt totalcst bnftphas;

#delimit cr
