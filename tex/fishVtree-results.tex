\documentclass[11pt]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{ulem}
\usepackage{graphicx}

\setcounter{MaxMatrixCols}{10}
\usepackage{natbib}

\newcommand{\dsum}{\displaystyle \sum}
\newcommand{\dint}{\displaystyle \int}
\newcommand{\tprod}{\textstyle \prod}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\plim}{plim}
\newcommand{\indist}{\overset{d}{\rightarrow}}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\  \rule{0.5em}{0.5em}}
\setlength{\textwidth}{470pt}
\setlength{\textheight}{650pt}
\setlength{\oddsidemargin}{0pt}
\setlength{\marginparwidth}{72pt}
\setlength{\topmargin}{-36pt}
\setlength{\footnotesep}{10pt}
\renewcommand{\baselinestretch}{1.2}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\usepackage{fancyhdr}

\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\rightmark}
\fancyhead[R]{\thepage}
\renewcommand{\sectionmark}[1]{\markboth{}{\small{\textsc{\thesection~#1}}}}
\renewcommand{\subsectionmark}[1]{}% Remove \subsection from header
\renewcommand{\headrulewidth}{0pt}


%\begin{filecontents}{est.bib}
%\end{filecontents}

%\date{\today}

\begin{document}

%\maketitle

This reports results using 10\% of observations in the 5 most popular
plans. This sample has 14,251 individuals and 20237 indivial $\times$
year observations. Both the baseline model with uncertainty and the
model without uncertainty were re-estimated using this sample. 

\subsection*{No uncertainty spending is approximate}

The model with no uncertainty was taking far too long to solve. Using
the branch and bound method takes about a quarter as long as brute
force checking all possibilities, but it was still too slow. For these
results, I used a simple heuristic to calculate spending. I first
checked for and calculated the value of an interior solution along
each segment of the budget set. I then calculated the value at the
kink allowing partial claims --- this is an upper bound for the value
at the kink with discrete claims. If the largest interior value is
larger, then that interior solution is optimal. If the kink upper
bound is larger, then I calculated the value of the partial claim
solution rounded up and down. I then took the solution with the
largest value among the interior ones and the kink rounded up and
down. In these cases, the calculated spending might not be optimal. 

\begin{table}\caption{Plans in sample}
  \begin{minipage}{\linewidth}
  \begin{center}
    \begin{scriptsize}
      \begin{tabular}{c c ccc cccc}
        Plan & Observations\footnote{Person $\times$ year
          observations, consists 18258 unique people.} & Deductible &
        ICL & Catastrophic & \multicolumn{4}{c}{Coinsurance rates}
        \\ 1 & 4000 & 0 & 2700 & 4350 & --- & 0.3817 & 0.9937 & 0.06884 
        \\ 2 & 6602 & 0 & 2510 & 4050 & --- & 0.3758 & 0.992 & 0.07021 
        \\ 3 & 3615 & 295 & 2700 & 4350 & 0.9959 & 0.1965 & 0.9949 & 0.07179
        \\ 4 & 3617 & 0 & 2510 & 4050 & --- & 0.3359 & 0.9897 & 0.07021
        \\ 5 & 6403 & 0 & 2400 & 3850 & --- & 0.3952 & 0.9923 & 0.07013
        \\ Total & 27499 & \\
      \end{tabular}
    \end{scriptsize}
  \end{center}
  \end{minipage}
\end{table}

\clearpage

\newcommand{\figuresAndTables}[2]{
  \begin{table}\caption{Parameter estimates #2}
    \begin{center}
      \begin{scriptsize}
        \begin{tabular}{c cccccc}
          %{\scriptsize{\input{\folder/paramSE.tex}}}
          \input{#1/param.tex}
        \end{tabular}
        Implied quantities
        
        \begin{tabular}{c cccccc}
          \input{#1/param2.tex}
        \end{tabular}    
      \end{scriptsize}
    \end{center}
  \end{table}
  
  \begin{figure}\caption{Distribution of spending #2}
    \begin{minipage}{\linewidth}
      \begin{tabular}{cc}
        \includegraphics[width=0.5\linewidth]{#1/fit} & 
        \includegraphics[width=0.5\linewidth]{#1/bunchFit} 
      \end{tabular}
    \end{minipage}
  \end{figure}

  \begin{figure}\caption{Fit of timing moments #2}
    \begin{minipage}{\linewidth}
      \begin{center}
        \includegraphics[width=0.7\linewidth]{#1/timingAll}
      \end{center}
    \end{minipage}
  \end{figure}
  

  
  \begin{table}\caption{Elastiticies #2}
  \begin{minipage}{\linewidth}
    \begin{center}
      \input{#1/elasticities}
    \end{center}
    \footnotetext{
      This table reports the elasticity of mean spending. Let
      $\bar{y}_0$ denote average spending under the baseline insurance
      plan, and $\bar{y}_1$ denote average spending when all coinsurance
      rates are changed by $\Delta$. Then the elasticticity of mean
      spending is 
      \[ \frac{\bar{y}_1 - \bar{y}_0}{\bar{y}_0} \times \Delta. \]
    }
  \end{minipage}
  \end{table}
  
  %\begin{figure}\caption{Budget sets \label{fig:budget}}
  %  \begin{minipage}{\linewidth}
  %    \includegraphics[width=0.7\linewidth]{#1/rates} 
  %  \end{minipage}
  %\end{figure}
  \begin{figure}\caption{Distribution of spending under decreased
      coinsurance rates #2}
    \begin{minipage}{\linewidth}
      \begin{center}
        \includegraphics[width=0.7\linewidth]{#1/hist-rates} 
      \end{center}
    \end{minipage}
  \end{figure}
  
  \begin{figure}\caption{Conditional elastiticies #2}
    \begin{minipage}{\linewidth}
      \begin{tabular}{ccc}
        Elasticity & Elasticity (rescaled axis) & Change in spending \\
        \includegraphics[width=0.33\linewidth]{#1/cond-elasticities}
        &
        \includegraphics[width=0.33\linewidth]{#1/cond-elasticities-zoom}
        & 
        \includegraphics[width=0.33\linewidth]{#1/cond-change}
      \end{tabular}
      \footnotetext{ This figure shows the avereage elasticticity and
        change in spending conditional on baseline spending. The left
        and middle panels show the conditional elasticity. They are
        identical except for the different scaling of the vertical
        axis. The lines in these figure plot the elasticitice of
        conditional mean spending. Baseline spending was divided into
        \$500 bins. Within each bin, we compute the conditional mean of
        baseline spending and spending when all coinsurance rates are
        changed by $\Delta$. If $\bar{y}_1$ and $\bar{y}_0$ are the
        conditional mean spending within a bin, then the elasticity of
        mean spending is
        \[ \frac{\bar{y}_1 - \bar{y}_0}{\bar{y}_0} \times \Delta. \]
        The points show the average of individual elasticities within
        each bin. If ${y}_{i,1}$ and ${y}_{i,0}$ are an individual's
        spending, then the individual elasticity is 
        \[ \epsilon_i = \frac{y_{i,1} - y_{i,0}}{y_{i,0}} \times
        \Delta. \] 
        The points show the average of $\epsilon_i$ within each bin. 
        
        The right panel shows average change in spending within each
        bin. 
      }
    \end{minipage}
  \end{figure}
}

\section{Results with uncertainty}
\figuresAndTables{../src/counterfactuals/figures/baseline-pc3-obsPlans}
                {with uncertainty}
\clearpage

\section{Results with certainty}
\figuresAndTables{../src/counterfactuals/figures/noUncertainty-approx-obsPlans}
                {with certainty}


\clearpage

\section{Results with uncertainty and $\delta=1$}
\figuresAndTables{../src/counterfactuals/figures/1delta-obsPlans}
                {with uncertainty and $\delta=1$}


\clearpage

\section{Results with $\delta=0$}
\figuresAndTables{../src/counterfactuals/figures/0delta-obsPlans}
                {with $\delta=0$}

\clearpage 

\section{Results with uncertainty and certainty parameters}
\figuresAndTables{../src/counterfactuals/figures/baseline-noUncertainty-obsPlans}
                {uncertainty, certainty parameters}
\clearpage

\section{Results with certainty and uncertainty parameters}
\figuresAndTables{../src/counterfactuals/figures/noUncertainty-baseline-obsPlans}
                {certainty, uncertainty parameters}
\clearpage


\section{Results with certainty and 0 $\delta$ parameters}
\figuresAndTables{../src/counterfactuals/figures/noUncertainty-0delta-obsPlans}
                {certainty, 0 $\delta$ parameters}
\clearpage

\section{Results with certainty and 1 $\delta$ parameters}
\figuresAndTables{../src/counterfactuals/figures/noUncertainty-1delta-obsPlans}
                {certainty, 1 $\delta$ parameters}
\clearpage

\section{Results with 1 $\delta$ and uncertainty parameters}
\figuresAndTables{../src/counterfactuals/figures/1delta-baseline-obsPlans}
                {1 $\delta$, uncertainty parameters}
\clearpage
\section{Results with 1 $\delta$ and certainty parameters}
\figuresAndTables{../src/counterfactuals/figures/1delta-noUncertainty-obsPlans}
                {1 $\delta$, certainty parameters}

\clearpage
\section{Results with 1 $\delta$ and 0 $\delta$ parameters}
\figuresAndTables{../src/counterfactuals/figures/1delta-0delta-obsPlans}
                {0 $\delta$, 1 $\delta$ parameters}

\clearpage

\section{Results with 0 $\delta$ and uncertainty parameters}
\figuresAndTables{../src/counterfactuals/figures/0delta-baseline-obsPlans}
                {0 $\delta$, uncertainty parameters}
\clearpage
\section{Results with 0 $\delta$ and certainty parameters}
\figuresAndTables{../src/counterfactuals/figures/0delta-noUncertainty-obsPlans}
                {0 $\delta$, certainty parameters}


\clearpage
\section{Results with 0 $\delta$ and 1 $\delta$ parameters}
\figuresAndTables{../src/counterfactuals/figures/0delta-1delta-obsPlans}
                {0 $\delta$, 1 $\delta$ parameters}


\clearpage
\begin{table}[!tbp]
\caption{Comparative statics for elasticities\label{tab4}} 
\begin{center}
\begin{tabular}{l|rrrr}
\hline\hline
& \multicolumn{4}{c}{\textbf{Model}} \\
\textbf{Parameters} &\multicolumn{1}{c}{Full}&\multicolumn{1}{c}{No uncertainty}&\multicolumn{1}{c}{1 delta}&\multicolumn{1}{c}{0 delta}\tabularnewline
\hline
Full&$-0.246$&$-0.197$&$-0.229$&$-0.285$\tabularnewline
No uncertainty&$-0.138$&$-0.131$&$-0.138$&$-0.144$\tabularnewline
1 delta&$$&$-0.187$&$-0.207$&$-0.249$\tabularnewline
0 delta&$$&$-0.159$&$-0.165$&$-0.217$\tabularnewline
\hline
\end{tabular}\end{center}

\end{table}


\end{document}
