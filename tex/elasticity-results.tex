\documentclass[11pt]{article}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{ulem}
\usepackage{graphicx}

\setcounter{MaxMatrixCols}{10}
\usepackage{natbib}

\newcommand{\dsum}{\displaystyle \sum}
\newcommand{\dint}{\displaystyle \int}
\newcommand{\tprod}{\textstyle \prod}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\plim}{plim}
\newcommand{\indist}{\overset{d}{\rightarrow}}
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\  \rule{0.5em}{0.5em}}
\setlength{\textwidth}{470pt}
\setlength{\textheight}{650pt}
\setlength{\oddsidemargin}{0pt}
\setlength{\marginparwidth}{72pt}
\setlength{\topmargin}{-36pt}
\setlength{\footnotesep}{10pt}
\renewcommand{\baselinestretch}{1.2}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\usepackage{fancyhdr}

\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\rightmark}
\fancyhead[R]{\thepage}
\renewcommand{\sectionmark}[1]{\markboth{}{\small{\textsc{\thesection~#1}}}}
\renewcommand{\subsectionmark}[1]{}% Remove \subsection from header
\renewcommand{\headrulewidth}{0pt}

\newcommand{\figdir}{../src/counterfactuals/figures/saezElasticity}

%\begin{filecontents}{est.bib}
%\end{filecontents}
\title{Elasticity and excess mass estimates}

\date{\today}

\begin{document}

\maketitle

\section{Excess Mass}

Excess mass is estimated as in our previous paper. We take the
difference between total spending and the ICL and we compute a
histogram with bins $\{x_j\}_{j=1}^J$ and counts $\{c_j\}_{j=1}^J$. We
then regress the counts on a polynomial of the bins, excluding the
bins within a ``bunching window'' of the ICL. The excess mass is the
difference between the observed mass and the predicted mass near the
ICL, relative to the predicted mass.

The following figures show the excess mass estimated when pooling all
plans together. The bin size varies across the columns, and the degree
of the polynomial varies across the rows. The bunching window is 200
in the first figure and 100 in the second. The excess mass varies
between 0.3 and 0.6 depending on the tuning parameters. 

\begin{figure} \caption{Excess mass with various tuning parameters}
  \begin{minipage}{\linewidth}
    \begin{tabular}{ccc}
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin20-d1}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin40-d1}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin60-d1}
      \\
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin20-d2}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin40-d2}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin60-d2}
      \\
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin20-d3}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin40-d3}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w200-bin60-d3}
    \end{tabular}
  \end{minipage}
\end{figure}

\begin{figure} \caption{Excess mass with various tuning parameters}
  \begin{minipage}{\linewidth}
    \begin{tabular}{ccc}
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin20-d1}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin40-d1}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin60-d1}
      \\
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin20-d2}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin40-d2}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin60-d2}
      \\
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin20-d3}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin40-d3}
      &
      \includegraphics[width=0.33\linewidth]{\figdir/em-w100-bin60-d3}
    \end{tabular}
  \end{minipage}
\end{figure}

\clearpage 
\section{Elasticity estimates}

The elasticity is related to bunching as follows. In our model
spending is 
\begin{equation}
m(\zeta _{i})=\left \{ 
\begin{array}{cc}
\zeta _{i}(2-c_{0})^{\alpha } & if\text{ }\zeta _{i}<m^{\ast
}/(2-c_{0})^{\alpha } \\ 
m^{\ast } & if\text{ }\zeta _{i}\in \lbrack m^{\ast }/(2-c_{0})^{\alpha
},m^{\ast }/(2-c_{1})^{\alpha }] \\ 
\zeta _{i}(2-c_{1})^{\alpha } & if\text{ }\zeta _{i}>m^{\ast
}/(2-c_{1})^{\alpha }
\end{array}
\right. . 
\end{equation}
Hence the mass of people at the kink, $B$ is
\begin{align*}
  B = & F\left(m^{\ast }/(2-c_{1})^{\alpha }\right) - F\left(m^{\ast
  }/(2-c_{0})^{\alpha }\right)
  = & f(\bar{\zeta}) \left[m^{\ast }/(2-c_{1})^{\alpha } - m^{\ast
    }/(2-c_{0})^{\alpha } \right]
\end{align*}
for some $\bar{\zeta} \in \left[m^{\ast }/(2-c_{1})^{\alpha }, m^{\ast 
  }/(2-c_{0})^{\alpha } \right]$. Making the approximation that 
\[ f(\bar{\zeta}) \approx \frac{f(m^{\ast }/(2-c_{1})^{\alpha }) +
  f(m^{\ast }/(2-c_{0})^{\alpha })}{2}. \]
Changing from the density of health, $f$, to the density of spending,
$h$, we have.
\begin{align}
  B \approx & \frac{h(m^\ast)_{-} (2-c_0)^\alpha +
    h(m^\ast)_{+}(2-c_1)^\alpha}{2} \left[m^{\ast }/(2-c_{1})^{\alpha } - m^{\ast
    }/(2-c_{0})^{\alpha } \right] \label{eq:alpha}
\end{align}
To estimate $\alpha$ and the elasticity, we estimate $B$,
$h(m^\ast)_{-}$, and $h(m^\ast)_{+}$, and then use (\ref{eq:alpha}) to
solve for $\alpha$. These three objects are estimated as in the
previous section. $B$ is the observed minus the predicted mass in the
bunching window. $h(m^\ast)_{-}$ is the average predicted density
between -bunchWidth and 0. $h(m^\ast)_{+}$ is the average predicted
density betweeen 0 and bunchWidth.\footnote{I don't think it will
  affect the results, but if I were starting from scratch, I'd
  estimate these differently. At the very least, I'd only estimate
  $h(m^\ast)_{-}$ using data below the ICL and $h(m^\ast)_{+}$ using
  data above. Even if $f(\zeta)$ is smooth, the model suggests $h$
  should not be at $m^\ast$.}


The following tables show the estimated elasticity for various tuning
parameters. The elasticity is estimated separately for each plan. The
last row of the table (plan = -1), is the weighted average across
plans. 

Given $\alpha$ the elasticity of spending at a coinsurance rate of $c$
is $\frac{-\alpha c}{2-c}$. The reported elasticities are at the plan
specific pre-icl coinsurance rates. ``elasticity2'' is evaluated at
the person specific end of year coinsurance rate.


\input{\figdir/elas-w200-bin40-d1}

\input{\figdir/elas-w200-bin40-d3}

\input{\figdir/elas-w200-bin60-d1}

\input{\figdir/elas-w100-bin40-d1}



\end{document}
